-----------------------------------------------------------------------------------------
-- maleta.lua
-----------------------------------------------------------------------------------------
--local introIsPlaying
local composer = require( "composer" )
local gameState = require("utilerias.gameState")
local scene = composer.newScene()
local imagenProducto = {}
-- Variable que indica si ganas el juego
gameState.success = 0

-- Variables obligatorias para juegos con dragging
scene.imagenSeleccionada = nil
scene.imagenColisionada = nil
scene.posIniX = nil
scene.posIniY = nil
------VARIABLES DE GRABACION--------------

local r
local isAndroid = false
local dataFileName = "audiopapu"
if "simulator" == system.getInfo("environment") then
    dataFileName = dataFileName .. ".aif"
else
	local platformName = system.getInfo( "platformName" )
    if "iPhone OS" == platformName then
        dataFileName = dataFileName .. ".aif"
    elseif "Android" == platformName then
        dataFileName = dataFileName .. ".wav"
        isAndroid = true
    else
    	print("Unknown OS " .. platformName )
    end
end
--crea el objeto r que va a servir para hacer las grabaciones
local filePath2 = system.pathForFile( dataFileName, system.DocumentsDirectory )
r = media.newRecording(filePath2)
--funcion para limpiar la memoria yliberar el audio
local function llamame(event)
    print("khe")
   
    audio.dispose(event.handle)
end

-- Variable que cambiará la carpeta local
local filePath

-- Variable que contabilizará los aciertos del juego
local getSuccess

---------------------******Funcion obligatoria*********----
function scene:successful()
    
    --local pronuncia = utils.validarVoz(scene.imagenSeleccionada.id)
    --local pronuncia = true
    
       --[[ if pronuncia then
        local r=math.random(1,2)
       local ruta
        if r==1 then
            ruta="excelente"
        else
            ruta="muybien"
        end
        

        utils.reproducirSonido("sounds/principales/"..ruta,0,function ( ... )]]--
        -------------------********inicio del record********----------------------
-------------------********inicio del record********----------------------
       print("presionado")
    utils.reproducirSonido("bip")
timer.performWithDelay(700, function()
   r:startRecording()
   end,1)
   
    timer.performWithDelay(3000, function() 
                    r:stopRecording()
                    utils.reproducirSonido("bip")
                    timer.performWithDelay(700, function()
            -- Play back the recording
            local file = io.open( filePath2, "r" ) 
            if file then
                io.close( file )
            local playbackSoundHandle = audio.loadStream( dataFileName, system.DocumentsDirectory )
	 			audio.play( playbackSoundHandle, { onComplete= llamame} )
              end 
              end,1)
                end,1)
----------**********fin de el record*****------------------------------
-- @return
timer.performWithDelay(5000, function() 
            scene.imagenSeleccionada:removeEventListener("touch")
            composer.removeScene( "bloqueo" )

            gameState.success = gameState.success+1
        
            if gameState.success >= getSuccess then
                 utils.reproducirSonido("sounds/principales/gana",1,function ( ... )
                     utils.ponerGlobos()
                 end)
                         gameState.success = 0
            end      
        end,1)

              
            
       --[[ else
            transition.to( scene.imagenSeleccionada , {x=scene.posIniX, y=scene.posIniY})
            
            utils.reproducirSonido("sounds/principales/intentalo",1)       
        end]]--
end

 ------------------------------------------------------------------------------------------
-- FUNCION "SHUFFLE" PARA NÚMEROS ALEATORIOS NO REPETIDOS
-------------------------------------------------

local function shuffleArray(array)
        local arrayCount = #array
            for i = arrayCount, 2, -1 do
                local j = math.random(1, i)
                array[i], array[j] = array[j], array[i]
            end
    return array
end

 ------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------
local function btnTap(event)
    --esto es para quitar el bloqueo 
--                        if introIsPlaying == true then
--                                local audioChannel = audio.stop() 
--                                introIsPlaying = false
--                        end
        utils.reproducirSonido("boton")
        composer.gotoScene ( event.target.destination, { effect = "fade"} )
        print(event.target.destination)
    return true
end 
----------------------------------------------------------------------------------------

function scene:create( event )
    
    local objColision
    local counter = 6
    local tipoJuego
    
    local group = self.view
   
        if(gameState.tipodejuego == 2) then
            filePath = "maleta/"
            tipoJuego = 15
            imagenProducto = {1,2,3,4,5,6}
            
            elseif(gameState.tipodejuego == 4) then
                filePath = "arrastra/"
                counter = 5
                tipoJuego = 19                
                imagenProducto = {1,2,3,4,5}
                
            else
                filePath = "secuencias/"
                tipoJuego = 20                
                imagenProducto = {1,2,3,4,5,6}                               
                objColision = {}
        end
        
    ----------------------- CREACIÓN DE COLISIONADORES -----------------------------------
    
    if(gameState.tipodejuego ~= 5)then
        
        objColision = display.newImage( group,path..filePath.."objColision.png" )
        objColision:translate( centerX/2, centerY - 40 )
        objColision.w=objColision.contentWidth/2
        objColision.h=objColision.contentHeight
        
    else
        local sumWidth, sumHeight = centerX/2.3, centerY - 40
        for count = 7, 12 do
            objColision[count] = display.newImage( group,path..filePath..count..".png" )
            objColision[count]:translate( sumWidth, sumHeight )
                        
            sumWidth = sumWidth + 280
            
            if(count%3==0) then
                sumHeight = sumHeight + 177
                sumWidth = sumWidth - 840
            end
                        
            objColision[count].w=objColision[count].contentWidth/2
            objColision[count].h=objColision[count].contentHeight
        end
    end
        
        ----------------------- CREACIÓN DE DRAGGEABLES -----------------------------------
        
        local imgWidth, sumWidth
        local imgHeight, sumHeight
        local countHeight
                
            -- Asignar valores de inicio a cada juego
        
            if gameState.tipodejuego == 2 then -- La maleta
                    imgWidth, imgHeight = display.contentWidth/2.1, centerY / 2.35
                    sumWidth, sumHeight = 121, 150
                    countHeight = 2
                    getSuccess = 6
                    
                    elseif(gameState.tipodejuego == 4) then -- Arrastra                                  
                    imgWidth, imgHeight = display.contentWidth/2, centerY / 1.5
                    sumWidth, sumHeight = 200, 180
                    countHeight = 3
                    getSuccess = 3
                    
                    elseif(gameState.tipodejuego == 5) then -- Secuencias   
                    imgWidth, imgHeight = 100, centerY / 2.35
                    sumWidth, sumHeight = 165, 150
                    countHeight = 7
                    getSuccess = 6
            end
        
        -- hacemos random un las posiciones de las piezas        
        shuffleArray(imagenProducto)
        
        --for count = 1, counter do
        for posicion, pieza in pairs (imagenProducto) do
            
            imagenProducto[posicion] = display.newImage(group,path..filePath..pieza..".png")            
            imagenProducto[posicion].myName = pieza
                        
            -- CREAR IMÁGENES POR EL TIPO DE JUEGO
            
                if gameState.tipodejuego == 2 then -- La maleta
                    utils.renombrarImagenes(tipoJuego,imagenProducto[posicion])
                    
                    elseif(gameState.tipodejuego == 4) then -- Arrastra                                  
                        utils.renombrarImagenes(tipoJuego,imagenProducto[posicion])
                         
                    elseif(gameState.tipodejuego == 5) then -- Secuencias                                  
                        utils.renombrarImagenes(tipoJuego,imagenProducto[posicion])
                end
            
            imagenProducto[posicion]:translate(imgWidth, imgHeight)
            imgWidth = imgWidth + sumWidth
            
            if(posicion%countHeight == 0) then
                imgHeight = imgHeight + sumHeight
                imgWidth = imgWidth - (sumHeight*(countHeight/1.3))
            end
                        
            imagenProducto[posicion].w = imagenProducto[posicion].contentWidth
            imagenProducto[posicion].h = imagenProducto[posicion].contentHeight
            
                if(gameState.tipodejuego ~= 5)then
                    utils.dragging(imagenProducto[posicion],objColision,composer.getSceneName("current"),
                        function (resultado) 
                        --esto es para quitar el bloqueo 
--                        if introIsPlaying == true then
--                                local audioChannel = audio.stop() 
--                                introIsPlaying = false
--                        end
                            if resultado then

                                if(gameState.tipodejuego == 4) then

                                    if(imagenProducto[posicion].id == "pino" or imagenProducto[posicion].id == "coca" or imagenProducto[posicion].id == "tortillas") then
                                        utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                        transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                                    else
                                        utils.reproducirSonido("error")
                                        transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                                    end
                                else
                                    utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                    transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                                end
                            else
                                utils.reproducirSonido("error")
                                transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                            end
                        end)
                    
                else 
                    utils.dragging(imagenProducto[posicion],objColision[pieza+6],composer.getSceneName("current"),
                    function (resultado) 
                        --esto es para quitar el bloqueo 
--                        if introIsPlaying == true then
--                                local audioChannel = audio.stop() 
--                                introIsPlaying = false
--                        end
                        if resultado then
                            utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                            transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                            
                        else
                            utils.reproducirSonido("error")
                            transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                        end
                    end)
                end
        end      
                
    local background = display.newImage( group,path..filePath.."fondo.png" )
    background:translate( centerX, centerY )
    background:toBack()

    matchText = display.newText(group,"", centerX, (centerY/3)*4, "fonts/GothamNarrow-Book" , 26 )
    matchText:setTextColor(0, 0, 0)
    matchText.x = centerX
        
    local btnRegresar = display.newImage( group,"images/btn-regresar.png" )
    btnRegresar:translate( centerX/4, centerY*1.85 )
    btnRegresar:addEventListener("tap", btnTap)
    btnRegresar.destination = "menuJuego"

    if gameState.tipodejuego == 2 then -- La maleta
                    utils.reproducirSonido("sounds/elviaje/jugar/LaMaleta/Instrucciones", 0)
--                    composer.removeScene( "bloqueo" )
--                     introIsPlaying=true 
                    elseif(gameState.tipodejuego == 4) then -- Arrastra                                  
                      utils.reproducirSonido("sounds/lossonidos/jugar/Arrastra/Instrucciones", 0)
--                      composer.removeScene( "bloqueo" )
--                     introIsPlaying=true 
                         
                    elseif(gameState.tipodejuego == 5) then -- Secuencias                                  
                      utils.reproducirSonido("sounds/popi/jugar/Secuencias/Instrucciones", 0)
--                      composer.removeScene( "bloqueo" )
--                     introIsPlaying=true 
                end
   


end        
 ------------------------------------------------------------------------------------------


function scene:show( event )
    local group = self.view
end

function scene:hide( event )
    local group = self.view
    
    local phase = event.phase
    
    if event.phase == "will" then
        -- Called when the scene is on screen and is about to move off screen
        --
        -- INSERT code here to pause the scene
        -- e.g. stop timers, stop animation, unload sounds, etc.)
        
    elseif phase == "did" then
        -- Called when the scene is now off screen
    end 
        
        composer.removeScene( composer.getSceneName( "current" ) )    
end

function scene:destroy( event )
    -- Called prior to the removal of scene's "view" (group)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc.
    local group = self.view 
end

-------------- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
--------------

return scene
