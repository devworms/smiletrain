-----------------------------------------------------------------------------------------
-- viaje.lua
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local scene = composer.newScene()
--local introIsPlaying
--local isTap=false
local grupo=nil
local arreglosBotones={}
local arregloSelecciones={}
local currentButton
local inicioEscena=0
local background=nil
 local inst
 ------VARIABLES DE GRABACION--------------
local r
local isAndroid = false
local dataFileName = "audiopapu"
if "simulator" == system.getInfo("environment") then
    dataFileName = dataFileName .. ".aif"
else
	local platformName = system.getInfo( "platformName" )
    if "iPhone OS" == platformName then
        dataFileName = dataFileName .. ".aif"
    elseif "Android" == platformName then
        dataFileName = dataFileName .. ".wav"
        isAndroid = true
    else
    	print("Unknown OS " .. platformName )
    end
end
local filePath = system.pathForFile( dataFileName, system.DocumentsDirectory )
r = media.newRecording(filePath)
--funcion para limpiar la memoria yliberar el audio
local function llamame(event)
    print("khe")
   
    audio.dispose(event.handle)
end
-------fin de las variables de grabacion---------
----------------------------------------------------------------------------------------
local function cargarFinalResultante()
    --[[local numeroFondo=0
    
    if arregloSelecciones[1]=="cancun"  or arregloSelecciones[2]=="barco" then
        numeroFondo=3
    else 
        numeroFondo= math.random(1, 3)
    end
    
    for i=1, table.getn(arregloSelecciones) do
        print (arregloSelecciones[i])
        
        
    end]]

    transition.fadeOut( background, { time =500 } )
    background:removeSelf()
    background = display.newImage( grupo, path.."viaje/fondo-"..arregloSelecciones[1]..".png")
    
    background:translate( centerX, centerY )
    background:toBack()
    background.alpha=0
    transition.fadeIn( background, { time =500 })
    
    local transporte = display.newImage( grupo, path.."viaje/escena 5/"..arregloSelecciones[2]..".png")
    transporte:translate(centerX*0.5, centerY*1.35)
    
    local sarita = display.newImage( grupo, path.."viaje/escena 5/sarita.png")
    sarita:translate(centerX*1.7, centerY*1.15)
    
        
end

----------------------------------------------------------------------------------------
function scene:successful()
    
    --local pronuncia = utils.validarVoz(currentButton.id)
    --local pronuncia = true
    
    --[[if pronuncia == true then
        local r=math.random(1,2)
       local ruta
        if r==1 then
            ruta="excelente"
        else
            ruta="muybien"
        end
        utils.reproducirSonido("sounds/principales/"..ruta,1,function ( )]]--
        -------------------********inicio del record********----------------------
        print("presionado")
    utils.reproducirSonido("bip")
timer.performWithDelay(700, function()
   r:startRecording()
   end,1)
   
    timer.performWithDelay(3000, function() 
                    r:stopRecording()
                    utils.reproducirSonido("bup")
                    timer.performWithDelay(1000, function()
            -- Play back the recording
            local file = io.open( filePath, "r" ) 
            if file then
                io.close( file )
            local playbackSoundHandle = audio.loadStream( dataFileName, system.DocumentsDirectory )
				audio.play( playbackSoundHandle, { onComplete= llamame} )
              end 
              end,1)
                end,1)
----------**********fin de el record*****------------------------------
-- @return
timer.performWithDelay(5500, function()
           table.insert(arregloSelecciones,currentButton.id)
        --print(table.getn(arregloSelecciones))
        if table.getn(arregloSelecciones)==1 then
            cambiarBotones(grupo,inicioEscena,4,17)
        elseif table.getn(arregloSelecciones)==2 then
            cambiarBotones(grupo,inicioEscena,4,18)
        elseif table.getn(arregloSelecciones)==3 then
            cambiarBotones(grupo,inicioEscena)
            
           utils.reproducirSonido("sounds/principales/gana",1,function ( ... )
                     utils.ponerGlobos()
                 end)
            
        end    
        end,1)
        --composer.removeScene( "bloqueo" )
        --[[end)

        
    else
       utils.reproducirSonido("sounds/principales/intentalo",1)
    end]]--
end
----------------------------------------------------------------------------------------


local function btnTap(event)
    --esto es para quitar el bloqueo 
--    if introIsPlaying == true then
--    local audioChannel = audio.stop() 
--   introIsPlaying = false
--   isTap=true
--    end
        utils.reproducirSonido("boton")
        composer.gotoScene ( event.target.destination, { effect = "fade"} )
        print(event.target.destination)
        
    return true
end 



local function posicionesBotones(escena)
    if escena== 2 then
        arreglosBotones[1]:translate(centerX*0.17,centerY*1.2)
        arreglosBotones[2]:translate(centerX*0.35,centerY*0.74)
        arreglosBotones[3]:translate(centerX*0.75,centerY*0.74)
        arreglosBotones[4]:translate(centerX*0.54,centerY*1.2)
        arreglosBotones[5]:translate(centerX*0.935,centerY*1.2)
    elseif escena== 3 then
        arreglosBotones[1]:translate(centerX*0.165,centerY*1.15)
        arreglosBotones[2]:translate(centerX*0.53,centerY*1.15)
        arreglosBotones[3]:translate(centerX*0.93,centerY*1.15)
        arreglosBotones[4]:translate(centerX*1.31,centerY*1.15)
    elseif escena== 4 then
        arreglosBotones[1]:translate(centerX*0.165,centerY*1.15)
        arreglosBotones[2]:translate(centerX*0.53,centerY*1.15)
        arreglosBotones[3]:translate(centerX*0.93,centerY*1.15)
        arreglosBotones[4]:translate(centerX*1.31,centerY*1.15)    
    end
end

--Esta funcion
function cambiarBotones(group,escena, numeroBotones, tipoJuego)
    
    for i,v in ipairs(arreglosBotones) do
        v:removeSelf()
        v=nil
        
    end
    arreglosBotones={}
    if escena==5 then
        inst.text="Felicidades"
        cargarFinalResultante()
    else
        for count=1,numeroBotones do
            imagen = path.."viaje/escena "..tostring(escena).."/"..tostring(count)..".png"
            
            arreglosBotones[count] = display.newImage(group,imagen)
            arreglosBotones[count].myName = count
            
            -- sonidos
            utils.renombrarImagenes(tipoJuego,arreglosBotones[count])
            
            arreglosBotones[count].w = arreglosBotones[count].contentWidth
            arreglosBotones[count].h = arreglosBotones[count].contentHeight
            
            arreglosBotones[count]:addEventListener("tap",
            function(event)
                currentButton=event.target
                utils.primerSonido("juegos.elviaje.viaje",event.target.id)
                --esto es para quitar el bloqueo 
--    if introIsPlaying == true then
--    local audioChannel = audio.stop() 
--   introIsPlaying = false
--   isTap=true
--    end
                return true
            end)
            
            
        end
        posicionesBotones(escena)
        inicioEscena=inicioEscena+1
        if escena==3 then
             inst.text="Escoge el medio de transporte"
             utils.reproducirSonido("sounds/elviaje/jugar/Elviaje/Instrucciones2", 0)
--             composer.removeScene( "bloqueo" )
--    introIsPlaying=true 
        elseif escena==4 then
             inst.text="Escoge el método de pago"
             utils.reproducirSonido("sounds/elviaje/jugar/Elviaje/Instrucciones3", 0)
--             composer.removeScene( "bloqueo" )
--             introIsPlaying=true 
       
        end
    end
end
    
----------------------------------------------------------------------------------------

function scene:create( event )
    
    local group = self.view
    grupo=group
    background = display.newImage( group,path.."viaje/escena 1/vista-1.png" )
    background:translate( centerX, centerY )
    background:toBack()
    
     
        
    utils.reproducirSonido("sounds/elviaje/jugar/Elviaje/Instrucciones0",0, 
    function()
--        composer.removeScene( "bloqueo" )
--        introIsPlaying=true 
        transition.fadeOut( background, { time =500 } )
        background:removeSelf()
        background = display.newImage( group,path.."viaje/fondo.png" )
        --background = display.newImage( group,path.."viaje/escena 3/fondo escena 3.png" )
        background:translate( centerX, centerY )
        background:toBack()
        background.alpha=0
        transition.fadeIn( background, { time =500 })
        inicioEscena=2
        local topsign = display.newImage( group,"images/top-sign.png" )
        topsign:translate( centerX, centerY/5 )
        cambiarBotones(group,inicioEscena,5,16)
        inst = display.newText( optionsTextMenu )
        group:insert(inst)
        inst.text="Escoge el destino de Sarita"
        --if isTap==false then 
        utils.reproducirSonido("sounds/elviaje/jugar/Elviaje/Instrucciones", 0)
--        composer.removeScene( "bloqueo" )
--        introIsPlaying=true 
        --end
      
    end)    
--     composer.removeScene( "bloqueo" )
--        introIsPlaying=true    
    local btnRegresar = display.newImage( group,"images/btn-regresar.png" )
    btnRegresar:translate( centerX/4, centerY*1.85 )
    btnRegresar:addEventListener("tap", btnTap)
    btnRegresar.destination = "menuJuego"
    
   -- utils.reproducirSonido("sounds/elviaje/jugar/Elviaje/Instrucciones", 0)

end        
 ------------------------------------------------------------------------------------------

function scene:show( event )
    local group = self.view
end

function scene:hide( event )
    local group = self.view
    
    local phase = event.phase
    
    if event.phase == "will" then
        -- Called when the scene is on screen and is about to move off screen
        --
        -- INSERT code here to pause the scene
        -- e.g. stop timers, stop animation, unload sounds, etc.)
        
    elseif phase == "did" then
        -- Called when the scene is now off screen
    end 
        
        composer.removeScene( composer.getSceneName( "current" ) )    
end

function scene:destroy( event )
    -- Called prior to the removal of scene's "view" (group)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc.
    local group = self.view 
end

-------------- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
--------------

return scene



