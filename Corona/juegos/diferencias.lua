-----------------------------------------------------------------------------------------
-- diferencias.lua
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local gameState = require("utilerias.gameState")
local scene = composer.newScene()
local imagenProducto = {}
--local introIsPlaying
local lugarIzq
local lugarDer

-- Variable que indica si ganas el juego
gameState.success = 0

-- Variables obligatorias para juegos con dragging
scene.imagenSeleccionada = nil
scene.imagenColisionada = nil
scene.posIniX = nil
scene.posIniY = nil
local topsign
------VARIABLES DE GRABACION--------------
local r
local isAndroid = false
local dataFileName = "audiopapu"
if "simulator" == system.getInfo("environment") then
    dataFileName = dataFileName .. ".aif"
else
	local platformName = system.getInfo( "platformName" )
    if "iPhone OS" == platformName then
        dataFileName = dataFileName .. ".aif"
    elseif "Android" == platformName then
        dataFileName = dataFileName .. ".wav"
        isAndroid = true
    else
    	print("Unknown OS " .. platformName )
    end
end
local filePath = system.pathForFile( dataFileName, system.DocumentsDirectory )
r = media.newRecording(filePath)
--funcion para limpiar la memoria yliberar el audio
local function llamame(event)
    print("khe")
   
    audio.dispose(event.handle)
end
-------fin de las variables de grabacion---------
---------------------******Funcion obligatoria*********----
function scene:successful()
    
    --local pronuncia = utils.validarVoz(scene.imagenSeleccionada.id)
    --local pronuncia = true
    
        --[[if pronuncia then
          local r=math.random(1,2)
       local ruta
        if r==1 then
            ruta="excelente"
        else
            ruta="muybien"
        end
        utils.reproducirSonido("sounds/principales/"..ruta)]]--
        -------------------********inicio del record********----------------------
        print("presionado")
    utils.reproducirSonido("bip")
timer.performWithDelay(700, function()
   r:startRecording()
   end,1)
   
    timer.performWithDelay(3000, function() 
                    r:stopRecording()
                    utils.reproducirSonido("bup")
                    timer.performWithDelay(1000, function()
            -- Play back the recording
            local file = io.open( filePath, "r" ) 
            if file then
                io.close( file )
            local playbackSoundHandle = audio.loadStream( dataFileName, system.DocumentsDirectory )
				audio.play( playbackSoundHandle, { onComplete= llamame} )
              end 
              end,1)
                end,1)
----------**********fin de el record*****------------------------------
            timer.performWithDelay(6000, function() 
            scene.imagenSeleccionada:removeEventListener("touch")
            composer.removeScene( "bloqueo" )

            gameState.success = gameState.success+1
        
            if gameState.success >= 3 then
                timer.performWithDelay(5500, function()	
                    utils.ponerGlobos()
                end,1)
                gameState.success = 0
            end        
                    
            end,1)
        --[[else
            transition.to( scene.imagenSeleccionada , {x=scene.posIniX, y=scene.posIniY})
            
            utils.reproducirSonido("sounds/principales/intentalo",1)        
        end]]--
end

----------------------------------------------------------------------------------------
local function btnTap(event)
    --esto es para quitar el bloqueo 
    --if introIsPlaying == true then
    --local audioChannel = audio.stop() 
   --introIsPlaying = false
    --end
        utils.reproducirSonido("boton")
        composer.gotoScene ( event.target.destination, { effect = "fade"} )
        print(event.target.destination)
    return true
end 
----------------------------------------------------------------------------------------

 ------------------------------------------------------------------------------------------
-- FUNCION "SHUFFLE" PARA NÚMEROS ALEATORIOS NO REPETIDOS
-------------------------------------------------

local function shuffleArray(array)
        local arrayCount = #array
            for i = arrayCount, 2, -1 do
                local j = math.random(1, i)
                array[i], array[j] = array[j], array[i]
            end
    return array
end

-------------------------------------------------
function scene:create( event )
    
    local group = self.view
    local lugarProductoX1, lugarProductoX2
        
    -- crear rectángulo 
    local super = display.newRect( 0, 0, 100, 400 )
    super:translate( centerX+35, centerY)
    super.alpha = 0
    super.w=super.contentWidth
    super.h=super.contentHeight

    local background = display.newImage( group,path.."diferencias/fondo.png" )
    background:translate( centerX, centerY )
    background:toBack()

    matchText = display.newText(group,"", centerX, (centerY/3)*4, "fonts/GothamNarrow-Book" , 26 )
    matchText:setTextColor(0, 0, 0)
    matchText.x = centerX
    
    -- Creamos las posiciones iniciales para cada juego
    
    if gameState.tipodejuego == 1 then--la compra
           
            lugarProductoX1 = display.contentWidth/1.50
            lugarProductoX2 = display.contentWidth/1.30

            lugarDer = { 1,2 }
            lugarIzq = { 1,2 }

            lugarIzq[1] = centerY / 2.35
            lugarIzq[2] = centerY + 8

            lugarDer[1] = centerY / 1.4
            lugarDer[2] = centerY / 0.76

            ------------------------------------------------------
        
        elseif gameState.tipodejuego == 3 then -- cochinitos
            
            lugarProductoX1 = display.contentWidth/1.65
            lugarProductoX2 = display.contentWidth/1.35
            
            lugarDer = { 1 }
            lugarIzq = { 1,2 }

            lugarIzq[1] = centerY / 3.3
            lugarIzq[2] = centerY + 118

            lugarDer[1] = centerY - 40

        elseif gameState.tipodejuego == 2 then -- el viaje
            
            lugarProductoX1 = display.contentWidth/1.45
            lugarProductoX2 = display.contentWidth/1.24

            lugarDer = { 1 }
            lugarIzq = { 1,2 }

            lugarIzq[1] = centerY / 1.85
            lugarIzq[2] = centerY + 100

            lugarDer[1] = centerY - 20
        
    else                                    -- sonidos            
            lugarProductoX1 = display.contentWidth/1.43
            lugarProductoX2 = display.contentWidth/1.27

            lugarDer = { 1 }
            lugarIzq = { 1,2 }

            lugarIzq[1] = centerY / 1.57
            lugarIzq[2] = centerY + 31

            lugarDer[1] = centerY - 25

    end     
    
    
    -- Lanzamos random para que las imágenes no se encimen
    shuffleArray(lugarIzq)    
    shuffleArray(lugarDer)
        
        --  CREACIÓN DE PRODUCTOS
        
        for count = 1, 3 do
            imagenProducto[count] = display.newImage(group,path.."diferencias/"..count..".png")
            
            imagenProducto[count].myName = count
            
            if gameState.tipodejuego == 1 then--la compra
                utils.renombrarImagenes(9,imagenProducto[count])
                
                elseif gameState.tipodejuego == 3 then -- cochinitos
                    utils.renombrarImagenes(8,imagenProducto[count])
                elseif gameState.tipodejuego == 2 then -- el viaje
                    utils.renombrarImagenes(7,imagenProducto[count])
                else                                    -- sonidos
                    utils.renombrarImagenes(10,imagenProducto[count])
            end                
                       
            imagenProducto[count].w = imagenProducto[count].contentWidth
            imagenProducto[count].h = imagenProducto[count].contentHeight
           
            utils.dragging(imagenProducto[count],super,"juegos.diferencias",
                function (resultado) 
                     --esto es para quitar el bloqueo 
    --if introIsPlaying == true then
   -- local audioChannel = audio.stop() 
   --introIsPlaying = false
   -- end
                    if resultado then
                        utils.primerSonido("juegos.diferencias", scene.imagenSeleccionada.id)
                            
                        transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                    else
                        utils.reproducirSonido("error")
                        transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                    end
                end)
            
        end

            shuffleArray(imagenProducto)
            
                imagenProducto[1]:translate(lugarProductoX2,lugarIzq[2])
                imagenProducto[2]:translate(lugarProductoX1,lugarDer[1])
                imagenProducto[3]:translate(lugarProductoX2,lugarIzq[1])

        
    local btnRegresar = display.newImage( group,"images/btn-regresar.png" )
    btnRegresar:translate( centerX/4, centerY*1.85 )
    btnRegresar:addEventListener("tap", btnTap)
    btnRegresar.destination = "menuJuego"
    
  if gameState.tipodejuego == 1 then--la compra
        utils.reproducirSonido("sounds/lacompra/jugar/LasDiferencias/Instrucciones", 0,function ( ... )
            topsign.isVisible=false
            inst.isVisible=false
        end)
        
    elseif gameState.tipodejuego == 3 then -- cochinitos
       utils.reproducirSonido("sounds/cochinitos/jugar/LasDiferencias/Instrucciones", 0, function ( ... )
            topsign.isVisible=false
            inst.isVisible=false
        end)
       
    elseif gameState.tipodejuego == 2 then -- el viaje
        utils.reproducirSonido("sounds/elviaje/jugar/LasDiferencias/Instrucciones", 0,function ( ... )
            topsign.isVisible=false
            inst.isVisible=false
        end)
        
    else                                    -- sonidos
       utils.reproducirSonido("sounds/lossonidos/jugar/LasDiferencias/Instrucciones", 0,function ( ... )
            topsign.isVisible=false
            inst.isVisible=false
        end)
    end    
    --composer.removeScene( "bloqueo" )
    --introIsPlaying=true 
     topsign = display.newImage( group,"images/top-sign.png" )
        topsign:translate( centerX, centerY/5 )
    inst = display.newText( optionsTextMenu )
        group:insert(inst)
        inst.text="Arrastra al centro"
end        
 ------------------------------------------------------------------------------------------

function scene:show( event )
    local group = self.view
end

function scene:hide( event )
    local group = self.view
    
    local phase = event.phase
    
    if event.phase == "will" then
        -- Called when the scene is on screen and is about to move off screen
        --
        -- INSERT code here to pause the scene
        -- e.g. stop timers, stop animation, unload sounds, etc.)
        
    elseif phase == "did" then
        -- Called when the scene is now off screen
    end 
        
        composer.removeScene( composer.getSceneName( "current" ) )    
end

function scene:destroy( event )
    -- Called prior to the removal of scene's "view" (group)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc.
    local group = self.view 
end

-------------- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
--------------

return scene