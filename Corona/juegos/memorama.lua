-----------------------------------------------------------------------------------------
--
-- memorama.lua
--
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local gameState=require("utilerias.gameState")
local scene = composer.newScene()
local totalButtons = 0
--local introIsPlaying   
-- Variable que indica si ganas el juego
gameState.success = 0

--variables para saber cuando esta seleccionado el boton
local secondSelect = 0
local checkForMatch = false
--Arreglo de botones
local button = {}
local buttonCover = {}
local buttonImages = {1,1, 2,2, 3,3, 4,4, 5,5, 6,6}
     
local currentButton --La carta seleccionada
------VARIABLES DE GRABACION--------------
local r
local isAndroid = false
local dataFileName = "audiopapu"
if "simulator" == system.getInfo("environment") then
    dataFileName = dataFileName .. ".aif"
else
	local platformName = system.getInfo( "platformName" )
    if "iPhone OS" == platformName then
        dataFileName = dataFileName .. ".aif"
    elseif "Android" == platformName then
        dataFileName = dataFileName .. ".wav"
        isAndroid = true
    else
    	print("Unknown OS " .. platformName )
    end
end
local filePath = system.pathForFile( dataFileName, system.DocumentsDirectory )
r = media.newRecording(filePath)
--funcion para limpiar la memoria yliberar el audio
local function llamame(event)
    print("khe")
   
    audio.dispose(event.handle)
end
-------fin de las variables de grabacion---------

local function btnTap(event)
    --esto es para quitar el bloqueo 
    --if introIsPlaying == true then
    --local audioChannel = audio.stop() 
   --introIsPlaying = false
    --end
    utils.reproducirSonido("boton")
   composer.gotoScene ( event.target.destination, { effect = "fade"} )
   print(event.target.destination)
   return true
end

-----------------------------------------------------------------------------------------
function onTouch(object, event)
    --esto es para quitar el bloqueo 
    --if introIsPlaying == true then
   -- local audioChannel = audio.stop() 
   --introIsPlaying = false
   -- end
	if(event.phase == "began") then	
         
		if(checkForMatch == false and secondSelect == 0) then
			--voltear el boton seleccionado
			buttonCover[object.number].isVisible = false;
                       
			lastButton = object
                         
			checkForMatch = true		
		elseif(checkForMatch == true) then
			if(secondSelect == 0 and lastButton.number~=object.number ) then
				--voltear el segundo boton seleccionado
				buttonCover[object.number].isVisible = false
                                                
				secondSelect = 1;
				--si los botones no son los mismos entonces voltear otra ves
				if(lastButton.myName ~= object.myName) then
					timer.performWithDelay(1250, function()						
						checkForMatch = false;
						secondSelect = 0;
						buttonCover[lastButton.number].isVisible = true;
						buttonCover[object.number].isVisible = true;
                                                buttonCover[object.number].isEnable=true
                                                buttonCover[lastButton.number].isEnable=true
					end, 1)					
				--Si los botones son iguales eliminalos de pantalla
                                    elseif(lastButton.myName == object.myName) then
                                        
                                        currentButton = object
                                        
                                                                               
                                        utils.primerSonido("juegos.memorama",currentButton.id)
                                        
                                        
                                       
				end
                            else
				---	 
			end			
		end
	end
end
---------------------******Funcion obligatoria*********----
function scene:successful()
    
    --local pronuncia = utils.validarVoz(currentButton.id)
   --local pronuncia = false
    
    --[[if pronuncia == true then
       local r=math.random(1,2)
       local ruta
        if r==1 then
            ruta="excelente"
        else
            ruta="muybien"
        end
        utils.reproducirSonido("sounds/principales/"..ruta)]]--
        -------------------********inicio del record********----------------------
        print("presionado")
    utils.reproducirSonido("bip")
timer.performWithDelay(700, function()
   r:startRecording()
   end,1)
   
    timer.performWithDelay(3000, function() 
                    r:stopRecording()
                    utils.reproducirSonido("bup")
                    timer.performWithDelay(1000, function()
            -- Play back the recording
            local file = io.open( filePath, "r" ) 
            if file then
                io.close( file )
            local playbackSoundHandle = audio.loadStream( dataFileName, system.DocumentsDirectory )
				audio.play( playbackSoundHandle, { onComplete= llamame} )
              end 
              end,1)
                end,1)
----------**********fin de el record*****------------------------------
            timer.performWithDelay(6000, function()						
                    checkForMatch = false;
                    secondSelect = 0;
                    lastButton:removeSelf();
                    currentButton:removeSelf();

                    buttonCover[lastButton.number]:removeSelf();
                    buttonCover[currentButton.number]:removeSelf();
                    
                    composer.removeScene( "bloqueo" )
            
            
            gameState.success = gameState.success+1
                
                -- variable >= no de coincidencias que debe haber
            if gameState.success >= 6 then
                timer.performWithDelay(1000, function()	
                    utils.ponerGlobos()
                end,1)
                gameState.success = 0
            end 
            end, 1)
        
        --[[else
            utils.reproducirSonido("sounds/principales/intentalo",1)
            timer.performWithDelay(1250, function()						
            checkForMatch = false;
            secondSelect = 0;
            buttonCover[lastButton.number].isVisible = true;
            buttonCover[currentButton.number].isVisible = true;
    end, 1)					
    end]]--
end
----------------------------------------------------------------------------------------

function scene:create( event )
    local group = self.view
    
    local background = display.newImage( group,path.."fondo-1.png" )
    background:translate( centerX, centerY )
    background:toBack()
        
    local lastButton = display.newImage(path.."memorama/1.png",group);	
    lastButton.myName = 1;
    lastButton.isVisible=false
        
        
    x = 0

    for count = 1,4 do
	x = (centerX*.3)*count + centerX/4
	
	y=10
	for insideCount = 1,3 do
		y = (centerY*.55)*insideCount - centerY/4.5
		--Imagenes ramdom
		temp = math.random(1,#buttonImages)
		button[count] = display.newImage(group,path.."memorama/"..buttonImages[temp] .. ".png");				

		--Posicion de los botones
		button[count].x = x;
		button[count].y = y;		
		
		--Se le da un nombre al boton y un valor

		button[count].myName = buttonImages[temp]
		button[count].number = totalButtons
		 -- no debe de estar, deberia de estar al crear el objeto original
                utils.renombrarImagenes(gameState.tipodejuego,button[count])
		--se quita la imagen de la tabla
		table.remove(buttonImages, temp)
				
		--Se pone el boton de cubierta
		buttonCover[totalButtons] = display.newImage(group,path.."memorama/tapa.png")
		buttonCover[totalButtons].x = x; buttonCover[totalButtons].y = y;
		totalButtons = totalButtons + 1
		
		button[count].touch = onTouch	
		button[count]:addEventListener( "touch", button[count] )
		end
	end
            
    local btnRegresar = display.newImage( group,"images/btn-regresar.png" )
    btnRegresar:translate( centerX/4, centerY*1.85 )
    btnRegresar:addEventListener("tap", btnTap)
    btnRegresar.destination = "menuJuego"    
      
    utils.reproducirSonido("sounds/lacompra/jugar/Memorama/Instrucciones",0)  
    --composer.removeScene( "bloqueo" )
    --introIsPlaying=true 
end


function scene:show( event )
	local group = self.view 
        
end

function scene:hide( event )
	local group = self.view
	
	local phase = event.phase
	
	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
		
	elseif phase == "did" then
		-- Called when the scene is now off screen
	end	
        
         composer.removeScene( composer.getSceneName( "current" ) )
	
end

function scene:destroy( event )

	-- Called prior to the removal of scene's "view" (group)
	-- 
	-- INSERT code here to cleanup the scene
	-- e.g. remove display objects, remove touch listeners, save state, etc.
	local group = self.view
	
	
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene