local composer = require( "composer" )
local scene = composer.newScene()
local gameState = require("utilerias.gameState")

-- Variable que indica si ganas el juego
gameState.success = 0

-- Variables obligatorias para juegos con dragging
scene.imagenSeleccionada = nil
scene.imagenColisionada = nil
scene.posIniX = nil
scene.posIniY = nil
--local introIsPlaying
local groupGlobal
local arrayColores = {}
local arrayPinturas = {}
------VARIABLES DE GRABACION--------------
local r
local isAndroid = false
local dataFileName = "audiopapu"
if "simulator" == system.getInfo("environment") then
    dataFileName = dataFileName .. ".aif"
else
	local platformName = system.getInfo( "platformName" )
    if "iPhone OS" == platformName then
        dataFileName = dataFileName .. ".aif"
    elseif "Android" == platformName then
        dataFileName = dataFileName .. ".wav"
        isAndroid = true
    else
    	print("Unknown OS " .. platformName )
    end
end
local filePath = system.pathForFile( dataFileName, system.DocumentsDirectory )
r = media.newRecording(filePath)
--funcion para limpiar la memoria yliberar el audio
local function llamame(event)
    print("khe")
   
    audio.dispose(event.handle)
end
-------fin de las variables de grabacion---------

function scene:successful()
    
    --local pronuncia = utils.validarVoz(scene.imagenSeleccionada.id)
    --local pronuncia = false
    
    --[[if pronuncia then
        local r=math.random(1,2)
       local ruta
        if r==1 then
            ruta="excelente"
        else
            ruta="muybien"
        end
        utils.reproducirSonido("sounds/principales/"..ruta)]]--
        -------------------********inicio del record********----------------------
        print("presionado")
    utils.reproducirSonido("bip")
timer.performWithDelay(700, function()
   r:startRecording()
   end,1)
   
    timer.performWithDelay(3000, function() 
                    r:stopRecording()
                    utils.reproducirSonido("bup")
                    timer.performWithDelay(1000, function()
            -- Play back the recording
            local file = io.open( filePath, "r" ) 
            if file then
                io.close( file )
            local playbackSoundHandle = audio.loadStream( dataFileName, system.DocumentsDirectory )
				audio.play( playbackSoundHandle, { onComplete= llamame} )
              end 
              end,1)
                end,1)
----------**********fin de el record*****------------------------------
timer.performWithDelay(6000, function() 
        local temporal = display.newImage(groupGlobal, "images/tiranosaurio/iluminar/" .. 
                                            scene.imagenSeleccionada.id .. "-color.png",
                                            scene.imagenColisionada.x,scene.imagenColisionada.y)                           
        
        scene.imagenColisionada:removeSelf()
        scene.imagenSeleccionada:removeSelf()
        
        composer.removeScene( "bloqueo" ) 
        
        gameState.success = gameState.success+1
        
        if gameState.success >= 4 then
            timer.performWithDelay(1250, function()	
                    utils.ponerGlobos()
                end,1)
            gameState.success = 0
        end
        end,1)
    --[[else
        transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})  
        utils.reproducirSonido("sounds/principales/intentalo",1)
    end]]--

end

local function btnTap(event)
    --esto es para quitar el bloqueo 
                --if introIsPlaying == true then
                    --local audioChannel = audio.stop() 
                    --introIsPlaying = false
                --end
    utils.reproducirSonido("boton")
   composer.gotoScene ( event.target.destination, { effect = "fade"} )
   print(event.target.destination)
   return true
end

-- Called when the scene's view does not exist:
function scene:create( event )
	local group = self.view
        
        groupGlobal = group
        
        local background = display.newImage( group,"images/tiranosaurio/iluminar/fondo.png" )
        background:translate( centerX, centerY )
        
        local azul = display.newImage( group,"images/tiranosaurio/iluminar/tiranosaurio.png" )
        azul:translate( centerX/4, centerY*.65 )
        local tiranosaurio = display.newImage(group, "images/tiranosaurio/iluminar/tiranosaurio-gris.png")
        tiranosaurio:translate(centerX*1.5, centerY*1.1)
        azul.w = azul.contentWidth
        azul.h = azul.contentHeight
        tiranosaurio.w = tiranosaurio.contentWidth/22
        tiranosaurio.h = tiranosaurio.contentHeight/18
        azul.id = "tiranosaurio"
        table.insert( arrayColores, azul )
        table.insert( arrayPinturas, tiranosaurio )
            
        
        local verde = display.newImage( group,"images/tiranosaurio/iluminar/piedra.png" )
        verde:translate( centerX/3.9, centerY*1.08 )
        local pera = display.newImage(group, "images/tiranosaurio/iluminar/piedra-gris.png")
        pera:translate(centerX*1.1, centerY*1.15)
        verde.w = verde.contentWidth
        verde.h = verde.contentHeight
        pera.w = pera.contentWidth/18
        pera.h = pera.contentHeight/18
        verde.id = "piedra"
        table.insert( arrayColores, verde )
        table.insert( arrayPinturas, pera )
        
        local amarillo = display.newImage( group,"images/tiranosaurio/iluminar/cueva.png" )
        amarillo:translate( centerX/6.4, centerY*.86 )
        local sol = display.newImage(group, "images/tiranosaurio/iluminar/cueva-gris.png")
        sol:translate(centerX*1.55, centerY*.55)
        amarillo.w = amarillo.contentWidth
        amarillo.h = amarillo.contentHeight
        sol.w = sol.contentWidth/18
        sol.h = sol.contentHeight/18
        amarillo.id = "cueva"
        table.insert( arrayColores, amarillo )
        table.insert( arrayPinturas, sol )        
        
        local rojo = display.newImage( group,"images/tiranosaurio/iluminar/cerro.png" )
        rojo:translate( centerX/2.5, centerY*.73 )
        local manzana = display.newImage(group, "images/tiranosaurio/iluminar/cerro-gris.png")
        manzana:translate(centerX*1.16, centerY*.55)
        rojo.w = rojo.contentWidth
        rojo.h = rojo.contentHeight
        manzana.w = manzana.contentWidth/18
        manzana.h = manzana.contentHeight/18
        rojo.id = "cerro"
        table.insert( arrayColores, rojo )
        table.insert( arrayPinturas, manzana )  
        
        azul:toFront()
        verde:toFront()
        amarillo:toFront()
        rojo:toFront()
        
        for count = 1,4 do
            utils.dragging(arrayColores[count],arrayPinturas[count],composer.getSceneName("current"),
                function (resultado) 
                    --esto es para quitar el bloqueo 
               -- if introIsPlaying == true then
                   -- local audioChannel = audio.stop() 
                   -- introIsPlaying = false
                --end
                    if resultado then
                        utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)
                        transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenColisionada.y})
                    else
                        utils.reproducirSonido("error")
                        transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                    end
                end)
        end
        
        local btnRegresar = display.newImage( group,"images/btn-regresar.png" )
        btnRegresar:translate( centerX/4, centerY*1.85 )
        btnRegresar:addEventListener("tap", btnTap)
        btnRegresar.destination = "menuJuego"
        utils.reproducirSonido("sounds/eltiranosaurio/jugar/Dibujando/Instrucciones", 0)
        --composer.removeScene( "bloqueo" )
        --introIsPlaying=true 
        
end


-- Called immediately after scene has moved onscreen:
function scene:show( event )
	-- INSERT code here (e.g. start timers, load audio, start listeners, etc.)
	local group = self.view

end

-- Called when scene is about to move offscreen:
function scene:hide( event )
	local group = self.view
	
	composer.removeScene( composer.getSceneName( "current" ) )
	
	--Runtime:removeEventListener("enterFrame", update)
	-- INSERT code here (e.g. stop timers, remove listeners, unload sounds, etc.)
	-- Remove listeners attached to the Runtime, timers, transitions, audio tracks
end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroy( event )
	local group = self.view

	-- INSERT code here (e.g. remove listeners, widgets, save state, etc.)
	-- Remove listeners attached to the Runtime, timers, transitions, audio tracks

end

---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene---------------------------------------------------------


