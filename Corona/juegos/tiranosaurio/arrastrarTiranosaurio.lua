local composer = require( "composer" )
local gameState=require("utilerias.gameState")
local scene = composer.newScene()
-- posiciones iniciaes de botones
local posIniManzanaX,posIniManzanay =centerX*0.76, centerY*0.42
local posIniNaranjaX,posIniNaranjay =centerX*0.976, centerY*0.42 
local posIniFogataX,posIniFogatay =centerX*1.195, centerY*0.42
local posIniCocoX,posIniCocoy = centerX*1.412, centerY*0.42
local posIniRocaX,posIniRocay =centerX*1.628, centerY*0.42
local posIniPeraX,posIniPeray =centerX*1.858, centerY*0.42
--local introIsPlaying
local elementoNaranja= nil
local elementoCoco = nil
local elementoFogata= nil
local elementoManzana = nil
local elementoRoca = nil
local elementoPera = nil
local grupoImagenes = nil
--Elementos arrastrados
local manzana = nil
local naranja = nil
local fogata = nil
local coco = nil
local roca = nil
local pera = nil
------VARIABLES DE GRABACION--------------
local r
local isAndroid = false
local dataFileName = "audiopapu"
if "simulator" == system.getInfo("environment") then
    dataFileName = dataFileName .. ".aif"
else
	local platformName = system.getInfo( "platformName" )
    if "iPhone OS" == platformName then
        dataFileName = dataFileName .. ".aif"
    elseif "Android" == platformName then
        dataFileName = dataFileName .. ".wav"
        isAndroid = true
    else
    	print("Unknown OS " .. platformName )
    end
end
local filePath = system.pathForFile( dataFileName, system.DocumentsDirectory )
r = media.newRecording(filePath)
--funcion para limpiar la memoria yliberar el audio
local function llamame(event)
    print("khe")
   
    audio.dispose(event.handle)
end
-------fin de las variables de grabacion---------

-- Variable obligatoria para juegos con dragging
scene.imagenSeleccionada = nil
scene.imagenColisionada = nil
-- 
local function eliminarImagen()
    
    if scene.imagenSeleccionada.id == "manzana" then
        manzana = nil
    elseif scene.imagenSeleccionada.id == "carne" then
        naranja = nil
    elseif scene.imagenSeleccionada.id == "canasta" then
        fogata = nil
    elseif scene.imagenSeleccionada.id == "frutas" then
        coco = nil
    elseif scene.imagenSeleccionada.id == "sandia" then
        roca = nil
    elseif scene.imagenSeleccionada.id == "pera" then
        pera = nil
    end
    scene.imagenSeleccionada:removeSelf()
    scene.imagenSeleccionada=nil
    
end

local function regresarElemento()
    local posIniX,posIniy=0,0
    
    if scene.imagenSeleccionada.id == "manzana" then
        posIniX,posIniy= posIniManzanaX,posIniManzanay
    elseif scene.imagenSeleccionada.id == "carne" then
        posIniX,posIniy= posIniNaranjaX,posIniNaranjay
    elseif scene.imagenSeleccionada.id == "canasta" then
        posIniX,posIniy= posIniFogataX,posIniFogatay
    elseif scene.imagenSeleccionada.id == "frutas" then
        posIniX,posIniy= posIniCocoX,posIniCocoy
    elseif scene.imagenSeleccionada.id == "sandia" then
        posIniX,posIniy= posIniRocaX,posIniRocay
    elseif scene.imagenSeleccionada.id == "pera" then
        posIniX,posIniy= posIniPeraX,posIniPeray
    end
    
    transition.to(scene.imagenSeleccionada,{x=posIniX,y=posIniy,onComplete = eliminarImagen})      
end
---------------------******Funcion obligatoria*********----
function scene:successful()
    
    --local pronuncia = utils.validarVoz(scene.imagenSeleccionada.id)
    --local pronuncia= true
    
    scene.imagenSeleccionada:removeEventListener("touch")
    
    --[[if pronuncia then
        local r=math.random(1,2)
       local ruta
        if r==1 then
            ruta="excelente"
        else
            ruta="muybien"
        end
        utils.reproducirSonido("sounds/principales/"..ruta)]]--
        -------------------********inicio del record********----------------------
        print("presionado")
    utils.reproducirSonido("bip")
timer.performWithDelay(700, function()
   r:startRecording()
   end,1)
   
    timer.performWithDelay(3000, function() 
                    r:stopRecording()
                    utils.reproducirSonido("bup")
                    timer.performWithDelay(1000, function()
            -- Play back the recording
            local file = io.open( filePath, "r" ) 
            if file then
                io.close( file )
            local playbackSoundHandle = audio.loadStream( dataFileName, system.DocumentsDirectory )
				audio.play( playbackSoundHandle, { onComplete= llamame} )
              end 
              end,1)
                end,1)
----------**********fin de el record*****------------------------------
timer.performWithDelay(6000, function() 
        composer.removeScene( "bloqueo" ) 
        if manzana ~= nil and naranja~=nil and fogata~=nil and coco~=nil and roca~=nil and pera~=nil then
            
            timer.performWithDelay(5500, function()	
                    utils.ponerGlobos()
                end,1)
        end
        end,1)
   --[[] else
      -- Regresa las imagenes deque se arrastran a su circulo/boton de donde salieron
      regresarElemento()
      
     utils.reproducirSonido("sounds/principales/intentalo",1)
    end]]--
   
end

local function crearElemento(objeto, destino , event)
        posIniX,posIniy=event.x, event.y
        objeto:translate( event.x, event.y )
        utils.dragging(objeto,destino,"juegos.tiranosaurio.arrastrarTiranosaurio",
            function (resultado) 
                if resultado then
                    transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x,y= scene.imagenColisionada.y})

                    utils.primerSonido("juegos.tiranosaurio.arrastrarTiranosaurio", scene.imagenSeleccionada.id)
                else
                    utils.reproducirSonido("error")
                   regresarElemento()
                end
            end)
end

-----------------------------Funciones de botones-----------------------------------------
local function btnTapManzana(event)
    --esto es para quitar el bloqueo 
    --if introIsPlaying == true then
    --local audioChannel = audio.stop() 
   --introIsPlaying = false
    --end
    if event.phase == "began" then
        if (manzana == nil) then
            manzana = display.newImage(grupoImagenes,"images/tiranosaurio/arrastrar/elemento-manzana.png" )
            manzana.id="manzana"
            manzana.w=54
            manzana.h=38
            elementoManzana.w=54
            elementoManzana.h=54
            crearElemento(manzana,elementoManzana,event)
        end
    end
   return true
end
local function btnTapNaranja(event)
    --esto es para quitar el bloqueo 
    --if introIsPlaying == true then
    --local audioChannel = audio.stop() 
   --introIsPlaying = false
    --end
     if event.phase == "began" then
        if (naranja == nil) then
            naranja = display.newImage(grupoImagenes,"images/tiranosaurio/arrastrar/elemento-carne.png" )
            naranja.id="carne"
            naranja.w=46
            naranja.h=34
            elementoNaranja.w=46
            elementoNaranja.h=46
            crearElemento(naranja,elementoNaranja,event)
        end
    end
   return true
end
local function btnTapFuego(event)
    --esto es para quitar el bloqueo 
    --if introIsPlaying == true then
    --local audioChannel = audio.stop() 
   --introIsPlaying = false
    --end
    if event.phase == "began" then
            if (fogata == nil) then
                fogata = display.newImage(grupoImagenes,"images/tiranosaurio/arrastrar/elemento-canasta.png" )
                fogata.id="canasta"
                fogata.w=72
                fogata.h=42
                elementoFogata.w=72
                elementoFogata.h=72
                crearElemento(fogata,elementoFogata,event)
            end
        end
   return true
end
local function btnTapCoco(event)
    --esto es para quitar el bloqueo 
    --if introIsPlaying == true then
    --local audioChannel = audio.stop() 
   --introIsPlaying = false
   -- end
    if event.phase == "began" then
                if (coco == nil) then
                    coco = display.newImage(grupoImagenes,"images/tiranosaurio/arrastrar/elemento-frutas.png" )
                    coco.id="frutas"
                    coco.w=49
                    coco.h=45
                    elementoCoco.w=72
                    elementoCoco.h=72
                    crearElemento(coco,elementoCoco,event)
                end
    end
   return true
end
local function btnTapRoca(event)
    --esto es para quitar el bloqueo 
    --if introIsPlaying == true then
    --local audioChannel = audio.stop() 
   --introIsPlaying = false
    --end
    if event.phase == "began" then
                if (roca == nil) then
                    roca = display.newImage(grupoImagenes,"images/tiranosaurio/arrastrar/elemento-sandia.png" )
                    roca.id="sandia"
                    roca.w=114
                    roca.h=44
                    elementoRoca.w=114
                    elementoRoca.h=106
                    crearElemento(roca,elementoRoca,event)
                end
    end
   return true
end
local function btnTapPera(event)
    --esto es para quitar el bloqueo 
--    if introIsPlaying == true then
--    local audioChannel = audio.stop() 
--   introIsPlaying = false
--    end
    if event.phase == "began" then
                if (pera == nil) then
                    pera = display.newImage(grupoImagenes,"images/tiranosaurio/arrastrar/elemento-pera.png" )
                    pera.id="pera"
                    pera.w=37
                    pera.h=40
                    elementoPera.w=37
                    elementoPera.h=60
                    crearElemento(pera,elementoPera,event)
                end
    end
   return true
end

local function btnTap(event)
    --esto es para quitar el bloqueo 
--    if introIsPlaying == true then
--    local audioChannel = audio.stop() 
--   introIsPlaying = false
--    end
   utils.reproducirSonido("boton")
   composer.gotoScene ( event.target.destination, { effect = "fade"} )
   print(event.target.destination)
   return true
end



-----------------------------------------------------------------------------------------
-- Called when the scene's view does not exist:
function scene:create( event )
	local group = self.view
        grupoImagenes = group
        gameState.success = 0
        local background = display.newImage( group,"images/tiranosaurio/arrastrar/tiranosaurio-arrastra-fondo-1.png")
        background:translate( centerX, centerY )
        
        ------------------------***** Botones ****------------------------------------------------------
        
        local btnManzana = display.newImage( group,"images/tiranosaurio/arrastrar/boton-manzana.png" )
        btnManzana:translate( posIniManzanaX,posIniManzanay )
        btnManzana:addEventListener("touch", btnTapManzana)
        
        local btnNaranja = display.newImage( group,"images/tiranosaurio/arrastrar/boton-carne.png" )
        btnNaranja:translate( posIniNaranjaX,posIniNaranjay )
        btnNaranja:addEventListener("touch", btnTapNaranja)
        
        local btnFuego = display.newImage( group,"images/tiranosaurio/arrastrar/boton-canasta.png" )
        btnFuego:translate( posIniFogataX,posIniFogatay )
        btnFuego:addEventListener("touch", btnTapFuego)
        
         local btnCoco = display.newImage( group,"images/tiranosaurio/arrastrar/boton-frutas.png" )
        btnCoco:translate(posIniCocoX,posIniCocoy )
        btnCoco:addEventListener("touch", btnTapCoco)
        
        local btnRoca = display.newImage( group,"images/tiranosaurio/arrastrar/boton-sandia.png" )
        btnRoca:translate( posIniRocaX,posIniRocay )
        btnRoca:addEventListener("touch", btnTapRoca)
        
         local btnPera = display.newImage( group,"images/tiranosaurio/arrastrar/boton-pera.png" )
        btnPera:translate(  posIniPeraX,posIniPeray )
        btnPera:addEventListener("touch", btnTapPera)
      ------------------------***** Botones ****------------------------------------------------------
        
      ------------------------***** elementos ****------------------------------------------------------
        elementoNaranja = display.newImage( group,"images/tiranosaurio/arrastrar/elemento-carne-n.png" )
        elementoNaranja:translate( centerX*0.476, centerY*1.527 )
        
        elementoPera = display.newImage( group,"images/tiranosaurio/arrastrar/elemento-pera-n.png" )
        elementoPera:translate( centerX*0.667, centerY*1.352 )
        
        elementoManzana = display.newImage( group,"images/tiranosaurio/arrastrar/elemento-manzana-n.png" )
        elementoManzana:translate( centerX*0.839, centerY*1.28 )
        
        elementoCoco = display.newImage( group,"images/tiranosaurio/arrastrar/elemento-frutas-n.png" )
        elementoCoco:translate( centerX*1.094, centerY*1.5 )
        
        elementoFogata = display.newImage( group,"images/tiranosaurio/arrastrar/elemento-canasta-n.png" )
        elementoFogata:translate( centerX*1.432, centerY*1.432 )
        
        elementoRoca = display.newImage( group,"images/tiranosaurio/arrastrar/elemento-sandia-n.png" )
        elementoRoca:translate( centerX*1.8, centerY*1.39 )
      ------------------------***** elementos ****------------------------------------------------------
        
        local btnRegresar = display.newImage( group,"images/btn-regresar.png" )
        btnRegresar:translate( centerX/4, centerY*1.85 )
        btnRegresar:addEventListener("tap", btnTap)
        btnRegresar.destination = "menuJuego"
        utils.reproducirSonido("sounds/eltiranosaurio/jugar/Cueva/Instrucciones", 0)
--        composer.removeScene( "bloqueo" )
--        introIsPlaying=true 
        
end


-- Called immediately after scene has moved onscreen:
function scene:show( event )
	-- INSERT code here (e.g. start timers, load audio, start listeners, etc.)
	local group = self.view

end

-- Called when scene is about to move offscreen:
function scene:hide( event )
	local group = self.view
	
	composer.removeScene( composer.getSceneName( "current" ) )
	
	--Runtime:removeEventListener("enterFrame", update)
	-- INSERT code here (e.g. stop timers, remove listeners, unload sounds, etc.)
	-- Remove listeners attached to the Runtime, timers, transitions, audio tracks
end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroy( event )
        
	local group = self.view
        grupoImagenes:removeSelf()
        grupoImagenes=nil
	-- INSERT code here (e.g. remove listeners, widgets, save state, etc.)
	-- Remove listeners attached to the Runtime, timers, transitions, audio tracks

end

---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene---------------------------------------------------------

