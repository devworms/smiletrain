--------------------------------------------------------------------------------
-- rompecabezas.lua
--------------------------------------------------------------------------------

local composer = require( "composer" )
local gameState = require("utilerias.gameState")
local scene = composer.newScene()
--local introIsPlaying
local imagenOpaca = {}
local imagenPieza = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16}
local imagenColision = {}


local coco, manzana, tirano, carne = 0,0,0,0

-- Variable que indica si ganas el juego
gameState.success = 0

-- Variables obligatorias para juegos con dragging
scene.imagenSeleccionada = nil
scene.imagenColisionada = nil
scene.posIniX = nil
scene.posIniY = nil
------VARIABLES DE GRABACION--------------
local r
local isAndroid = false
local dataFileName = "audiopapu"
if "simulator" == system.getInfo("environment") then
    dataFileName = dataFileName .. ".aif"
else
	local platformName = system.getInfo( "platformName" )
    if "iPhone OS" == platformName then
        dataFileName = dataFileName .. ".aif"
    elseif "Android" == platformName then
        dataFileName = dataFileName .. ".wav"
        isAndroid = true
    else
    	print("Unknown OS " .. platformName )
    end
end
local filePath = system.pathForFile( dataFileName, system.DocumentsDirectory )
r = media.newRecording(filePath)
--funcion para limpiar la memoria yliberar el audio
local function llamame(event)
    print("khe")
   
    audio.dispose(event.handle)
end
-------fin de las variables de grabacion---------

---------------------******Funcion obligatoria*********----
function scene:successful()

    --local pronuncia = utils.validarVoz(scene.imagenSeleccionada.id)
    --local pronuncia = true
    
        --[[if pronuncia then
            local r=math.random(1,2)
       local ruta
        if r==1 then
            ruta="excelente"
        else
            ruta="muybien"
        end
        utils.reproducirSonido("sounds/principales/"..ruta)]]--
        -------------------********inicio del record********----------------------
        print("presionado")
    utils.reproducirSonido("bip")
timer.performWithDelay(700, function()
   r:startRecording()
   end,1)
   
    timer.performWithDelay(3000, function() 
                    r:stopRecording()
                    utils.reproducirSonido("bup")
                    timer.performWithDelay(1000, function()
            -- Play back the recording
            local file = io.open( filePath, "r" ) 
            if file then
                io.close( file )
            local playbackSoundHandle = audio.loadStream( dataFileName, system.DocumentsDirectory )
				audio.play( playbackSoundHandle, { onComplete= llamame} )
              end 
              end,1)
                end,1)
----------**********fin de el record*****------------------------------
      timer.performWithDelay(6000, function() 
            composer.removeScene( "bloqueo" )

            gameState.success = gameState.success+1
            
            if gameState.success >= 4 then
                timer.performWithDelay(5500, function()	
                    utils.ponerGlobos()
                end,1)
                gameState.success = 0
            end   
            end,1)

        --[[else
            
            transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
            scene.imagenSeleccionada:addEventListener( "touch", scene.imagenSeleccionada )
            
          utils.reproducirSonido("sounds/principales/intentalo",1)   
        end]]--
        
end

 ------------------------------------------------------------------------------------------
-- FUNCION "SHUFFLE" PARA NÚMEROS ALEATORIOS NO REPETIDOS
-------------------------------------------------

local function shuffleArray(array)
        local arrayCount = #array
            for i = arrayCount, 2, -1 do
                local j = math.random(1, i)
                array[i], array[j] = array[j], array[i]
            end
    return array
end

----------------------------------------------------------------------------------------
local function btnTap(event)
    --esto es para quitar el bloqueo 
                --if introIsPlaying == true then
                    --local audioChannel = audio.stop() 
                    --introIsPlaying = false
                --end
    utils.reproducirSonido("boton")
        composer.gotoScene ( event.target.destination, { effect = "fade"} )
        print(event.target.destination)
    return true
end 
----------------------------------------------------------------------------------------

function scene:create( event )
    
    local group = self.view
    
    local background = display.newImage( group,path.."rompecabezas/tiranosaurio-rompecabezas-fondo-1.png" )
    background:translate( centerX, centerY )
    background:toBack()

    matchText = display.newText(group,"", centerX, (centerY/3)*4, "fonts/GothamNarrow-Book" , 26 )
    matchText:setTextColor(0, 0, 0)
    matchText.x = centerX
            
         -----------  CREACIÓN DE FIGURAS OPACAS -------------------------------------
        local imgWidthPar = centerX/1.6
        local imgWidthNon = centerX/7
        local imgHeightPar = centerY / 4 
        local imgHeightNon = centerY * 1.05
        
            for count = 1, 16 do

                imagenOpaca[count] = display.newImage(group,path.."rompecabezas/piezas-opacas/"..count..".png")
                imagenOpaca[count]:translate( imgWidthNon, imgHeightPar )

                imgWidthPar = imgWidthPar + 110
                imgWidthNon = imgWidthNon + 110
                                
                if(count%4 == 0) then
                    imgHeightPar = imgHeightPar + 110
                    imgHeightNon = imgHeightNon + 110
                    imgWidthPar = imgWidthPar - 460
                    imgWidthNon = imgWidthNon - 460
                end
                
                if(count == 2 or count == 6 or count == 10 or count == 14)  then
                    imgWidthNon = imgWidthNon + 20
                end
                
                if(count == 8)  then
                    imgHeightPar = imgHeightPar + 20
                    imgHeightNon = imgHeightNon + 20
                end
                
                imagenOpaca[count].w = imagenOpaca[count].contentWidth/2.5
                imagenOpaca[count].h = imagenOpaca[count].contentHeight/2.5

            end
         
     
        -----------  CREACIÓN DE PIEZAS DE ROMPECABEZAS -------------------------------------
        local imgWidthPar = display.contentWidth/1.75
        local imgHeightPar = centerY / 4
                
        -- hacemos random un las posiciones de las piezas        
        shuffleArray(imagenPieza)
        
            for posicion, pieza in ipairs (imagenPieza) do
            
                    imagenPieza[posicion] = display.newImage(group,path.."rompecabezas/piezas-dino/".. pieza ..".png")
                    imagenPieza[posicion]:translate( imgWidthPar, imgHeightPar )
                    
                    table.insert( imagenColision, {id = pieza, value = posicion} )
                    
                    imgWidthPar = imgWidthPar + 122   

                    if(posicion%4 == 0) then
                        imgHeightPar = imgHeightPar + 115
                        imgWidthPar = imgWidthPar - 488
                    end 
                    
                    imagenPieza[posicion].w = imagenPieza[posicion].contentWidth/2.5
                    imagenPieza[posicion].h = imagenPieza[posicion].contentHeight/2.5
                    
                -- poner los id    
                if pieza==1 or pieza==2 or pieza==5 or pieza==6 then -- coco    
                    imagenPieza[posicion].id= "come sandia"
                elseif pieza==3 or pieza==4 or pieza==7 or pieza==8 then -- manzana
                    imagenPieza[posicion].id= "come manzana"
                elseif pieza==9 or pieza==10 or pieza==13 or pieza==14 then -- tirano
                    imagenPieza[posicion].id= "tiranosaurio"
                else -- carne
                    imagenPieza[posicion].id= "come carne"
                end
                    
                    
            end
            
        --  Ordenar array de menor a mayor por pieza 
        --  para preparar su colision
        table.sort(imagenColision, function(a,b) return a.id < b.id end)  
            
    -----------  CREACIÓN DE DRAG CON COLISIONES -------------------------------------
    
        for posicion, value in ipairs (imagenColision) do
            --print("pisicion: "..posicion.." value: "..value.value.." id: "..value.id)
            --drag para memorama
            utils.dragging(imagenPieza[value.value], imagenOpaca[posicion], composer.getSceneName("current"),
                function (resultado) 
                --esto es para quitar el bloqueo 
               -- if introIsPlaying == true then
                    --local audioChannel = audio.stop() 
                    --introIsPlaying = false
               -- end
                    if resultado then
                        scene.imagenSeleccionada:removeEventListener("touch")
                        transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenColisionada.y})
                        
                        if scene.imagenSeleccionada.id == "come sandia" then
                            utils.reproducirSonido("correct")
                            coco = coco+1
                            if coco >= 4 then
                                utils.primerSonido("juegos.tiranosaurio.rompecabezas", scene.imagenSeleccionada.id)
                            end
                        elseif scene.imagenSeleccionada.id == "tiranosaurio" then
                            utils.reproducirSonido("correct")
                            tirano = tirano+1
                            if tirano >= 4 then
                                utils.primerSonido("juegos.tiranosaurio.rompecabezas", scene.imagenSeleccionada.id)
                            end
                        elseif scene.imagenSeleccionada.id == "come manzana" then
                            utils.reproducirSonido("correct")
                            manzana = manzana+1
                            if manzana >= 4 then
                                utils.primerSonido("juegos.tiranosaurio.rompecabezas", scene.imagenSeleccionada.id)
                            end
                        else
                            utils.reproducirSonido("correct")
                            carne = carne+1
                            if carne >= 4 then
                                utils.primerSonido("juegos.tiranosaurio.rompecabezas", scene.imagenSeleccionada.id)
                            end
                        end
                        
                    else
                        utils.reproducirSonido("error")
                        transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                    end
                end)
        end
    
    
    local btnRegresar = display.newImage( group,"images/btn-regresar.png" )
    btnRegresar:translate( centerX/4, centerY*1.85 )
    btnRegresar:addEventListener("tap", btnTap)
    btnRegresar.destination = "menuJuego"

      utils.reproducirSonido("sounds/eltiranosaurio/jugar/Rompecabeza/Instrucciones", 0)
      --composer.removeScene( "bloqueo" )
      --introIsPlaying=true 
    
end       

function scene:show( event )
    local group = self.view
end

function scene:hide( event )
    local group = self.view
    
    local phase = event.phase
    
    if event.phase == "will" then
        -- Called when the scene is on screen and is about to move off screen
        --
        -- INSERT code here to pause the scene
        -- e.g. stop timers, stop animation, unload sounds, etc.)
        
    elseif phase == "did" then
        -- Called when the scene is now off screen
    end 
        
        composer.removeScene( composer.getSceneName( "current" ) )    
end

function scene:destroy( event )
    -- Called prior to the removal of scene's "view" (group)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc.
    local group = self.view 
end

-------------- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
--------------

return scene