local composer = require( "composer" )
local scene = composer.newScene()
local musicafondochanel=nil
local nube

local function btnTap(event)
    audio.stop(musicafondochanel)  
    audio.dispose(musicafondochanel)
    musicafondochanel=nil
    utils.reproducirSonido("boton")
    composer.gotoScene ( event.target.destination, { effect = "fade"} )
   
   print(event.target.destination)
   return true
end



function scene:create( event )
    local group = self.view
    
 
    
    local background = display.newImage( group,"images/fondo.png" )
    background:translate( centerX, centerY )
    
    local titulo = display.newImage( group,"images/menu/info_titulo.png" )
    titulo:translate (centerX, centerY/4)
    local titulo = display.newImage( group,"images/menu/Info_nombres.png" )
    titulo:translate (centerX, centerY*1.2)
     local btnRegresar = display.newImage( group,"images/btn-regresar.png" )
        btnRegresar:translate( centerX/4, centerY*1.85 )
        btnRegresar:addEventListener("tap", btnTap)
        btnRegresar.destination = "home"

    local musicafondo =audio.loadStream( "sounds/sounds/home/fondo.mp3" )
     musicafondochanel= audio.play( musicafondo, { channel=audioChannel, loops= -1 }  ) 
     
   
    
end
 
-- Called immediately after scene has moved onscreen:
function scene:show( event )
	local group = self.view
	-- INSERT code here (e.g. start timers, load audio, start listeners, etc.)
        
end
 
-- Called when scene is about to move offscreen:
function scene:hide( event )
	local group = self.view
        
       
        composer.removeScene( composer.getSceneName( "current" ) )
 	collectgarbage()
	-- INSERT code here (e.g. stop timers, remove listeners, unload sounds, etc.)
	-- Remove listeners attached to the Runtime, timers, transitions, audio tracks
 
end
 
-- Called prior to the removal of scene's "view" (display group)
function scene:destroy( event )
	local group = self.view

	-- INSERT code here (e.g. remove listeners, widgets, save state, etc.)
	-- Remove listeners attached to the Runtime, timers, transitions, audio tracks
 

 end
---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------
 
-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

return scene

