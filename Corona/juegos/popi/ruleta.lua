local composer = require( "composer" )
local gameState=require("utilerias.gameState")
local scene = composer.newScene()
 --local introIsPlaying
  gameState.success = 0
    local pres=false
    local contadordevueltas=0
    local velocidad=5
    local objeto
    local para=0
    local paravelo=0
    local posanterior=0
    local objetosSalidos={}
    local numeroDeGiros=0
    local ruleta
    local bandera=false
    ------VARIABLES DE GRABACION--------------
local r
local isAndroid = false
local dataFileName = "audiopapu"
if "simulator" == system.getInfo("environment") then
    dataFileName = dataFileName .. ".aif"
else
	local platformName = system.getInfo( "platformName" )
    if "iPhone OS" == platformName then
        dataFileName = dataFileName .. ".aif"
    elseif "Android" == platformName then
        dataFileName = dataFileName .. ".wav"
        isAndroid = true
    else
    	print("Unknown OS " .. platformName )
    end
end
local filePath = system.pathForFile( dataFileName, system.DocumentsDirectory )
r = media.newRecording(filePath)
--funcion para limpiar la memoria yliberar el audio
local function llamame(event)
    print("khe")
   
    audio.dispose(event.handle)
end
-------fin de las variables de grabacion---------
    
    
local function btnTapJugar(event)
    --esto es para quitar el bloqueo 
--    if introIsPlaying == true then
--    local audioChannel = audio.stop() 
--   introIsPlaying = false
--    end
   composer.showOverlay( "bloqueo" ,{ isModal = true } )
   pres=true
    aleatorio=math.random(1, 8)--Escoge el objeto aleatorio que caera en la ruleta
   contadordevueltas=0
   velocidad=5
   posanterior= ruleta.rotation
   para=0
   paravelo=0
   
    
end

function gira()
    
    if pres== true then
        ruleta.rotation=ruleta.rotation +velocidad
        contadordevueltas=contadordevueltas+velocidad
       
        if aleatorio==1 then -- Pone el objeto que cayo asi como su grados de rotacion y su velocidad de parada
            objeto= "tina"
            para=10
            paravelo=2
        elseif aleatorio==2 then
             objeto= "cepillo"
              para=55
              paravelo=2
        elseif aleatorio==3 then
            objeto= "plato"
             para=95
             paravelo=2
        elseif aleatorio==4 then
            objeto= "toalla"
             para=135
             paravelo=2.5
        elseif aleatorio==5 then
            objeto= "palo"
             para=175
             paravelo=2.5
        elseif aleatorio==6 then
            objeto= "hueso"
             para=235
             paravelo=3
        elseif aleatorio==7 then
            objeto= "pelota"
             para=275
             paravelo=3
        elseif aleatorio==8 then
            objeto= "croquetas"
             para=320
             paravelo=3
            
        end
        for i= 0,posanterior do -- Resta la posicion anterior del objeto que cayo para que estos concuerden
            if para<=0 then
                para=360
            end
            para=para-1
        end
        print("Objeto que cayo "..objeto)
        --print("Valor Para "..para)
        --print("Valor Pos anterior"..posanterior)
    end
    if contadordevueltas>=360 and contadordevueltas<360*2 then--a lenta la ruleta
         velocidad= paravelo
    elseif contadordevueltas>=360*2 + para  then-- Detiene la ruleta
        pres=false
        for index, objetodelista in ipairs(objetosSalidos) do --pseudocode
            print("Objetos en la lista "..objetodelista)
            if objetodelista==objeto then

            bandera=true
            end
        end
       
           
           
      
       if bandera==true then
           bandera=false
           utils.reproducirSonido("error",1)
            
       else
           numeroDeGiros=numeroDeGiros+1
           utils.primerSonido("juegos.popi.ruleta",objeto)
       end
       
        contadordevueltas=0
    end
end

local function btnTap(event)
    --esto es para quitar el bloqueo 
--    if introIsPlaying == true then
--    local audioChannel = audio.stop() 
--   introIsPlaying = false
--    end
    utils.reproducirSonido("boton")
   composer.gotoScene ( event.target.destination, { effect = "fade"} )
   print(event.target.destination)
   return true
end
function scene:successful() 
    --local pronuncia = utils.validarVoz(objeto)
   -- local pronuncia = true
    
    --[[if pronuncia == true then
        local r=math.random(1,2)
       local ruta
        if r==1 then
            ruta="excelente"
        else
            ruta="muybien"
        end
        utils.reproducirSonido("sounds/principales/"..ruta)]]--
        -------------------********inicio del record********----------------------
        print("presionado")
    utils.reproducirSonido("bip")
timer.performWithDelay(700, function()
   r:startRecording()
   end,1)
   
    timer.performWithDelay(3000, function() 
                    r:stopRecording()
                    utils.reproducirSonido("bup")
                    timer.performWithDelay(1000, function()
            -- Play back the recording
            local file = io.open( filePath, "r" ) 
            if file then
                io.close( file )
            local playbackSoundHandle = audio.loadStream( dataFileName, system.DocumentsDirectory )
				audio.play( playbackSoundHandle, { onComplete= llamame} )
              end 
              end,1)
                end,1)
----------**********fin de el record*****------------------------------
timer.performWithDelay(6000, function() 
        objetosSalidos[numeroDeGiros]=objeto
        composer.removeScene( "bloqueo" )
         gameState.success = gameState.success+1
                
                -- variable >= no de coincidencias que debe haber
            if gameState.success >= 3 then
               timer.performWithDelay(1250, function()	
                    utils.ponerGlobos()
                end,1)
               gameState.success = 0
            end
            end,1)
    --[[else
       utils.reproducirSonido("sounds/principales/intentalo",1)
    end]]--
end
function scene:create( event )
     local group = self.view
      local background = display.newImage( group,path.."ruleta/popi-ruleta-fondo.png" )
    background:translate( centerX, centerY )
    background:toBack()
     
    ruleta=display.newImage(group,path.."ruleta/ruleta.png")
    ruleta:translate( centerX+(centerX/3), centerY-(centerY/6) )
    ruleta.rotation=0
    local flecha=display.newImage(group, path.."ruleta/indicador-ruleta.png")
    flecha:translate( centerX+(centerX/3), centerY-(centerY/3) )
    flecha:toFront()
    
    local btnJugar=  display.newImage(group,path.."ruleta/boton-jugar-1.png")
    btnJugar:translate( (centerX/9.5)*4, centerY+((centerY/5)*2) )
    btnJugar:addEventListener("tap", btnTapJugar)
    
    local btnRegresar = display.newImage( group,"images/btn-regresar.png" )
    btnRegresar:translate( centerX/4, centerY*1.85 )
    btnRegresar:addEventListener("tap", btnTap)
    btnRegresar.destination = "menuJuego"    
     
    utils.reproducirSonido("sounds/popi/jugar/Ruleta/Instrucciones", 0) 
    --composer.removeScene( "bloqueo" )
    --introIsPlaying=true 
     Runtime:addEventListener("enterFrame", gira )
end



function scene:show( event )
	local group = self.view 
        
end
function scene:hide( event )
	local group = self.view
	
	local phase = event.phase
	
	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
		
	elseif phase == "did" then
		-- Called when the scene is now off screen
	end	
        
         composer.removeScene( composer.getSceneName( "current" ) )
	
end

function scene:destroy( event )

	-- Called prior to the removal of scene's "view" (group)
	-- 
	-- INSERT code here to cleanup the scene
	-- e.g. remove display objects, remove touch listeners, save state, etc.
	local group = self.view
	
	
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene
