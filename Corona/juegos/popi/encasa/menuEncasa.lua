local composer = require( "composer" )
local gameState=require("utilerias.gameState")
local scene = composer.newScene()
local subDirectory = nil
--local introIsPlaying

local function btnTap(event)
    --esto es para quitar el bloqueo 
--    if introIsPlaying == true then
--    local audioChannel = audio.stop() 
--   introIsPlaying = false
--    end
    
    utils.tipoImagen()
     
    if event.target.name == "patio" then
        gameState.tipodeSubJuego = 1
    elseif event.target.name == "banio" then
        gameState.tipodeSubJuego = 2
    elseif event.target.name == "cocina" then
        gameState.tipodeSubJuego = 3
    end

    utils.reproducirSonido("boton")
    composer.gotoScene ( event.target.destination, { effect = "fade"} )
    print(event.target.destination)
    return true
end

-- Called when the scene's view does not exist:
function scene:create( event )
	local group = self.view
        subDirectory="juegos.popi.encasa"
        local background = display.newImage( group,path.."escenarios/sala-presentacion/fondo-sala.png" )
        background:translate( centerX, centerY )
        
        local btnJ1 = display.newImage( group,path.."escenarios/sala-presentacion/btn-sala_patio.png" )
        btnJ1:translate( centerX*1.17, centerY*1.40 )
        btnJ1:addEventListener("tap", btnTap)
        
        btnJ1.destination = subDirectory..".encasa"
        btnJ1.name = "patio"
        
        local btnJ2 = display.newImage( group,path.."escenarios/sala-presentacion/btn-sala_banio.png" )
        btnJ2:translate( centerX*1.49, centerY*1.40 )
        btnJ2:addEventListener("tap", btnTap)
        btnJ2.destination = subDirectory..".encasa"
        btnJ2.name = "banio"
        
        local btnJ3 = display.newImage( group,path.."escenarios/sala-presentacion/btn-sala_cocina.png" )
        btnJ3:translate( centerX*1.81, centerY*1.40 )
        btnJ3:addEventListener("tap", btnTap)
        btnJ3.destination = subDirectory..".encasa"
        btnJ3.name = "cocina"
        
        local btnRegresar = display.newImage( group,"images/btn-regresar.png" )
        btnRegresar:translate( centerX/4, centerY*1.85 )
        btnRegresar:addEventListener("tap", btnTap)
        btnRegresar.destination = "menuJuego"
        
        row = utils.obtenerPreferencesJuego("popi")
        if  row.btn1== 'false' and row.btn2== 'false' and row.btn3== 'false'  then
                    utils.resetearMiniJuego("popi")
                    utils.ponerGlobos()          
                else
                    utils.reproducirSonido("sounds/popi/jugar/EnCasa/Instrucciones", 0)
                    --composer.removeScene( "bloqueo" )
                    --introIsPlaying=true 
        end
            
      
end


-- Called immediately after scene has moved onscreen:
function scene:show( event )
	-- INSERT code here (e.g. start timers, load audio, start listeners, etc.)
	local group = self.view
        
end

-- Called when scene is about to move offscreen:
function scene:hide( event )
	local group = self.view
                
	composer.removeScene( composer.getSceneName( "current" ) )
	
	--Runtime:removeEventListener("enterFrame", update)
	-- INSERT code here (e.g. stop timers, remove listeners, unload sounds, etc.)
	-- Remove listeners attached to the Runtime, timers, transitions, audio tracks
end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroy( event )
	local group = self.view

	-- INSERT code here (e.g. remove listeners, widgets, save state, etc.)
	-- Remove listeners attached to the Runtime, timers, transitions, audio tracks

end

---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene---------------------------------------------------------
