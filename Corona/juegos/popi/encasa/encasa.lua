-----------------------------------------------------------------------------------------
-- encasa.lua
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local gameState = require("utilerias.gameState")
local scene = composer.newScene()
local imagenJuguete = {}
local imagenArrastradas={}
--local introIsPlaying

-- Variable que indica si ganas el juego
gameState.success = 0

-- Variables obligatorias para juegos con dragging
scene.imagenSeleccionada = nil
scene.imagenColisionada = nil
scene.posIniX = nil
scene.posIniY = nil
---------------------------
local subDirectory = nil
local numeroTipoJuego = 0
local carpeta= nil
local background = nil
local cuadroColisionX,cuadroColisionY=0,0
local imagenPerro=nil
local perro= nil
local grupoImagenes = nil
------VARIABLES DE GRABACION--------------
local r
local isAndroid = false
local dataFileName = "audiopapu"
if "simulator" == system.getInfo("environment") then
    dataFileName = dataFileName .. ".aif"
else
	local platformName = system.getInfo( "platformName" )
    if "iPhone OS" == platformName then
        dataFileName = dataFileName .. ".aif"
    elseif "Android" == platformName then
        dataFileName = dataFileName .. ".wav"
        isAndroid = true
    else
    	print("Unknown OS " .. platformName )
    end
end
local filePath = system.pathForFile( dataFileName, system.DocumentsDirectory )
r = media.newRecording(filePath)
--funcion para limpiar la memoria yliberar el audio
local function llamame(event)
    print("khe")
   
    audio.dispose(event.handle)
end
-------fin de las variables de grabacion---------
local function eliminarImagenes(imagen)
    scene.imagenSeleccionada=nil
    imagen:removeSelf()
    imagen=nil
end

---------------------******Funcion obligatoria*********----
function scene:successful()
    
    --[[local pronuncia = utils.validarVoz(scene.imagenSeleccionada.id)

        if pronuncia then
            local r=math.random(1,2)
       local ruta
        if r==1 then
            ruta="excelente"
        else
            ruta="muybien"
        end
        utils.reproducirSonido("sounds/principales/"..ruta,1,function ( ... )]]--
        -------------------********inicio del record********----------------------
        print("presionado")
    utils.reproducirSonido("bip")
timer.performWithDelay(700, function()
   r:startRecording()
   end,1)
   
    timer.performWithDelay(3000, function() 
                    r:stopRecording()
                    utils.reproducirSonido("bup")
                    timer.performWithDelay(1000, function()
            -- Play back the recording
            local file = io.open( filePath, "r" ) 
            if file then
                io.close( file )
            local playbackSoundHandle = audio.loadStream( dataFileName, system.DocumentsDirectory )
				audio.play( playbackSoundHandle, { onComplete= llamame} )
              end 
              end,1)
                end,1)
----------**********fin de el record*****------------------------------
           timer.performWithDelay(6000, function() 
           scene.imagenSeleccionada:removeEventListener("touch")
            composer.removeScene( "bloqueo" )
            
            imagenJuguete[ tonumber(scene.imagenSeleccionada.myName)]:removeSelf()
            imagenJuguete[ tonumber(scene.imagenSeleccionada.myName)]=nil
            
            scene.imagenSeleccionada:removeSelf()
            scene.imagenSeleccionada=nil
            gameState.success = gameState.success+1
        
            if gameState.success >= 4 then
                 utils.actualizarPreferenciaJuego("btn"..tostring(gameState.tipodeSubJuego), 'false', 'popi')
                
                composer.gotoScene ( "juegos.popi.encasa.menuEncasa", { effect = "fade"} )
                gameState.success = 0
            end        
           end,1)
        --end)

            
        --[[else
           -- transition.to( scene.imagenSeleccionada , {x=scene.posIniX, y=scene.posIniY})
            eliminarImagenes(scene.imagenSeleccionada)
            
       utils.reproducirSonido("sounds/principales/intentalo",1)     
        end]]--
end

local function acomodarImagenes()
      if gameState.tipodeSubJuego == 1 then --patio
        imagenJuguete[1]:translate( centerX*0.31, centerY*0.68 )--pelota
        imagenJuguete[2]:translate( centerX*0.314, centerY*1.15 )--cuerda
        imagenJuguete[3]:translate( centerX*0.60, centerY*0.68 )--disco
        imagenJuguete[4]:translate( centerX*0.60, centerY*1.15 )--hueso
        utils.reproducirSonido("sounds/popi/jugar/EnCasa/patio/Instrucciones", 0,function ( ... )
             topsign.isVisible=false
            inst.isVisible=false
        end)

    elseif gameState.tipodeSubJuego == 2 then --banio
        imagenJuguete[1]:translate( centerX*0.255, centerY*0.47 )--shampoo
        imagenJuguete[2]:translate( centerX*0.255, centerY*0.98 )--cepillo
        imagenJuguete[3]:translate( centerX*0.55, centerY*0.46 )--jabon
        imagenJuguete[4]:translate( centerX*0.55, centerY*0.99 )--toalla
        utils.reproducirSonido("sounds/popi/jugar/EnCasa/bano/Instrucciones", 0, function ( ... )
             topsign.isVisible=false
            inst.isVisible=false
        end)

    elseif gameState.tipodeSubJuego == 3 then --cocina
        imagenJuguete[1]:translate( centerX*0.224, centerY*0.35 )--croquetas
        imagenJuguete[2]:translate( centerX*0.224, centerY*0.85 )--salchicha
        imagenJuguete[3]:translate( centerX*0.51, centerY*0.35 )--plato
        imagenJuguete[4]:translate( centerX*0.51, centerY*0.85 )--galleta
        utils.reproducirSonido("sounds/popi/jugar/EnCasa/cocina/Instrucciones", 0,function ( ... )
             topsign.isVisible=false
            inst.isVisible=false
        end)
    end
--    composer.removeScene( "bloqueo" )
--    introIsPlaying=true 
end
----------------------------------------------------------------------------------------
local function btnTap(event)
--    --esto es para quitar el bloqueo 
--    if introIsPlaying == true then
--    local audioChannel = audio.stop() 
--   introIsPlaying = false
--    end
        utils.reproducirSonido("boton")
        composer.gotoScene ( event.target.destination, { effect = "fade"} )
        print(event.target.destination)
    return true
end 
----------------------------------------------------------------------------------------
local function configuracion(group)
    --inicializacion del arreglo
   
    if gameState.tipodeSubJuego == 1 then --patio
        carpeta= "patio"
        background= path.."escenarios/patio/fondo-jardin.png"   
        numeroTipoJuego=11
        cuadroColisionX, cuadroColisionY= centerX*1.28, centerY*1.37
        imagenPerro= path.."escenarios/patio/perro.png"   
    elseif gameState.tipodeSubJuego == 2 then --banio
        carpeta= "banio"
        background= path.."escenarios/banio/fondo-banio.png" 
        numeroTipoJuego=12
        cuadroColisionX, cuadroColisionY= centerX*1.60, centerY*0.90
        imagenPerro= path.."escenarios/banio/perro.png"   
        local baniera = display.newImage( group, path.."escenarios/"..carpeta.."/baniera.png")
        baniera:translate( centerX, centerY )
    elseif gameState.tipodeSubJuego == 3 then --cocina
        carpeta= "cocina"
        background= path.."escenarios/cocina/fondo-cocina.png" 
        numeroTipoJuego=13
        cuadroColisionX, cuadroColisionY= centerX*1.45, centerY*1.42
        imagenPerro= path.."escenarios/cocina/perro.png"   
        
    end
    
end

local function onTouch(event)
    --esto es para quitar el bloqueo 
--    if introIsPlaying == true then
--    local audioChannel = audio.stop() 
--   introIsPlaying = false
--    end
    if event.phase == "began" then
        if not utils.contains(imagenArrastradas, event.target.myName) then
            
            local imagen = path.."escenarios/"..carpeta.."/"..event.target.myName..".png" 

            local imagenArrastrada = display.newImage(grupoImagenes,imagen)
                  imagenArrastrada:translate( event.x, event.y )
                  imagenArrastrada.w=imagenArrastrada.contentWidth
                  imagenArrastrada.h=imagenArrastrada.contentHeight
                  imagenArrastrada.myName=event.target.myName
                 utils.renombrarImagenes(numeroTipoJuego,imagenArrastrada)
            utils.dragging(imagenArrastrada,perro,subDirectory,
            function (resultado) 
                if resultado then
                    utils.primerSonido(subDirectory, scene.imagenSeleccionada.id)
                    transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                else
                    utils.reproducirSonido("error")
                    imagenArrastrada:removeEventListener("touch")
                    transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY, onComplete = eliminarImagenes(imagenArrastrada)})

                end
            end)
        end
    end
end

----------------------------------------------------------------------------------------
function scene:create( event )
    subDirectory="juegos.popi.encasa.encasa"
    local group = self.view
    grupoImagenes= group
    configuracion(group)    
    -- crear rectángulo 
    --[[local perro = display.newRect( 0, 0, 73, 114 )
    perro.alpha = 100]]
    
    perro = display.newImage( group, imagenPerro)
    
    perro:translate(cuadroColisionX,cuadroColisionY)
    perro.w=perro.contentWidth
    perro.h=perro.contentHeight
    
    local background = display.newImage( group, background)
    background:translate( centerX, centerY )
    background:toBack()
    
    gameState.success = 0
    
    --  CREACIÓN DE juguetes
        for count = 1, 4 do
            imagen = path.."escenarios/"..carpeta.."/btn-"..count..".png"
            
            imagenJuguete[count] = display.newImage(group,imagen)
            imagenJuguete[count].myName = count
            
            -- sonidos
            utils.renombrarImagenes(numeroTipoJuego,imagenJuguete[count])
            
            imagenJuguete[count].w = imagenJuguete[count].contentWidth
            imagenJuguete[count].h = imagenJuguete[count].contentHeight
                    
            imagenJuguete[count]:addEventListener("touch",onTouch)
            
        end
    acomodarImagenes()
    local btnRegresar = display.newImage( group,"images/btn-regresar.png" )
    btnRegresar:translate( centerX/4, centerY*1.85 )
    btnRegresar:addEventListener("tap", btnTap)
    btnRegresar.destination = "juegos.popi.encasa.menuEncasa"
     topsign = display.newImage( group,"images/top-sign.png" )
        topsign:translate( centerX, centerY/5 )
    inst = display.newText( optionsTextMenu )
        group:insert(inst)
        inst.text="Arrastra hacia popi"

end        
 ------------------------------------------------------------------------------------------

function scene:show( event )
    local group = self.view
end

function scene:hide( event )
    local group = self.view
    
    local phase = event.phase
    
    if event.phase == "will" then
        -- Called when the scene is on screen and is about to move off screen
        --
        -- INSERT code here to pause the scene
        -- e.g. stop timers, stop animation, unload sounds, etc.)
        
    elseif phase == "did" then
        -- Called when the scene is now off screen
    end 
        
        composer.removeScene( composer.getSceneName( "current" ) )    
end

function scene:destroy( event )
    -- Called prior to the removal of scene's "view" (group)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc.
    local group = self.view 
end

-------------- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
--------------

return scene
