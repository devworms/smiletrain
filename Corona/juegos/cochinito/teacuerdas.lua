local composer = require( "composer" )
local scene = composer.newScene()
--local introIsPlaying
local paso=1
local paso1imagen1
local paso1imagen2
local paso1imagen3
local paso2imagen1
local paso2imagen2
local paso2imagen3
local paso3imagen1
local paso3imagen2
local paso3imagen3
local paso4imagen1
local paso4imagen2
local paso4imagen3
------VARIABLES DE GRABACION--------------
local r
local isAndroid = false
local dataFileName = "audiopapu"
if "simulator" == system.getInfo("environment") then
    dataFileName = dataFileName .. ".aif"
else
	local platformName = system.getInfo( "platformName" )
    if "iPhone OS" == platformName then
        dataFileName = dataFileName .. ".aif"
    elseif "Android" == platformName then
        dataFileName = dataFileName .. ".wav"
        isAndroid = true
    else
    	print("Unknown OS " .. platformName )
    end
end
local filePath = system.pathForFile( dataFileName, system.DocumentsDirectory )
r = media.newRecording(filePath)
--funcion para limpiar la memoria yliberar el audio
local function llamame(event)
    print("khe")
   
    audio.dispose(event.handle)
end
-------fin de las variables de grabacion---------

local function btnTapbien(event)
    --esto es para quitar el bloqueo 
--    if introIsPlaying == true then
--    local audioChannel = audio.stop() 
--   introIsPlaying = false
--    end
    if paso==1 then
        objeto= "cochinitos"
         utils.primerSonido("juegos.cochinito.teacuerdas",objeto)
    elseif paso==2 then
        objeto= "paja"
         utils.primerSonido("juegos.cochinito.teacuerdas",objeto)
    elseif paso==3 then
        objeto= "palos"
         utils.primerSonido("juegos.cochinito.teacuerdas",objeto)
    elseif paso==4 then
        objeto= "ladrillos"
         utils.primerSonido("juegos.cochinito.teacuerdas",objeto)
    end
end

local function btnTap(event)
    --esto es para quitar el bloqueo 
--    if introIsPlaying == true then
--    local audioChannel = audio.stop() 
--   introIsPlaying = false
--    end
     if event.target.name == "regresar" then
         utils.tipoImagen()
        utils.reproducirSonido("boton")
        composer.gotoScene ( event.target.destination, { effect = "fade"} )
        print(event.target.destination)
        return true
     else
         utils.reproducirSonido("error")
     end
         
end

local function cambiarscena()
    if paso==2 then
        paso1imagen1.isVisible=false
        paso1imagen2.isVisible=false
        paso1imagen3.isVisible=false
        paso2imagen1.isVisible=true
        paso2imagen2.isVisible=true
        paso2imagen3.isVisible=true
        paso3imagen1.isVisible=false
        paso3imagen2.isVisible=false
        paso3imagen3.isVisible=false 
        paso4imagen1.isVisible=false
        paso4imagen2.isVisible=false
        paso4imagen3.isVisible=false
         utils.reproducirSonido("sounds/cochinitos/jugar/TeAcuerdas/Paso2",0)
     elseif  paso==3 then  
        paso1imagen1.isVisible=false
        paso1imagen2.isVisible=false
        paso1imagen3.isVisible=false
        paso2imagen1.isVisible=false
        paso2imagen2.isVisible=false
        paso2imagen3.isVisible=false
        paso3imagen1.isVisible=true
        paso3imagen2.isVisible=true
        paso3imagen3.isVisible=true 
        paso4imagen1.isVisible=false
        paso4imagen2.isVisible=false
        paso4imagen3.isVisible=false
        utils.reproducirSonido("sounds/cochinitos/jugar/TeAcuerdas/Paso3",0)
    elseif  paso==4 then 
        paso1imagen1.isVisible=false
        paso1imagen2.isVisible=false
        paso1imagen3.isVisible=false
        paso2imagen1.isVisible=false
        paso2imagen2.isVisible=false
        paso2imagen3.isVisible=false
        paso3imagen1.isVisible=false
        paso3imagen2.isVisible=false
        paso3imagen3.isVisible=false 
        paso4imagen1.isVisible=true
        paso4imagen2.isVisible=true
        paso4imagen3.isVisible=true
        utils.reproducirSonido("sounds/cochinitos/jugar/TeAcuerdas/Paso4",0)
    end
end

function scene:successful() 
    --local pronuncia = utils.validarVoz(objeto)
  local pronuncia = true
    
    if pronuncia == true then
-------------------********inicio del record********----------------------
        print("presionado")
    utils.reproducirSonido("bip")
timer.performWithDelay(700, function()
   r:startRecording()
   end,1)
   
    timer.performWithDelay(3000, function() 
                    r:stopRecording()
                    utils.reproducirSonido("bup")
                    timer.performWithDelay(1000, function()
            -- Play back the recording
            local file = io.open( filePath, "r" ) 
            if file then
                io.close( file )
            local playbackSoundHandle = audio.loadStream( dataFileName, system.DocumentsDirectory )
				audio.play( playbackSoundHandle, { onComplete= llamame} )
              end 
              end,1)
                end,1)
----------**********fin de el record*****------------------------------
       
timer.performWithDelay(5500, function() 
        composer.removeScene( "bloqueo" )
       paso=paso+1
       if paso==5 then
            timer.performWithDelay(1250, function()	
                    utils.ponerGlobos()
                end,1)
       end
       cambiarscena()
       end,1)
    else
       
        utils.reproducirSonido("sounds/principales/intentalo",1)
       
    end
  
    
end
   
function scene:create( event )
    local group = self.view 
    local background = display.newImage(group,path.."fondo-1.png")
     background:translate( centerX, centerY )
    background:toBack()
     paso1imagen1= display.newImage(group,path.."teacuerdas/paso 1/boton perro.png")
    paso1imagen1:translate( (centerX*2)/6, centerY )
    paso1imagen1:addEventListener("tap", btnTap)
      paso1imagen2= display.newImage(group,path.."teacuerdas/paso 1/boton cochinitos.png")
    paso1imagen2:translate( ((centerX*2)/6)*3, centerY )
     paso1imagen2:addEventListener("tap", btnTapbien)
      paso1imagen3= display.newImage(group,path.."teacuerdas/paso 1/boton cordero.png")
    paso1imagen3:translate( ((centerX*2)/6)*5, centerY )
     paso1imagen3:addEventListener("tap", btnTap)
      paso2imagen1= display.newImage(group,path.."teacuerdas/paso 2/paja.png")
    paso2imagen1:translate( (centerX*2)/6, centerY )
     paso2imagen1.isVisible=false
      paso2imagen1:addEventListener("tap", btnTapbien)
      paso2imagen2= display.newImage(group,path.."teacuerdas/paso 2/carton.png")
    paso2imagen2:translate( ((centerX*2)/6)*3, centerY )
      paso2imagen2.isVisible=false
       paso2imagen2:addEventListener("tap", btnTap)
      paso2imagen3= display.newImage(group,path.."teacuerdas/paso 2/piedra.png")
    paso2imagen3:translate( ((centerX*2)/6)*5, centerY )
      paso2imagen3.isVisible=false
       paso2imagen3:addEventListener("tap", btnTap)
     paso3imagen1= display.newImage(group,path.."teacuerdas/paso 3/lodo.png")
    paso3imagen1:translate( (centerX*2)/6, centerY )
      paso3imagen1.isVisible=false
       paso3imagen1:addEventListener("tap", btnTap)
      paso3imagen2= display.newImage(group,path.."teacuerdas/paso 3/plastico.png")
    paso3imagen2:translate( ((centerX*2)/6)*3, centerY )
    paso3imagen2.isVisible=false
     paso3imagen2:addEventListener("tap", btnTap)
      paso3imagen3= display.newImage(group,path.."teacuerdas/paso 3/palos.png")
    paso3imagen3:translate( ((centerX*2)/6)*5, centerY )
    paso3imagen3.isVisible=false
     paso3imagen3:addEventListener("tap", btnTapbien)
      paso4imagen1= display.newImage(group,path.."teacuerdas/paso 4/madera.png")
    paso4imagen1:translate( (centerX*2)/6, centerY )
    paso4imagen1.isVisible=false
     paso4imagen1:addEventListener("tap", btnTap)
      paso4imagen2= display.newImage(group,path.."teacuerdas/paso 4/fierro.png")
    paso4imagen2:translate( ((centerX*2)/6)*3, centerY )
    paso4imagen2.isVisible=false
     paso4imagen2:addEventListener("tap", btnTap)
      paso4imagen3= display.newImage(group,path.."teacuerdas/paso 4/ladrillo.png")
    paso4imagen3:translate( ((centerX*2)/6)*5, centerY )
    paso4imagen3.isVisible=false
     paso4imagen3:addEventListener("tap", btnTapbien)
     
     local btnRegresar = display.newImage( group,"images/btn-regresar.png" )
    btnRegresar:translate( centerX/4, centerY*1.85 )
    btnRegresar:addEventListener("tap", btnTap)
    btnRegresar.destination = "menuJuego"
    btnRegresar.name = "regresar"
    utils.reproducirSonido("sounds/cochinitos/jugar/TeAcuerdas/Paso1",0)
--    composer.removeScene( "bloqueo" )
--    introIsPlaying=true 
end
function scene:show( event )
	local group = self.view 
        
end
function scene:hide( event )
	local group = self.view
	
	local phase = event.phase
	
	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
		
	elseif phase == "did" then
		-- Called when the scene is now off screen
	end	
        
         composer.removeScene( composer.getSceneName( "current" ) )
	
end

function scene:destroy( event )

	-- Called prior to the removal of scene's "view" (group)
	-- 
	-- INSERT code here to cleanup the scene
	-- e.g. remove display objects, remove touch listeners, save state, etc.
	local group = self.view
	
	
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene


