-----------------------------------------------------------------------------------------
-- lacasa.lua
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local scene = composer.newScene()
local introIsPlaying
local cochinito
local pino
local casa
local flores
local puerta
local ventana
local isOn=false
local objeto
local contador=0

local inst
 ------VARIABLES DE GRABACION--------------
local r
local isAndroid = false
local dataFileName = "audiopapu"
if "simulator" == system.getInfo("environment") then
    dataFileName = dataFileName .. ".aif"
else
	local platformName = system.getInfo( "platformName" )
    if "iPhone OS" == platformName then
        dataFileName = dataFileName .. ".aif"
    elseif "Android" == platformName then
        dataFileName = dataFileName .. ".wav"
        isAndroid = true
    else
    	print("Unknown OS " .. platformName )
    end
end
local filePath = system.pathForFile( dataFileName, system.DocumentsDirectory )
r = media.newRecording(filePath)
--funcion para limpiar la memoria yliberar el audio
local function llamame(event)
    print("khe")
   
    audio.dispose(event.handle)
end
-------fin de las variables de grabacion---------
 ----------------------------------------------------------------------------------------
local function btnTap(event)
    --esto es para quitar el bloqueo 
  isOn = true
    local audioChannel = audio.stop() 
   introIsPlaying = false
    
        utils.reproducirSonido("boton")
        composer.gotoScene ( event.target.destination, { effect = "fade"} )
        print(event.target.destination)
    return true
end 

local function  Gameplay()
    if isOn == false then
    
    introIsPlaying=true 
        if contador==0 then
            objeto="casa"
             utils.primerSonido("juegos.cochinito.lacasa",objeto)
             inst.text="Ayuda al cochinito a construir su casa"

        end
composer.removeScene( "bloqueo" )
    introIsPlaying=true 
        if contador==1 then
        	 casa.isVisible=true
        	utils.reproducirSonido("sounds/Objetos/cochinitosIndica2",0, function ( ... )
        		 objeto="ventana"
                         
	            utils.primerSonido("juegos.cochinito.lacasa",objeto)
	           
        	end)
           
   
            elseif contador==2 then
                objeto="puerta"
                utils.primerSonido("juegos.cochinito.lacasa",objeto)
                ventana.isVisible=true
    
            elseif contador==3 then 
                objeto="cochinito"
                utils.reproducirSonido("sounds/Objetos/cochinitoIndica",0,function ( ... )
                
                	 utils.primerSonido("juegos.cochinito.lacasa",objeto)
                end)
               
                puerta.isVisible=true
                inst.text="Ahora invita al cochinito a su casa"

            elseif contador==4 then 
            	 objeto="flor"
            	 utils.reproducirSonido("sounds/Objetos/jardin",0,function ( ... )
                	  utils.primerSonido("juegos.cochinito.lacasa",objeto)
                           
                end)
               
               
                cochinito.isVisible=false
                inst.text="¿Si completamos la casa con un jardin?"

            elseif contador==5 then 
                objeto="pino"
                utils.primerSonido("juegos.cochinito.lacasa",objeto)
                composer.removeScene( "bloqueo" )
                introIsPlaying=true 
                flores.isVisible=true
    
            elseif contador==6 then
                pino.isVisible = true

                timer.performWithDelay(1250, function()    
                utils.ponerGlobos()
            end,1)
            
            end 
            end
end

function scene:successful() 
   
    --local pronuncia = utils.validarVoz(objeto)
    
   local pronuncia = true
    
    local tiempo=0
    
    if pronuncia == true then
        -------------------********inicio del record********----------------------
        print("presionado")
    utils.reproducirSonido("bip")
timer.performWithDelay(700, function()
   r:startRecording()
   end,1)
   
    timer.performWithDelay(3000, function() 
                    r:stopRecording()
                    utils.reproducirSonido("bup")
                    timer.performWithDelay(1000, function()
            -- Play back the recording
            local file = io.open( filePath, "r" ) 
            if file then
                io.close( file )
            local playbackSoundHandle = audio.loadStream( dataFileName, system.DocumentsDirectory )
				audio.play( playbackSoundHandle, { onComplete= llamame} )
              end 
              end,1)
                end,1)
----------**********fin de el record*****------------------------------
        contador=contador+1
        composer.removeScene( "bloqueo" )
        tiempo=5500

        else
            utils.reproducirSonido("sounds/principales/intentalo",1)
            tiempo=2000
    end

    timer.performWithDelay(tiempo, function()
    Gameplay()
    end,1)

end

function scene:create( event )
    local group = self.view
    local background = display.newImage(group,path.."lacasa/fondo.png")
    background:translate( centerX, centerY )
    background:toBack()

    cochinito=display.newImage(group,path.."lacasa/cochino.png")
    cochinito:translate(centerX + 300, centerY + 60)
    cochinito.isVisible=true

    casa=display.newImage(group,path.."lacasa/casa-de-paja.png")
    casa:translate(centerX, centerY)
    casa.isVisible=false

    ventana=display.newImage(group,path.."lacasa/ventana.png")
    ventana:translate(centerX - 40, centerY + 50)
    ventana.isVisible=false

    puerta=display.newImage(group,path.."lacasa/puerta.png")
    puerta:translate(centerX + 40, centerY + 70)
    puerta.isVisible=false

    flores=display.newImage(group,path.."lacasa/flores.png")
    flores:translate(centerX, centerY + 115)
    flores.isVisible=false

    pino=display.newImage(group,path.."lacasa/pino.png")
    pino:translate(centerX - 300, centerY + 25)
    pino.isVisible=false
    
    local btnRegresar = display.newImage( group,"images/btn-regresar.png" )
    btnRegresar:translate( centerX/4, centerY*1.85 )
    btnRegresar:addEventListener("tap", btnTap)
    btnRegresar.destination = "menuJuego"
    utils.reproducirSonido("sounds/cochinitos/jugar/Lacasa/instrucciones",0,function()
        composer.removeScene( "bloqueo" )
    introIsPlaying=true 
         Gameplay() 
    end)

    topsign = display.newImage( group,"images/top-sign.png" )
    topsign:translate( centerX, centerY/5 )

    inst = display.newText( optionsTextMenu )
    group:insert(inst)
    inst.text="Completemos la casa"
end

function scene:show( event )
    local group = self.view         
end

function scene:hide( event )
    local group = self.view
    
    local phase = event.phase
    
    if event.phase == "will" then
        -- Called when the scene is on screen and is about to move off screen
        --
        -- INSERT code here to pause the scene
        -- e.g. stop timers, stop animation, unload sounds, etc.)
        
    elseif phase == "did" then
        -- Called when the scene is now off screen
    end 
        
         composer.removeScene( composer.getSceneName( "current" ) )
    
end

function scene:destroy( event )

    -- Called prior to the removal of scene's "view" (group)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc.
    local group = self.view
    
    
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene


