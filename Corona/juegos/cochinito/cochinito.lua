local composer = require( "composer" )
local scene = composer.newScene()

local cochinito
local lobo
 local piedras
   local trepa
   local casa
local isOn=false
local fin=false

local objeto

 local contador=0
 local errores=false
 local errorcontador=0
 ------VARIABLES DE GRABACION--------------
local r
local isAndroid = false
local dataFileName = "audiopapu"
if "simulator" == system.getInfo("environment") then
    dataFileName = dataFileName .. ".aif"
else
	local platformName = system.getInfo( "platformName" )
    if "iPhone OS" == platformName then
        dataFileName = dataFileName .. ".aif"
    elseif "Android" == platformName then
        dataFileName = dataFileName .. ".wav"
        isAndroid = true
    else
    	print("Unknown OS " .. platformName )
    end
end
local filePath = system.pathForFile( dataFileName, system.DocumentsDirectory )
r = media.newRecording(filePath)
--funcion para limpiar la memoria yliberar el audio
local function llamame(event)
    print("khe")
   
    audio.dispose(event.handle)
end
-------fin de las variables de grabacion---------
 ----------------------------------------------------------------------------------------
local function btnTap(event)
    isOn = true
        local audioChannel = audio.stop() 

        utils.reproducirSonido("boton")
        composer.gotoScene ( event.target.destination, { effect = "fade"} )
        print(event.target.destination)
    return true
end 

local function errores()
    if  errorcontador==1 then
      transition.to(lobo, {y=(centerY-(centerY/4)),yScale=.5,xScale=.5})
    elseif errorcontador==2 then
         transition.to(lobo, {y=cochinito.y,x=cochinito.x-cochinito.contentWidth, yScale=.8,xScale=.8})
   
     end   
end

local function  Gameplay()
    if isOn == false then
    
    if contador==0 then
        objeto="corre"
         utils.primerSonido("juegos.cochinito.cochinito",objeto)
        
        
    end
  

   if contador==1 then
       transition.to(cochinito, {x=(centerX-(piedras.contentWidth ))})
        objeto="salta"
          utils.primerSonido("juegos.cochinito.cochinito",objeto)
   
    
    elseif contador==2 then
         --saltar()
         transition.to(cochinito, {x=(centerX)+ (piedras.contentWidth),y=centerY})
        timer.performWithDelay(250, function()	
         transition.to(cochinito, {x=(centerX-(piedras.contentWidth ))+ (piedras.contentWidth)*2,y=centerY+((centerY/6))})
         end,1 )
         
        objeto="corre"
        utils.primerSonido("juegos.cochinito.cochinito",objeto)
     
    
    
    elseif contador==3 then 
           transition.to(cochinito, {x=(centerX+((centerX/6)*5)) - trepa.contentWidth/2})
        objeto="trepa"
          utils.primerSonido("juegos.cochinito.cochinito",objeto)
    
  
   
    elseif contador==4 then
     transition.to(cochinito, {y=((centerY/4)*3)})
    fin=true
    end
    
    if fin==true then
      
        casa.isVisible=true
        cochinito.isVisible=false
        utils.reproducirSonido("sounds/principales/ganastelobo",1, function ( ... )
          
           utils.ponerGlobos()
        end)
            
        
   
    end
    end
end

function scene:successful() 
   
    --local pronuncia = utils.validarVoz(objeto)
    local pronuncia = true
    
    local tiempo=0
    
    if pronuncia == true then
        -------------------********inicio del record********----------------------
        print("presionado")
    utils.reproducirSonido("bip")
timer.performWithDelay(700, function()
   r:startRecording()
   end,1)
   
    timer.performWithDelay(3000, function() 
                    r:stopRecording()
                    utils.reproducirSonido("bup")
                    timer.performWithDelay(1000, function()
            -- Play back the recording
            local file = io.open( filePath, "r" ) 
            if file then
                io.close( file )
            local playbackSoundHandle = audio.loadStream( dataFileName, system.DocumentsDirectory )
				audio.play( playbackSoundHandle, { onComplete= llamame} )
              end 
              end,1)
                end,1)
----------**********fin de el record*****------------------------------
       
--timer.performWithDelay(6000, function() 
        contador=contador+1
        
        tiempo=5500
    else
        errorcontador=errorcontador+1
        if errorcontador==3 then
         transition.to(lobo, {y=cochinito.y,x=cochinito.x, yScale=.8,xScale=.8})
            cochinito.isVisible=false

            utils.reproducirSonido("sounds/principales/pierdes",1,function ( ... )
                composer.gotoScene ( "menuJuego", { effect = "fade"} )
            end)
       
              
        else
            utils.reproducirSonido("sounds/principales/intentaloLobo",1)
        
        errores()
        tiempo=3200
        end
    end
  if errorcontador==3 then
  else
       timer.performWithDelay(tiempo, function()
           composer.removeScene( "bloqueo" )
    Gameplay()
    end,1)
  end
   --end,1) 
   
end
function scene:create( event )
    local group = self.view
    local background = display.newImage(group,path.."cochinito/fondo.png")
     background:translate( centerX, centerY )
    background:toBack()
     piedras=display.newImage(group,path.."cochinito/salta.png")
    piedras:translate(centerX, centerY+((centerY/12)*5))
    trepa=display.newImage(group,path.."cochinito/trepa.png")
    trepa:translate(centerX+((centerX/6)*5), centerY+((centerY/12)*3))
     lobo=display.newImage(group,path.."cochinito/Lobo.png")
    lobo:scale(.4,.4)
    lobo:translate(centerX-((centerX/6)*4), centerY-((centerY/12)*7))
    casa=display.newImage(group,path.."cochinito/cochinos.png")
    casa:translate(centerX+((centerX/24)*21), centerY-((centerY/24)*11))
    casa.isVisible=false
    cochinito=display.newImage(group,path.."cochinito/cochinito.png")
    cochinito:translate(centerX-((centerX/6)*5), centerY+((centerY/6))) 
    
    local btnRegresar = display.newImage( group,"images/btn-regresar.png" )
    btnRegresar:translate( centerX/4, centerY*1.85 )
    btnRegresar:addEventListener("tap", btnTap)
    btnRegresar.destination = "menuJuego"
    utils.reproducirSonido("sounds/cochinitos/jugar/ElCochinito/Instrucciones",0,function()
         Gameplay() 
    end)
   
end
function scene:show( event )
	local group = self.view 
        
end
function scene:hide( event )
	local group = self.view
	
	local phase = event.phase
	
	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
		
	elseif phase == "did" then
		-- Called when the scene is now off screen
	end	
        
         composer.removeScene( composer.getSceneName( "current" ) )
	
end

function scene:destroy( event )

	-- Called prior to the removal of scene's "view" (group)
	-- 
	-- INSERT code here to cleanup the scene
	-- e.g. remove display objects, remove touch listeners, save state, etc.
	local group = self.view
	
	
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene


