-----------------------------------------------------------------------------------------
-- papas.lua
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local scene = composer.newScene()
local ventanas = {}

local video = ""
local textLoad
local SpriteNuevo

----------------------------------------------------------------------------------------
local function btnTap(event)
        utils.reproducirSonido("boton")
        composer.gotoScene ( event.target.destination, { effect = "fade"} )
        print(event.target.destination)
    return true
end
----------------------------------------------------------------------------------------
-- @param event
-- @return
local onCompleteVideo = function( event )
        print( "video session ended" )
        
end

local function playVideo()
    media.playVideo( video , system.DocumentsDirectory, true, onCompleteVideo )
    
end
----------------------------------------------------------------------------------------
local function networkListener( event )
    if ( event.isError ) then
        print( "Network error - download failed" )
        textLoad.text = "No hay conexión a internet"
        timer.performWithDelay(1200, function()	
                                        SpriteNuevo:pause()
                                        SpriteNuevo:removeSelf()
                                        textLoad:removeSelf()
					composer.removeScene( "bloqueo" )
					end, 1)
        
    elseif ( event.phase == "began" ) then
        print( "Progress Phase: began" )
    elseif ( event.phase == "ended" ) then
        print( "Displaying response video file" )
        SpriteNuevo:pause()
        SpriteNuevo:removeSelf()
        textLoad:removeSelf()
        composer.removeScene( "bloqueo" )
        playVideo()
    end
end
-------------------------------------------------

local function doesFileExist( fname, path )

    local results = false

    local filePath = system.pathForFile( fname, path )

    --filePath will be 'nil' if file doesn't exist and the path is 'system.ResourceDirectory'
    if ( filePath ) then
        filePath = io.open( filePath, "r" )
    end

    if ( filePath ) then
        print( "File found: " .. fname )
        --clean up file handles
        filePath:close()
        results = true
    else
        print( "File does not exist: " .. fname )
    end

    return results
end

local function vTap(event)    
    
    video = event.target.myName 

    local ress = doesFileExist( video , system.DocumentsDirectory )
    
    if ress then
        playVideo()
    else    
        composer.showOverlay( "bloqueo" ,{ isModal = true } )
        
        local spriteTam = { width=128, height=128,  numFrames=12 }
	local spriteImag = graphics.newImageSheet( "images/menu/loader.png", spriteTam )
	local sequenceDataSprite =
				{
				    { name="loader", start=1, count=12, time=700, loopCount=0 }
				}
	SpriteNuevo = display.newSprite( spriteImag, sequenceDataSprite )
		SpriteNuevo.x = centerX
		SpriteNuevo.y = centerY*0.5
                SpriteNuevo.xScale=.5
		SpriteNuevo.yScale=.5
        SpriteNuevo:play()
        
        textLoad = display.newText("Espera un momento por favor...", 
                                centerX, centerY*1.5, "fonts/GothamNarrow-Book", 25)
        textLoad:setTextColor(0, 0, 0)
        
        local params = {}
        params.progress = true

        network.download(
            "http://smiletrainla.org/Videos/".. event.target.myName,
            "GET",
            networkListener,
            params,
            video,
            system.DocumentsDirectory
        )
    end 
     
end

-------------------------------------------------

function scene:create( event )
    
    local group = self.view
    
    local background = display.newImage( group,"images/papas/fondo.png" )
    background:translate( centerX, centerY )
    background:toBack()
    
    
    local imgWidth, imgHeight = 300, centerY / 1.8
    local sumWidth, sumHeight = 400, 190
    
    for count = 1, 4 do
        ventanas[count] = display.newImage(group,"images/papas/"..count..".png")
        
        ventanas[count]:translate(imgWidth, imgHeight)
        imgWidth = imgWidth + sumWidth
            
            if(count%2 == 0) then
                imgHeight = imgHeight + sumHeight
                imgWidth = imgWidth - 800
            end
        
        if count == 3 then
            ventanas[count].myName = "video" .. (count+1) ..".mp4"
        elseif count == 4 then 
            ventanas[count].myName = "video" .. (count-1) ..".mp4"
        else -- 1,2
            ventanas[count].myName = "video" .. count ..".mp4"
        end
        	
        ventanas[count]:addEventListener( "tap", vTap )
    end
    
    local btnRegresar = display.newImage( group,"images/btn-regresar.png" )
    btnRegresar:translate( centerX/4, centerY*1.85 )
    btnRegresar:addEventListener("tap", btnTap)
    btnRegresar.destination = "home"
    
end        
 ------------------------------------------------------------------------------------------


function scene:show( event )
    local group = self.view
end

function scene:hide( event )
    local group = self.view
    
    local phase = event.phase
    
    if event.phase == "will" then
        -- Called when the scene is on screen and is about to move off screen
        --
        -- INSERT code here to pause the scene
        -- e.g. stop timers, stop animation, unload sounds, etc.)
        
    elseif phase == "did" then
        -- Called when the scene is now off screen
    end 
        
        composer.removeScene( composer.getSceneName( "current" ) )    
end

function scene:destroy( event )
    -- Called prior to the removal of scene's "view" (group)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc.
    local group = self.view 
end

-------------- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
--------------

return scene
