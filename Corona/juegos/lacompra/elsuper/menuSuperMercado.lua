local composer = require( "composer" )
local gameState=require("utilerias.gameState")
local scene = composer.newScene()
--local introIsPlaying
local function btnTap(event)
    --esto es para quitar el bloqueo 
--    if introIsPlaying == true then
--    local audioChannel = audio.stop() 
--   introIsPlaying = false
--    end
    if event.target.name == "verduras" then
        gameState.tipodeSubJuego = 1
    elseif event.target.name == "frutas" then
        gameState.tipodeSubJuego = 2
    elseif event.target.name == "harinas" then
        gameState.tipodeSubJuego = 3
    else
        gameState.tipodeSubJuego = 4
    end
    
    utils.reproducirSonido("boton")
    
   composer.gotoScene ( event.target.destination, { effect = "fade"} )
   print(event.target.destination)
   return true
end

-- Called when the scene's view does not exist:
function scene:create( event )
	local group = self.view
        
        
        
        -- Sirve para generalizar el juego del supermercado y saber que debo de arrastrar, frutas, carne, etc...
            local background = display.newImage( group,"images/lacompra/elsuper/fondo-super.png" )
            background:translate( centerX, centerY )
            local topsign = display.newImage( group,"images/top-sign.png" )
            topsign:translate( centerX, centerY/5 )

            local btnRegresar = display.newImage( group,"images/btn-regresar.png" )
            btnRegresar:translate( centerX/4, centerY*1.85 )
            btnRegresar:addEventListener("tap", btnTap)

            btnRegresar.destination = "menuJuego"
             local inst = display.newText( optionsTextMenu )
            group:insert(inst)
            inst.text="Escoge el pasillo que quieras"
         utils.reproducirSonido("sounds/lacompra/jugar/ElSuper/Instrucciones2", 0)
         --composer.removeScene( "bloqueo" )
        --introIsPlaying=true 
end

----Configuracion de botones------------------
local function configurarBtn1(group, destino)
    local imagen = ""
            if row.btn1 == 'true' then
                imagen= "images/menu/btn-j1.png"--verduras
            
            else
                imagen = "images/menu/paloma.png"
            end
        
            
            local btnJ1 = display.newImage( group,imagen )
            btnJ1:translate( centerX/4, centerY*1.15 )
            if row.btn1 == 'true' then
                btnJ1:addEventListener("tap", btnTap)
                btnJ1.destination = destino
                btnJ1.name = "verduras"
                local text1 = display.newText(group, "Verduras", btnJ1.x+5, centerY, "fonts/GothamNarrow-Book", 25 )
            end

end
local function configurarBtn2(group,destino)
            local imagen = ""
             if row.btn2 == 'true' then
                imagen= "images/menu/btn-j2.png"--dulces
            
            else
                imagen = "images/menu/paloma.png"
            end
        
            local btnJ2 = display.newImage( group, imagen)
            btnJ2:translate( centerX/1.35, centerY*1.15 )
            if row.btn2 == 'true' then
                btnJ2:addEventListener("tap", btnTap)
                btnJ2.destination = destino
                btnJ2.name = "frutas"
                local text2 = display.newText(group, "Dulces", btnJ2.x+5, centerY, "fonts/GothamNarrow-Book", 25 )
            end
            

end
local function configurarBtn3(group,destino)
            local imagen = ""
            if row.btn3 == 'true' then
                imagen= "images/menu/btn-j3.png"--harinas
            
            else
                imagen = "images/menu/paloma.png"
            end

            local btnJ3 = display.newImage( group, imagen)
            btnJ3:translate( centerX*1.25, centerY*1.15 )
            if row.btn3 == 'true' then
                btnJ3:addEventListener("tap", btnTap)
                btnJ3.destination = destino
                btnJ3.name = "harinas"
                local text3 = display.newText(group, "Harinas", btnJ3.x+5, centerY, "fonts/GothamNarrow-Book", 25 )
            end

end
local function configurarBtn4(group,destino)
            local imagen = ""
             if row.btn4 == 'true' then
                imagen= "images/menu/btn-j4.png"--bebidas
            
            else
                imagen = "images/menu/paloma.png"
            end
        
            local btnJ4 = display.newImage( group,imagen )
            btnJ4:translate( centerX*1.75, centerY*1.15 )
            if row.btn4 == 'true' then
                btnJ4:addEventListener("tap", btnTap)
                btnJ4.destination = destino
                btnJ4.name = "bebidas"
                local text4 = display.newText(group, "Bebidas", btnJ4.x, centerY, "fonts/GothamNarrow-Book", 25 )
            end

end
-- Called immediately after scene has moved onscreen:
function scene:show( event )
	-- INSERT code here (e.g. start timers, load audio, start listeners, etc.)
	local group = self.view
        row = utils.obtenerPreferencesJuego("elsuper")
        if row ~=nil then
            local destino = "juegos.lacompra.elsuper.carritocompras"
            
            configurarBtn1(group,destino)
            configurarBtn2(group,destino)  
            configurarBtn3(group,destino)
            configurarBtn4(group,destino)
        end
end

-- Called when scene is about to move offscreen:
function scene:hide( event )
	local group = self.view
	
	composer.removeScene( composer.getSceneName( "current" ) )
	
	--Runtime:removeEventListener("enterFrame", update)
	-- INSERT code here (e.g. stop timers, remove listeners, unload sounds, etc.)
	-- Remove listeners attached to the Runtime, timers, transitions, audio tracks
end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroy( event )
	local group = self.view

	-- INSERT code here (e.g. remove listeners, widgets, save state, etc.)
	-- Remove listeners attached to the Runtime, timers, transitions, audio tracks

end

---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene---------------------------------------------------------
