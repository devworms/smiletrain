local composer = require( "composer" )
local utils = require("utilerias.utils" )
local gameState=require("utilerias.gameState")
local scene = composer.newScene()
--local introIsPlaying
local productosIzquierda = nil
local productosDerecha=nil
------VARIABLES DE GRABACION--------------
local r
local isAndroid = false
local dataFileName = "audiopapu"
if "simulator" == system.getInfo("environment") then
    dataFileName = dataFileName .. ".aif"
else
	local platformName = system.getInfo( "platformName" )
    if "iPhone OS" == platformName then
        dataFileName = dataFileName .. ".aif"
    elseif "Android" == platformName then
        dataFileName = dataFileName .. ".wav"
        isAndroid = true
    else
    	print("Unknown OS " .. platformName )
    end
end
local filePath = system.pathForFile( dataFileName, system.DocumentsDirectory )
r = media.newRecording(filePath)
--funcion para limpiar la memoria yliberar el audio
local function llamame(event)
    print("khe")
   
    audio.dispose(event.handle)
end
-------fin de las variables de grabacion---------
local carrito=""
local productoIzq=nil
local productoDer=nil
local productosIzq=nil
local productosDer=nil

local grupoImagenes = nil

local btnOrigen = nil
-- Variables obligatorias para juegos con dragging
scene.imagenSeleccionada = nil
scene.imagenColisionada = nil
scene.posIniX = nil
scene.posIniY = nil
-- 


local function crearProducto(producto, event)
        producto:translate( event.x, event.y )
        producto.w=72
        producto.h=38
        
        utils.dragging(producto,carrito,"juegos.lacompra.elsuper.carritocompras",
            function (resultado) 
                if resultado then
                    transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x,y= scene.imagenColisionada.y})

                    utils.primerSonido("juegos.lacompra.elsuper.carritocompras", scene.imagenSeleccionada.id)
                else
                    if productoIzq ~= nil and productosIzq~=nil then
                       productoIzq:removeSelf()
                       productoIzq = nil
                   elseif productoDer ~= nil and productosDer~=nil then
                       productoDer:removeSelf()
                       productoDer = nil
                   end

                end
            end)
end

---------------------******Funcion obligatoria*********----
function scene:successful()
    
   --local pronuncia = utils.validarVoz(scene.imagenSeleccionada.id)
   --local pronuncia= true
    
    --[[if pronuncia then
         local r=math.random(1,2)
       local ruta
        if r==1 then
            ruta="excelente"
        else
            ruta="muybien"
        end
        utils.reproducirSonido("sounds/principales/"..ruta,1,function( )]]--
        -------------------********inicio del record********----------------------
        print("presionado")
    utils.reproducirSonido("bip")
timer.performWithDelay(700, function()
   r:startRecording()
   end,1)
   
    timer.performWithDelay(3000, function() 
                    r:stopRecording()
                    utils.reproducirSonido("bup")
                    timer.performWithDelay(1000, function()
            -- Play back the recording
            local file = io.open( filePath, "r" ) 
            if file then
                io.close( file )
            local playbackSoundHandle = audio.loadStream( dataFileName, system.DocumentsDirectory )
				audio.play( playbackSoundHandle, { onComplete= llamame} )
              end 
              end,1)
                end,1)
----------**********fin de el record*****------------------------------
             timer.performWithDelay(5000, function() 
             if productosIzq ~=nil and productoIzq~=nil  then
             productoIzq:removeEventListener("touch")
           -- productoIzq=nil
            productosIzq:removeSelf()
            productosIzq=nil
            carrito:toFront()
        elseif productosDer ~=nil and productoDer~=nil  then
            productoDer:removeEventListener("touch")
           -- productoDer=nil
            productosDer:removeSelf()
            productosDer=nil
            carrito:toFront()  
        end
    
        composer.removeScene( "bloqueo" )
        
        --- juego terminado
        if productosIzq==nil and productosDer==nil then
                -- variable >= no de coincidencias que debe haber
            utils.actualizarPreferenciaJuego(btnOrigen, 'false', 'elsuper')
            row = utils.obtenerPreferencesJuego("elsuper")
            if  row.btn1== 'false' and row.btn2== 'false' and row.btn3== 'false' and row.btn4== 'false' then
                composer.removeScene("juegos.lacompra.elsuper.carritocompras")
                composer.gotoScene ("juegos.lacompra.final-lacompra" , { effect = "fade"} )
            else
                composer.removeScene("juegos.lacompra.elsuper.carritocompras")
                composer.gotoScene ("juegos.lacompra.elsuper.menuSuperMercado" , { effect = "fade"} )
            end
        end-- body
        --end)

       

    --[[else
        
        if productosIzq ~=nil and productoIzq~=nil  then
            productoIzq: removeSelf()
            productoIzq=nil
        elseif productosDer ~=nil and productoDer~=nil  then 
            productoDer: removeSelf()
            productoDer=nil
        end
        utils.reproducirSonido("sounds/principales/intentalo",1)
    
    end]]--
end,1)
end
----------------------------
-- @return
---Crea las imagenes que debe de llevar el juego a la derecha y a la izquierda
local function configurarProductos()
    if gameState.tipodeSubJuego == 1 then
        productosIzquierda = "calabazas"
        productosDerecha = "peras"
        btnOrigen="btn1"
    elseif gameState.tipodeSubJuego == 2 then
        productosIzquierda = "cacahuates"
        productosDerecha = "chocolates"
        btnOrigen="btn2"
    elseif gameState.tipodeSubJuego == 3 then
        productosIzquierda = "papa"
        productosDerecha = "tortillas"
        btnOrigen="btn3"
    elseif gameState.tipodeSubJuego == 4 then
        productosIzquierda = "cocas"
        productosDerecha = nil
        btnOrigen="btn4"
    end
    
end

--Decide que imagen debe de crearse al momento de que se toque una de las imagenes de los productos de la izquierda o la derecha
--tap es 2 si es derecha y 1 si es izquierda
local function configurarTipoProductoTap(tap)
    --esto es para quitar el bloqueo 
--    if introIsPlaying == true then
--    local audioChannel = audio.stop() 
--   introIsPlaying = false
--    end
    local variableProducto=""

    if tap == 1 then -- toque a la izquierda
        if gameState.tipodeSubJuego == 1 then -- si el tipo es 1 y se cliqueo a la izquierda
            variableProducto = "calabaza"
        elseif gameState.tipodeSubJuego == 2 then
            variableProducto = "cacahuate"
        elseif gameState.tipodeSubJuego == 3 then
            variableProducto = "papas"
        elseif gameState.tipodeSubJuego == 4 then
            variableProducto = "coca"
        end
    else --toque a la derecha
        if gameState.tipodeSubJuego == 1 then
            variableProducto = "pera"    
        elseif gameState.tipodeSubJuego == 2 then
            variableProducto = "chocolate"
        elseif gameState.tipodeSubJuego == 3 then
            variableProducto = "tortilla"
        elseif gameState.tipodeSubJuego == 4 then
            variableProducto = nil
        end  
    end
    
    return variableProducto
end

local function btnTapIzq(event)
    --esto es para quitar el bloqueo 
--    if introIsPlaying == true then
--    local audioChannel = audio.stop() 
--   introIsPlaying = false
--    end
    if event.phase == "began" then
        if productoDer ~=nil and productosDer~=nil then
               productoDer:removeSelf()
               productoDer=nil
        end    

        if productoIzq == nil then
            --print "boton tap"
            local variableProducto = configurarTipoProductoTap(1)
            productoIzq = display.newImage(grupoImagenes,"images/lacompra/elsuper/"..variableProducto..".png" )
            productoIzq.id=variableProducto
            crearProducto(productoIzq,event)
        end
    end
   return true
end

local function btnTapDer(event)
    --esto es para quitar el bloqueo 
--    if introIsPlaying == true then
--    local audioChannel = audio.stop() 
--   introIsPlaying = false
--    end
    if event.phase == "began" then
        if productoIzq ~=nil and productosIzq~=nil then
            productoIzq:removeSelf()
            productoIzq=nil
         end

        if productoDer == nil then  
            local variableProducto = configurarTipoProductoTap(2)
            productoDer = display.newImage( grupoImagenes,"images/lacompra/elsuper/"..variableProducto..".png" )
            productoDer.id=variableProducto
            --print ("posicion:"..event.x..","..event.y)
            crearProducto(productoDer,event)
         end 
    end
   return true
end


----------------------------------------------------------------------------------------
local function btnTap(event)
    --esto es para quitar el bloqueo 
--    if introIsPlaying == true then
--    local audioChannel = audio.stop() 
--   introIsPlaying = false
--    end
   utils.reproducirSonido("boton")
   composer.gotoScene ( event.target.destination, { effect = "fade"} )
   print(event.target.destination)
   return true
end

-- Called when the scene's view does not exist:
function scene:create( event )
	local group = self.view
        grupoImagenes = group
        configurarProductos()
        
        local background = display.newImage( group,"images/lacompra/elsuper/fondo.png" )
        background:translate( centerX, centerY )
        
        if productosIzquierda~=nil then
            productosIzq = display.newImage( group,"images/lacompra/elsuper/".. productosIzquierda ..".png" )
            productosIzq:translate( centerX*.4, centerY*.80 )
            productosIzq:addEventListener("touch", btnTapIzq)
        
        end
        if productosDerecha~=nil then
            productosDer = display.newImage( group,"images/lacompra/elsuper/"..productosDerecha..".png" )
            productosDer:translate( centerX*1.6, centerY*.75 )
            productosDer:addEventListener("touch", btnTapDer)
        end
        
        local topsign = display.newImage( group,"images/top-sign.png" )
        topsign:translate( centerX, centerY/5 )
        
        local carmen = display.newImage( group,"images/lacompra/elsuper/carmen.png" )
        carmen:translate( centerX*1.3, centerY*1.1 )
        
        carrito = display.newImage( group,"images/lacompra/elsuper/carrito.png" )
        carrito:translate( centerX, centerY*1.3 )
        
        carrito.w = 41
        carrito.h = 22
        -- Asignacion de eventos a los estantes derecha e izquierda
        
       
        
      
        
        
        local btnRegresar = display.newImage( group,"images/btn-regresar.png" )
        btnRegresar:translate( centerX/4, centerY*1.85 )
        btnRegresar:addEventListener("tap", btnTap)
        btnRegresar.destination = "juegos.lacompra.elsuper.menuSuperMercado"
        local inst = display.newText( optionsTextMenu )
        group:insert(inst)
        inst.text="Arrastra al carrito"
        utils.reproducirSonido("sounds/lacompra/jugar/ElSuper/Instrucciones3", 0)
--        composer.removeScene( "bloqueo" )
--        introIsPlaying=true 
        
end


-- Called immediately after scene has moved onscreen:
function scene:show( event )
	-- INSERT code here (e.g. start timers, load audio, start listeners, etc.)
	local group = self.view

end

-- Called when scene is about to move offscreen:
function scene:hide( event )
	local group = self.view
	
	composer.removeScene( composer.getSceneName( "current" ) )
	
	--Runtime:removeEventListener("enterFrame", update)
	-- INSERT code here (e.g. stop timers, remove listeners, unload sounds, etc.)
	-- Remove listeners attached to the Runtime, timers, transitions, audio tracks
end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroy( event )
	local group = self.view
        grupoImagenes:removeSelf()
        grupoImagenes=nil
        
       -- INSERT code here (e.g. remove listeners, widgets, save state, etc.)
	-- Remove listeners attached to the Runtime, timers, transitions, audio tracks

end

---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene---------------------------------------------------------

