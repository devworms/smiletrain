local composer = require( "composer" )
local gameState=require("utilerias.gameState")
local scene = composer.newScene()
--local introIsPlaying
local imagenProducto = {}
local elementos={}
local totalelementos=0
local objetoseleccionado
local contador=0
local carrito
local objeto
------VARIABLES DE GRABACION--------------
local r
local isAndroid = false
local dataFileName = "audiopapu"
if "simulator" == system.getInfo("environment") then
    dataFileName = dataFileName .. ".aif"
else
	local platformName = system.getInfo( "platformName" )
    if "iPhone OS" == platformName then
        dataFileName = dataFileName .. ".aif"
    elseif "Android" == platformName then
        dataFileName = dataFileName .. ".wav"
        isAndroid = true
    else
    	print("Unknown OS " .. platformName )
    end
end
local filePath = system.pathForFile( dataFileName, system.DocumentsDirectory )
r = media.newRecording(filePath)
--funcion para limpiar la memoria yliberar el audio
local function llamame(event)
    print("khe")
   
    audio.dispose(event.handle)
end
-------fin de las variables de grabacion---------
 gameState.success = 0
local function onTouch(object, event)
    --esto es para quitar el bloqueo 
--    if introIsPlaying == true then
--    local audioChannel = audio.stop() 
--   introIsPlaying = false
--    end
    objetoseleccionado=object.number
    objeto=object.id
    utils.primerSonido("juegos.lacompra.lacompra",object.id)
end

local function btnTap(event)
    --esto es para quitar el bloqueo 
--    if introIsPlaying == true then
--    local audioChannel = audio.stop() 
--   introIsPlaying = false
--    end
    utils.tipoImagen()
    utils.reproducirSonido("boton")
    composer.gotoScene ( event.target.destination, { effect = "fade"} )
    print(event.target.destination)
    return true
end

local function mover()
    
    imagenProducto[objetoseleccionado+1]:removeSelf()
    
    contador=contador+1
    elementos[objetoseleccionado].isVisible=true
    
    if contador==1 then
        transition.to( elementos[objetoseleccionado],{x=centerX-((carrito.contentWidth/8)),y=centerY+(centerY/2)-centerY/20})
    elseif contador==2 then
         transition.to( elementos[objetoseleccionado],{x=centerX-((carrito.contentWidth/8)*2),y=centerY+(centerY/2)-centerY/20})
    elseif contador==3 then
         transition.to( elementos[objetoseleccionado],{x=centerX+((carrito.contentWidth/8)),y=centerY+(centerY/2)-centerY/20})
    elseif contador==4 then
         transition.to( elementos[objetoseleccionado],{x=centerX,y=centerY+(centerY/2)})
    elseif contador==5 then
         transition.to( elementos[objetoseleccionado],{x=centerX-((carrito.contentWidth/8)),y=centerY+(centerY/2)-centerY/8})
    elseif contador==6 then
         transition.to( elementos[objetoseleccionado],{x=centerX-((carrito.contentWidth/8)*2),y=centerY+(centerY/2)-centerY/8})
    elseif contador==7 then
         transition.to( elementos[objetoseleccionado],{x=centerX+((carrito.contentWidth/8)),y=centerY+(centerY/2)-centerY/8})
    elseif contador==8 then
         transition.to( elementos[objetoseleccionado],{x=centerX ,y=centerY+(centerY/2)-centerY/8})
     end    
    
end
function scene:successful() 
    
    --local pronuncia = utils.validarVoz(objeto)
    --local pronuncia = true
  
    --[[if pronuncia == true then
        local r=math.random(1,2)
       local ruta
        if r==1 then
            ruta="excelente"
        else
            ruta="muybien"
        end
        utils.reproducirSonido("sounds/principales/"..ruta)]]--
        -------------------********inicio del record********----------------------
        print("presionado")
    utils.reproducirSonido("bip")
timer.performWithDelay(700, function()
   r:startRecording()
   end,1)
   
    timer.performWithDelay(3000, function() 
                    r:stopRecording()
                    utils.reproducirSonido("bup")
                    timer.performWithDelay(1000, function()
            -- Play back the recording
            local file = io.open( filePath, "r" ) 
            if file then
                io.close( file )
            local playbackSoundHandle = audio.loadStream( dataFileName, system.DocumentsDirectory )
				audio.play( playbackSoundHandle, { onComplete= llamame} )
              end 
              end,1)
                end,1)
----------**********fin de el record*****------------------------------
timer.performWithDelay(6000, function() 
        mover()
        composer.removeScene( "bloqueo" )
        gameState.success = gameState.success+1
          
           if gameState.success >= 8 then
                timer.performWithDelay(1250, function()	
                    utils.ponerGlobos()
                end,1)
                gameState.success = 0
            end
            end,1)
     --[[else
       
      utils.reproducirSonido("sounds/principales/intentalo",1)
       
    end]]--
  
    
end
   
function scene:create( event )
    local group = self.view 
    local background = display.newImage(group,path.."lacompra/fondo.png")
     background:translate( centerX, centerY )
    background:toBack()
    
    
     for count = 1, 8 do
         imagenProducto[count] = display.newImage(group,path.."lacompra/botones/"..count..".png")
         imagenProducto[count].x = (((centerX*2)/17)*(count*2))-(centerX/17)
         imagenProducto[count].y = centerY/3
         imagenProducto[count].myName = count
         imagenProducto[count].number=totalelementos
         utils.renombrarImagenes(14,imagenProducto[count])
         imagenProducto[count].touch = onTouch	
         imagenProducto[count]:addEventListener( "touch", imagenProducto[count] )
         elementos[totalelementos] = display.newImage(group,path.."lacompra/elementos/"..count..".png")
         elementos[totalelementos].x = (((centerX*2)/17)*(count*2))-(centerX/17)
         elementos[totalelementos].y = centerY/3
         elementos[totalelementos].isVisible=false
         totalelementos=totalelementos+1
         
     end
      
    carrito=display.newImage(group,path.."lacompra/carrito.png")
    carrito:translate( centerX, centerY+(centerY/9) )
    
    local btnRegresar = display.newImage( group,"images/btn-regresar.png" )
        btnRegresar:translate( centerX/4, centerY*1.85 )
        btnRegresar:addEventListener("tap", btnTap)
        btnRegresar.destination = "menuJuego"
    utils.reproducirSonido("sounds/lacompra/jugar/laCompra/Instrucciones", 0)
--    composer.removeScene( "bloqueo" )
--    introIsPlaying=true 
end

function scene:show( event )
	local group = self.view 
        
end

function scene:hide( event )
	local group = self.view
	
	local phase = event.phase
	
	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
		
	elseif phase == "did" then
		-- Called when the scene is now off screen
	end	
        
         composer.removeScene( composer.getSceneName( "current" ) )
	
end

function scene:destroy( event )

	-- Called prior to the removal of scene's "view" (group)
	-- 
	-- INSERT code here to cleanup the scene
	-- e.g. remove display objects, remove touch listeners, save state, etc.
	local group = self.view
	
	
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene