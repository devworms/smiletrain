local composer = require( "composer" )
local scene = composer.newScene()
local gameState=require("utilerias.gameState")
local lfs = require("lfs") --sistema de archivos

local video = ""
local textLoad
local recordVoice = false
local recording
local SpriteNuevo
local banderaPop = false
local codePaciente = ""
local dialogText
local textBox

local groupG  

local function testPath(pathPaciente)
  -- "/mnt/sdcard"
  -- system.pathForFile( nil, system.DocumentsDirectory )
  -- using only "/" will printout all the files and directories, so I used /mnt and found extSdCard
  local path = pathPaciente 
  local pathType = ""
   
  -- Check to see if path exists
  if path and lfs.attributes( path ) then
     pathType = lfs.attributes( path ).mode
     print("PATHTYPE: " .. pathType)
  else
    print("NOT PATHTYPE")

    return false --cuando no se encuenta el path

  end--
   
  -- Check if path is a directory
  if pathType == "directory" then
    for file in lfs.dir( path ) do
        if "." ~= file and ".." ~= file then
        print("FILE: " .. file)
           -- Get the file attributes.
           local fileAtr = lfs.attributes( path .. "/" .. file )
           -- Print path, name and type of file (Directory or file)
           if fileAtr ~= nil then 
              print("FILEATR: "..path,file,fileAtr.mode) 
           else
              print("NO FILEATR")
           end
        end
     end
  else
    print("NOT FILE DIRECTORY: ")
  end

  return true

end

local function btnTap(event)
    utils.tipoImagen()
    utils.reproducirSonido("boton")
    composer.gotoScene ( event.target.destination, { effect = "fade"} )
    print(event.target.destination)
    return true
end

local onCompleteVideo = function( event )
        print( "video session ended" )
        if recordVoice then
            recording:stopRecording()
            --recording:stopTuner()
            recordVoice = false
        end
end

local function playVideo()
    
    if recordVoice then -- reproduce el video, graba y guarda la voz

        if dialogText == nil then
            dialogText = display.newImage( groupG, "images/menu/paciente.png" )
            dialogText:translate( centerX*1.45, centerY*1.35 )
            
            textBox = native.newTextBox( centerX*1.48, centerY*1.4, 180, 55 )
            textBox.isEditable = true
            textBox:addEventListener( "userInput", function ( event )
                                                    if event.phase == "began" then
                                                        --print("began".. event.text )

                                                    elseif  event.phase == "submitted" or event.phase == "ended" then

                                                        -- do something with textBox text
                                                        --print("ended: ".. event.target.text )
                                                        
                                                    elseif event.phase == "editing" then
                                                        --[[
                                                        print( event.newCharacters )
                                                        print( event.oldText )
                                                        print( event.startPosition )
                                                        print( event.text )
                                                        ]]
                                                        codePaciente = event.text
                                                    end
                                                end )
			banderaPop=true
   		elseif banderaPop == true and dialogText ~= nil then

			native.showAlert( "Aviso", "Revisa tu código de nuevo" ,{ "OK"})
			banderaPop=false
        else

            dialogText:removeSelf()
            textBox:removeSelf()
            textBox = nil

            local pathVideo = "/mnt/sdcard/SmileTrain/"..codePaciente.."/Dinamicas"
            local pathEnabled = testPath( pathVideo )
            print("pathEnabled ".. tostring(pathEnabled) )

            if not(pathEnabled) then
                local temp_path = "/mnt/sdcard"

                -- change current working directory
                local success = lfs.chdir( temp_path ) -- returns true on success

                if success then
                    local another_temp_path = "SmileTrain/"..codePaciente
                   lfs.mkdir( another_temp_path ) --crea carpeta
                   lfs.chdir( temp_path .."/".. another_temp_path ) -- la pone en path
                   lfs.mkdir("Dinamicas") --crea otra carpeta
                   print("success")
                   --new_folder_path = lfs.currentdir() .. "/MyNewFolder"
                else
                    print("NO success")
                end

            end

            local date = os.date( "*t" )
            local nameFile = video.."_"..date.day.."_"..date.month.."_"..date.year
                            .."_"..date.hour.."_"..date.min.."_"..date.sec..".mp3"

            media.playVideo( video..".mp4" , system.DocumentsDirectory, true, onCompleteVideo )

            local recPath = pathVideo.."/"..nameFile

                                                        -- http://stackoverflow.com/questions/8443291/how-to-load-images-from-sdcard-in-corona-sdk
            local file = io.open (recPath ,"w")
                    io.close(file) 

            recording = media.newRecording(recPath)
            --recording:startTuner()
            recording:startRecording()
        end

        

    else -- solo reproduce el video
    
        media.playVideo( video..".mp4" , system.DocumentsDirectory, true, onCompleteVideo )
        --media2.playVideo( video..".mp4" , system.DocumentsDirectory, true, onCompleteVideo )
        print(system.DocumentsDirectory)
    end
end

local function networkListener( event )
    if ( event.isError ) then
        print( "Network error - download failed" )
        textLoad.text = "No hay conexión a internet"
        timer.performWithDelay(1200, function()	
                                        SpriteNuevo:pause()
                                        SpriteNuevo:removeSelf()
                                        textLoad:removeSelf()
					composer.removeScene( "bloqueo" )
					end, 1)
        
    elseif ( event.phase == "began" ) then
        print( "Progress Phase: began" )
    elseif ( event.phase == "ended" ) then
        print( "Displaying response video file" )
        --[[print( event.response.filename )
        print( event.response.baseDirectory )]]
        SpriteNuevo:pause()
        SpriteNuevo:removeSelf()
        textLoad:removeSelf()
        composer.removeScene( "bloqueo" )
       -- media.playVideo( event.response.filename , event.response.baseDirectory, true, onCompleteVideo )
        playVideo()
    end
end

local function doesFileExist( fname, path )

    local results = false

    local filePath = system.pathForFile( fname, path )

    --filePath will be 'nil' if file doesn't exist and the path is 'system.ResourceDirectory'
    if ( filePath ) then
        filePath = io.open( filePath, "r" )
    end

    if ( filePath ) then
        print( "File found: " .. fname )
        --clean up file handles
        filePath:close()
        results = true
    else
        print( "File does not exist: " .. fname )
    end

    return results
end

local function btnPlay(event)     
    
    if event.target.name == "record" then
        recordVoice = true
    end
    
     --check for file in 'system.DocumentsDirectory'
    local ress = doesFileExist( video..".mp4", system.DocumentsDirectory )
    --print ("existe en system.DocumentsDirectory ::::   "..tostring(ress))

    --[[check for file in 'system.ResourceDirectory'
    local res = doesFileExist( video )
    print ("existe en system.ResourceDirectory ::::   "..tostring(res)) ]]
    
    if ress then
        playVideo()
        --print ("ruta system.DocumentsDirectory: "..tostring(system.DocumentsDirectory))
    else    
        composer.showOverlay( "bloqueo" ,{ isModal = true } )
        
        local spriteTam = { width=128, height=128,  numFrames=12 }
	    local spriteImag = graphics.newImageSheet( "images/menu/loader.png", spriteTam )
	    local sequenceDataSprite =
				{
				    { name="loader", start=1, count=12, time=700, loopCount=0 }
				}
	    SpriteNuevo = display.newSprite( spriteImag, sequenceDataSprite )
		SpriteNuevo.x = centerX
		SpriteNuevo.y = centerY*0.5
                SpriteNuevo.xScale=.5
		SpriteNuevo.yScale=.5
        SpriteNuevo:play()
        
        textLoad = display.newText("Espera un momento por favor...", 
                                centerX, centerY*1.5, "fonts/GothamNarrow-Book", 25)
        textLoad:setTextColor(0, 0, 0)
        
        local params = {}
        params.progress = true

        network.download(
            "http://smiletrainla.org/Videos/"..video..".mp4",
            "GET",
            networkListener,
            params,
            video..".mp4",
            system.DocumentsDirectory
        )
    end 
     
end

local function tipoJuego()
    if  gameState.tipodejuego == 1 then -- la compra
        video = "la_compra"
    elseif  gameState.tipodejuego == 2 then -- el viaje
        video = "el_viaje"
    elseif  gameState.tipodejuego == 3 then -- cochinitos
        video = "los_cochinitos"
    elseif  gameState.tipodejuego == 4 then -- los sonidos
        video = "los_sonidos"
    elseif  gameState.tipodejuego == 5 then -- popi
        video = "Popi"
    elseif gameState.tipodejuego == 6 then  -- tiranosaurio
        video = "tiranosaurio"
    end    
end 

-- Called when the scene's view does not exist:
function scene:create( event )
	local group = self.view

	groupG = group
        
        tipoJuego()
        
        local background = display.newImage( group,path.."fondo-1.png" )--fondo video
        background:translate( centerX, centerY )
        --local topsign = display.newImage( group,"images/top-sign.png" )
        --topsign:translate( centerX, centerY/5 )
        
        local btnJ1 = display.newImage( group,"images/menu/play.png" )
        btnJ1:translate( centerX, centerY )
        btnJ1:addEventListener("tap", btnPlay)
        btnJ1.name = "video"
        
        local btnJ2 = display.newImage( group,"images/menu/record.png" )
        btnJ2:translate( centerX*1.75, centerY*1.85 )
        btnJ2:addEventListener("tap", btnPlay)
        btnJ2.name = "record"
        
        ---- +++ video dentro de una pantalla +++ ----
        -- http://docs.coronalabs.com/api/library/native/newVideo.html
        
        local btnRegresar = display.newImage( group,"images/btn-regresar.png" )
        btnRegresar:translate( centerX/4, centerY*1.85 )
        btnRegresar:addEventListener("tap", btnTap)
        btnRegresar.destination = "menuCuento"

          utils.reproducirSonido("sounds/lacompra/Cantar el cuento/Instrucciones",0)

end


-- Called immediately after scene has moved onscreen:
function scene:show( event )
	-- INSERT code here (e.g. start timers, load audio, start listeners, etc.)
	local group = self.view

end

-- Called when scene is about to move offscreen:
function scene:hide( event )
	local group = self.view

	if textBox ~= nil then
		textBox:removeSelf()
	end
                
	composer.removeScene( composer.getSceneName( "current" ) )
	
	-- INSERT code here (e.g. stop timers, remove listeners, unload sounds, etc.)
	-- Remove listeners attached to the Runtime, timers, transitions, audio tracks
end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroy( event )
	local group = self.view

	-- INSERT code here (e.g. remove listeners, widgets, save state, etc.)
	-- Remove listeners attached to the Runtime, timers, transitions, audio tracks

end

---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene---------------------------------------------------------
