local composer = require( "composer" )
local scene = composer.newScene()
local gameState=require("utilerias.gameState")

local buttonEco = {}
local compX = 0.25
local recording
--local introIsPlaying
local function btnTap(event)
    --esto es para quitar el bloqueo 
    --if introIsPlaying == true then
   -- local audioChannel = audio.stop() 
   --introIsPlaying = false
    --end
    utils.tipoImagen()
    utils.reproducirSonido("boton")
    composer.gotoScene ( event.target.destination, { effect = "fade"} )
    print(event.target.destination)
    return true
end

local function stopRec()
    recording:stopRecording()
    --recording:stopTuner()
     
    local recorded = audio.loadStream('myRecording.aif', system.DocumentsDirectory)
    audio.play(recorded, {onComplete = function()
                                            for count = 1, 4 do
                                                buttonEco[count].alpha = 1
                                            end
                                            composer.removeScene( "bloqueo" )
                                        end})
end

local btnRecord = function( event )
    --esto es para quitar el bloqueo 
    --if introIsPlaying == true then
    --local audioChannel = audio.stop() 
   --introIsPlaying = false
    --end
    composer.showOverlay( "bloqueo" ,{ isModal = true } )
    
    local recPath = system.pathForFile('myRecording.aif', system.DocumentsDirectory)
    recording = media.newRecording(recPath)
    --recording:startTuner()
    recording:startRecording()
    
    for count = 1, 4 do
        if event.target.name == count then
            buttonEco[count].alpha = 0.7
        else
            buttonEco[count].alpha = 0.3
        end
    end

   timer.performWithDelay( 3500, stopRec )
   
end

-- Called when the scene's view does not exist:
function scene:create( event )
	local group = self.view

        local background = display.newImage( group,path.."fondo.png" )
        background:translate( centerX, centerY )
        local topsign = display.newImage( group,"images/top-sign.png" )
        topsign:translate( centerX, centerY/5 )
         inst = display.newText( optionsTextMenu )
        group:insert(inst)
        inst.text="Toca el elemento que deseas practicar"
        for count = 1, 4 do
            buttonEco[count] = display.newImage(group,path.."eco/"..count..".png")
            buttonEco[count]:translate( centerX*compX, centerY*1.15 )
            buttonEco[count].name = count
            buttonEco[count]:addEventListener("tap", btnRecord)
            
            compX = compX+0.50
        end
        
        
        local btnRegresar = display.newImage( group,"images/btn-regresar.png" )
        btnRegresar:translate( centerX/4, centerY*1.85 )
        btnRegresar:addEventListener("tap", btnTap)
        btnRegresar.destination = "menuCuento"

        utils.reproducirSonido("sounds/lacompra/repetirlapalabras/repite",0)
        --composer.removeScene( "bloqueo" )
       --introIsPlaying=true 
        
end


-- Called immediately after scene has moved onscreen:
function scene:show( event )
	-- INSERT code here (e.g. start timers, load audio, start listeners, etc.)
	local group = self.view

end

-- Called when scene is about to move offscreen:
function scene:hide( event )
	local group = self.view
                
	composer.removeScene( composer.getSceneName( "current" ) )
	
	--Runtime:removeEventListener("enterFrame", update)
	-- INSERT code here (e.g. stop timers, remove listeners, unload sounds, etc.)
	-- Remove listeners attached to the Runtime, timers, transitions, audio tracks
end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroy( event )
	local group = self.view

	-- INSERT code here (e.g. remove listeners, widgets, save state, etc.)
	-- Remove listeners attached to the Runtime, timers, transitions, audio tracks

end

---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene---------------------------------------------------------


