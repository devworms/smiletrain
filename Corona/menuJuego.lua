local composer = require( "composer" )
local gameState=require("utilerias.gameState")
local scene = composer.newScene()
local destino1, destino2, destino3, destino4, destino5
local txt1, txt2, txt3, txt4
--local introIsPlaying
local function btnTap(event)
     --esto es para quitar el bloqueo 
    --if introIsPlaying == true then
    --local audioChannel = audio.stop() 
  -- introIsPlaying = false
   -- end
    utils.tipoImagen()
    utils.reproducirSonido("boton")
    composer.gotoScene ( event.target.destination, { effect = "fade"} )
    print(event.target.destination)
    return true
end

local function tipoJuego()
    if  gameState.tipodejuego == 1 then -- la compra
        destino1 = "juegos.memorama"
        destino2 = "juegos.diferencias"
        destino3 = "juegos.lacompra.lacompra"
        destino4 = "juegos.lacompra.elsuper.elsuper"
        
    elseif  gameState.tipodejuego == 2 then -- el viaje
        destino1 = "juegos.memorama"
        destino2 = "juegos.elviaje.maleta"
        destino3 = "juegos.diferencias"
        destino4 = "juegos.elviaje.viaje"
        
    elseif  gameState.tipodejuego == 3 then -- cochinitos
        destino1 = "juegos.cochinito.teacuerdas"
        destino2 = "juegos.memorama"
        destino3 = "juegos.diferencias"
        destino4 = "juegos.cochinito.cochinito"
        destino5 = "juegos.cochinito.lacasa"
    elseif  gameState.tipodejuego == 4 then -- los sonidos
        destino1 = "juegos.memorama"
        destino2 = "juegos.lossonidos.recuerda"
        destino3 = "juegos.diferencias"
        destino4 = "juegos.elviaje.maleta"
        
    elseif  gameState.tipodejuego == 5 then -- popi
        destino1 = "juegos.memorama"
        destino2 = "juegos.popi.encasa.menuEncasa"
        destino3 = "juegos.elviaje.maleta"
        destino4 = "juegos.popi.ruleta"
        
    elseif gameState.tipodejuego == 6 then -- tiranosaurio
        destino1 = "juegos.memorama"
        destino2 = "juegos.tiranosaurio.arrastrarTiranosaurio"
        destino3 = "juegos.tiranosaurio.rompecabezas"
        destino4 = "juegos.tiranosaurio.iluminar"
        
    end    
end 

-- Called when the scene's view does not exist:
function scene:create( event )
	local group = self.view
        
        tipoJuego()
        if  gameState.tipodejuego==1 and gameState.menucompra==false then
             utils.reproducirSonido("sounds/menu/lacompra", 0)
             gameState.menucompra=true
        elseif  gameState.tipodejuego==2 and gameState.menuviaje==false then
                utils.reproducirSonido("sounds/menu/viaje", 0)
                 gameState.menuviaje=true
        elseif  gameState.tipodejuego==3 and gameState.menucochi==false then
            utils.reproducirSonido("sounds/menu/cochi", 0)
             gameState.menucochi=true
        elseif  gameState.tipodejuego==4 and gameState.menusonido==false then
                utils.reproducirSonido("sounds/menu/sonidos", 0)
                 gameState.menusonido=true
        elseif  gameState.tipodejuego==5 and gameState.menupopi==false then
                utils.reproducirSonido("sounds/menu/popi", 0)
                 gameState.menupopi=true
        elseif  gameState.tipodejuego==6 and gameState.menudino==false then
                utils.reproducirSonido("sounds/menu/dino", 0)
                 gameState.menudino=true
        end
        --composer.removeScene( "bloqueo" )
         --introIsPlaying=true

       
        
      local  optionsText = 
        {
            --parent = group,
            text = "",
            width = 140,
            align = "center",--required for multi-line and alignment
            font = "fonts/GothamNarrow-Book",
            fontSize = 25
        }
        -- Create first multi-line text object


        local pos1,pos2, pos3, pos4 = centerX/4, centerX/1.35, centerX*1.25, centerX*1.75
       

        local background = display.newImage( group,path.."fondo-1.png" )
        background:translate( centerX, centerY )
        local topsign = display.newImage( group,"images/top-sign.png" )
        topsign:translate( centerX, centerY/5 )

            
        
        if  gameState.tipodejuego == 3 then

            pos1,pos2, pos3, pos4 = centerX/5, centerX/1.7, centerX, centerX*1.4
            
            local btnJ5 = display.newImage( group,path.."boton5.png" )
            btnJ5:translate( centerX*1.8, centerY*1.1 )
            btnJ5:addEventListener("tap", btnTap)
            btnJ5.destination = destino5
            
        end

            local btnJ1 = display.newImage( group,path.."boton1.png" )
            btnJ1:translate( pos1, centerY*1.1 )
            btnJ1:addEventListener("tap", btnTap)
            btnJ1.destination = destino1
            
            local btnJ2 = display.newImage( group,path.."boton2.png" )
            btnJ2:translate( pos2, centerY*1.1 )
            btnJ2:addEventListener("tap", btnTap)
            btnJ2.destination = destino2
            
            local btnJ3 = display.newImage( group,path.."boton3.png" )
            btnJ3:translate( pos3, centerY*1.1 )
            btnJ3:addEventListener("tap", btnTap)
            btnJ3.destination = destino3
            
            local btnJ4 = display.newImage( group,path.."boton4.png" )
            btnJ4:translate( pos4, centerY*1.1 )
            btnJ4:addEventListener("tap", btnTap)
            btnJ4.destination = destino4
        
        
        local btnRegresar = display.newImage( group,"images/btn-regresar.png" )
        btnRegresar:translate( centerX/4, centerY*1.85 )
        btnRegresar:addEventListener("tap", btnTap)
        btnRegresar.destination = "menuCuento"
        local inst = display.newText( optionsTextMenu )
        group:insert(inst)
        inst.text="Escoge un juego"
end


-- Called immediately after scene has moved onscreen:
function scene:show( event )
	-- INSERT code here (e.g. start timers, load audio, start listeners, etc.)
	local group = self.view

end

-- Called when scene is about to move offscreen:
function scene:hide( event )
	local group = self.view
                
	composer.removeScene( composer.getSceneName( "current" ) )
	
	--Runtime:removeEventListener("enterFrame", update)
	-- INSERT code here (e.g. stop timers, remove listeners, unload sounds, etc.)
	-- Remove listeners attached to the Runtime, timers, transitions, audio tracks
end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroy( event )
	local group = self.view

	-- INSERT code here (e.g. remove listeners, widgets, save state, etc.)
	-- Remove listeners attached to the Runtime, timers, transitions, audio tracks

end

---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene---------------------------------------------------------
