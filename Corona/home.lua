local composer = require( "composer" )
local scene = composer.newScene()
local gameState = require("utilerias.gameState")
local musicafondochanel=nil
local nube
--local introIsPlaying
local function btnTap(event)
    --[[esto es para quitar el bloqueo 
    if introIsPlaying == true then
    local audioChannel = audio.stop() 
   introIsPlaying = false
    end]]--
    audio.stop(musicafondochanel)  
    audio.dispose(musicafondochanel)
    musicafondochanel=nil
    utils.reproducirSonido("boton")
    composer.gotoScene ( event.target.destination, { effect = "fade"} )
   
   print(event.target.destination)
   return true
end

local function Paralax( )
	nube.x = nube.x - 3
        
        if (nube.x < -45 )  then
 		nube.x = (centerX*2)+45
 	end

end

function scene:create( event )
    local group = self.view
    
    
    
    if  gameState.home==false then
         utils.reproducirSonido("sounds/home/appst1",0)
         --composer.removeScene( "bloqueo" )
         --introIsPlaying=true 
         gameState.home=true
    end
    
    local background = display.newImage( group,"images/fondo.png" )
    background:translate( centerX, centerY )
    local btnNinos = display.newImage( group,"images/btn-ninos.png" )
    btnNinos:translate( centerX, centerY )
    btnNinos:addEventListener("tap", btnTap)
    btnNinos.destination = "menu"
    nube = display.newImage( group,"images/nube.png" )
    nube:translate( centerX*1.8 , centerY/4 )
    local topsign = display.newImage( group,"images/top-sign.png" )
    topsign:translate( centerX, centerY/5 )
    
    local btnPadres = display.newImage( group,"images/btn-papas.png" )
    btnPadres:translate( centerX*1.75, centerY*1.85 )
    btnPadres:addEventListener("tap", btnTap)
    btnPadres.destination = "juegos.papas"

    local btnInfo = display.newImage( group,"images/menu/btninfo.png" )
    btnInfo :translate( centerX/6 ,centerY*1.85 )
    btnInfo :addEventListener("tap", btnTap)
    btnInfo.destination = "juegos.info"

    local inst = display.newText( optionsTextMenu )
    group:insert(inst)
 
    inst.text="Bienvenidos"
    
    local musicafondo =audio.loadStream( "sounds/sounds/home/fondo.mp3" )
     musicafondochanel= audio.play( musicafondo, { channel=audioChannel, loops= -1 }  ) 
     
     Runtime:addEventListener("enterFrame", Paralax )
    
end
 
-- Called immediately after scene has moved onscreen:
function scene:show( event )
	local group = self.view
	-- INSERT code here (e.g. start timers, load audio, start listeners, etc.)
        
end
 
-- Called when scene is about to move offscreen:
function scene:hide( event )
	local group = self.view
        
        Runtime:removeEventListener("enterFrame", Paralax)
        composer.removeScene( composer.getSceneName( "current" ) )
 	collectgarbage()
	-- INSERT code here (e.g. stop timers, remove listeners, unload sounds, etc.)
	-- Remove listeners attached to the Runtime, timers, transitions, audio tracks
 
end
 
-- Called prior to the removal of scene's "view" (display group)
function scene:destroy( event )
	local group = self.view

	-- INSERT code here (e.g. remove listeners, widgets, save state, etc.)
	-- Remove listeners attached to the Runtime, timers, transitions, audio tracks
 
end
 
---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------
 
-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

return scene

