local composer = require( "composer" )
--local introIsPlaying
local scene = composer.newScene()
local musicafondochanel=nil
local gameState = require("utilerias.gameState")
local function btnTap(event)
    --esto es para quitar el bloqueo 
   -- if introIsPlaying == true then
    --local audioChannel = audio.stop() 
   --introIsPlaying = false
    --end
    utils.tipoImagen()
    utils.reproducirSonido("boton")
    audio.stop(musicafondochanel)  
    audio.dispose(musicafondochanel)
    musicafondochanel=nil
    composer.gotoScene ( event.target.destination, { effect = "fade"} )
   
    print(event.target.destination)
    return true
end  

-- Called when the scene's view does not exist:
function scene:create( event )
	local group = self.view
     if  gameState.menuCuento==false then
         utils.reproducirSonido("sounds/menu/menuCuento",0)
         --composer.removeScene( "bloqueo" )
         --introIsPlaying=true
         gameState.menuCuento=true
        print("entro a sonido---------------------------------------------------------------------------------------------")
        end
       
        local background = display.newImage( group, path.."fondo.png" )
        background:translate( centerX, centerY )
        local topsign = display.newImage( group,"images/top-sign.png" )
        topsign:translate( centerX, centerY/5 )
        
        local btnCantar = display.newImage( group,"images/menu/btn-cantar.png" )
        btnCantar:translate( centerX/3, centerY*1.12 )
        btnCantar:addEventListener("tap", btnTap)
        btnCantar.destination = "juegos.video"
        local btnJugar = display.newImage( group,"images/menu/btn-jugar.png" )
        btnJugar:translate( centerX, centerY*1.12 )
        btnJugar:addEventListener("tap", btnTap)
        btnJugar.destination = "menuJuego"
        local btnRepetir = display.newImage( group,"images/menu/btn-repetir.png" )
        btnRepetir:translate( centerX*1.67, centerY*1.12 )
        btnRepetir:addEventListener("tap", btnTap)
        btnRepetir.destination = "juegos.eco"
        
        local btnRegresar = display.newImage( group,"images/btn-regresar.png" )
        btnRegresar:translate( centerX/4, centerY*1.85 )
        btnRegresar:addEventListener("tap", btnTap)
        btnRegresar.destination = "menu"
         local musicafondo =audio.loadStream( "sounds/sounds/menu/fondo.mp3" )
        musicafondochanel= audio.play( musicafondo, { channel=audioChannel, loops= -1 }  ) 
        local inst = display.newText( optionsTextMenu )
        group:insert(inst)
        inst.text="¿Qué deseas hacer?"
      
    

end


-- Called immediately after scene has moved onscreen:
function scene:show( event )
	-- INSERT code here (e.g. start timers, load audio, start listeners, etc.)
	local group = self.view

end

-- Called when scene is about to move offscreen:
function scene:hide( event )
	local group = self.view
	
	composer.removeScene( composer.getSceneName( "current" ) )
	
	--Runtime:removeEventListener("enterFrame", update)
	-- INSERT code here (e.g. stop timers, remove listeners, unload sounds, etc.)
	-- Remove listeners attached to the Runtime, timers, transitions, audio tracks
end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroy( event )
	local group = self.view

	-- INSERT code here (e.g. remove listeners, widgets, save state, etc.)
	-- Remove listeners attached to the Runtime, timers, transitions, audio tracks

end

---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene---------------------------------------------------------
