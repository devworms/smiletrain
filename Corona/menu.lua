local composer = require( "composer" )
local scene = composer.newScene()
local gameState = require("utilerias.gameState")
local  musicafondochanel=nil
local nube
--local introIsPlaying
-- http://docs.coronalabs.com/api/event/tap/index.html
-- http://docs.coronalabs.com/guide/events/touchMultitouch/index.html
local function btnTap(event)
    --[[esto es para quitar el bloqueo 
    if introIsPlaying == true then
    local audioChannel = audio.stop() 
   introIsPlaying = false
    end]]--
    print(event.target.name)
    audio.stop(musicafondochanel)  
    audio.dispose(musicafondochanel)
    musicafondochanel=nil
    if event.target.name == "compra" then
        gameState.tipodejuego=1
    elseif event.target.name == "popi" then
        gameState.tipodejuego=5
    elseif event.target.name == "viaje" then
        gameState.tipodejuego=2
    elseif event.target.name == "sonidos" then
        gameState.tipodejuego=4
    elseif event.target.name == "tirano" then
        gameState.tipodejuego=6
    elseif event.target.name == "cochinitos" then
        gameState.tipodejuego=3
    end
    
    print("tipo de juego = "..gameState.tipodejuego)
    
    utils.tipoImagen()
    utils.reproducirSonido("boton")
    composer.gotoScene ( event.target.destination, { effect = "fade"} )
    
    return true
end

local function Paralax( )
	nube.x = nube.x - 3
        
        if (nube.x < -45 )  then
 		nube.x = (centerX*2)+45
 	end

end

-- Called when the scene's view does not exist:
function scene:create( event )
	local group = self.view
        if  gameState.menunNino==false then
        utils.reproducirSonido("sounds/menu/appst2",0)
         gameState.menunNino=true
         --composer.removeScene( "bloqueo" )
         --introIsPlaying=true 
        print("entro a sonido")
        end
    
    local background = display.newImage( group,"images/menu/fondo.png" )
        background:translate( centerX, centerY )
        nube = display.newImage( group,"images/nube.png" )
        nube:translate( centerX*1.8, centerY/4 )
        local topsign = display.newImage( group,"images/top-sign.png" )
        topsign:translate( centerX, centerY/5 )
        
        local btnCompra = display.newImage( group,"images/menu/btn-compra.png" )
        btnCompra:translate( centerX/4.5, centerY/1.2 )
        btnCompra:addEventListener("tap", btnTap)
        btnCompra.destination = "menuCuento"
        btnCompra.name = "compra"
        
        local btnCochinos = display.newImage( group,"images/menu/btn-cochinitos.png" )
        btnCochinos:translate( centerX/1.2, centerY/1.2 )
        btnCochinos:addEventListener("tap", btnTap)
        btnCochinos.destination = "menuCuento"
        btnCochinos.name = "cochinitos"
        
        local btnPopi = display.newImage( group,"images/menu/btn-popi.png" )
        btnPopi:translate( centerX*1.5, centerY/1.2 )
        btnPopi:addEventListener("tap", btnTap)
        btnPopi.destination = "menuCuento"
        btnPopi.name = "popi"
           
        local btnViaje = display.newImage( group,"images/menu/btn-viaje.png" )
        btnViaje:translate( centerX/2, centerY*1.4 )
        btnViaje:addEventListener("tap", btnTap)
        btnViaje.destination = "menuCuento"
        btnViaje.name = "viaje"
        
        local btnSonido = display.newImage( group,"images/menu/btn-sonidos.png" )
        btnSonido:translate( centerX*1.2, centerY*1.4 )
        btnSonido:addEventListener("tap", btnTap)
        btnSonido.destination = "menuCuento"
        btnSonido.name = "sonidos"
        
        local btnTiranosaurio = display.newImage( group,"images/menu/btn-tiranosaurio.png" )
        btnTiranosaurio:translate( centerX*1.8, centerY*1.4 )
        btnTiranosaurio:addEventListener("tap", btnTap)
        btnTiranosaurio.destination = "menuCuento"
        btnTiranosaurio.name = "tirano"
        
        local btnRegresar = display.newImage( group,"images/btn-regresar.png" )
        btnRegresar:translate( centerX/4, centerY*1.85 )
        btnRegresar:addEventListener("tap", btnTap)
        btnRegresar.destination = "home"
        
        local musicafondo =audio.loadStream( "sounds/sounds/menu/fondo.mp3" )
        musicafondochanel= audio.play( musicafondo, { channel=audioChannel, loops= -1 }  ) 
        local inst = display.newText( optionsTextMenu )
        group:insert(inst)
        inst.text="Escoge un cuento"
        
        Runtime:addEventListener("enterFrame", Paralax )
end


-- Called immediately after scene has moved onscreen:
function scene:show( event )
	-- INSERT code here (e.g. start timers, load audio, start listeners, etc.)
	local group = self.view        

end

-- Called when scene is about to move offscreen:
function scene:hide( event )
	local group = self.view
        
        Runtime:removeEventListener("enterFrame", Paralax)
	composer.removeScene( composer.getSceneName( "current" ) )
	
	--Runtime:removeEventListener("enterFrame", update)
	-- INSERT code here (e.g. stop timers, remove listeners, unload sounds, etc.)
	-- Remove listeners attached to the Runtime, timers, transitions, audio tracks
end

-- Called prior to the removal of scene's "view" (display group)
function scene:destroy( event )
	local group = self.view

	-- INSERT code here (e.g. remove listeners, widgets, save state, etc.)
	-- Remove listeners attached to the Runtime, timers, transitions, audio tracks

end

---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene---------------------------------------------------------

