-- Tabla de Palabras, solo para la creacion de la base de datos

local tabla_palabras={}
        
    function tabla_palabras.query()
        local query = 

        [[ 

        	 INSERT INTO palabras VALUES (1,'ventana');
        	 INSERT INTO palabras VALUES (2,'puerta');
        	 INSERT INTO palabras VALUES (3,'sillon');
        	 INSERT INTO palabras VALUES (4,'tapete');
        	 INSERT INTO palabras VALUES (5,'cochinito');
        	 INSERT INTO palabras VALUES (6,'flores');
        	 INSERT INTO palabras VALUES (7,'arbusto');
        	 INSERT INTO palabras VALUES (8,'pino');
        	 INSERT INTO palabras VALUES (9,'practico');
        	 INSERT INTO palabras VALUES (10,'violinista');
        	 INSERT INTO palabras VALUES (11,'flautista');
			 INSERT INTO palabras VALUES (12,'rosa');
        	 INSERT INTO palabras VALUES (13,'blanco');
        	 INSERT INTO palabras VALUES (14,'paja');
        	 INSERT INTO palabras VALUES (15,'pon paja');
        	 INSERT INTO palabras VALUES (16,'palos');
        	 INSERT INTO palabras VALUES (17,'ladrillos');
        	 INSERT INTO palabras VALUES (18,'mesa');
        	 INSERT INTO palabras VALUES (19,'cuadro');
        	 INSERT INTO palabras VALUES (20,'camina');
        	 INSERT INTO palabras VALUES (21,'salta');
        	 INSERT INTO palabras VALUES (22,'trepa');
        	 INSERT INTO palabras VALUES (23,'recoge');
        	 INSERT INTO palabras VALUES (24,'corre');
        	 INSERT INTO palabras VALUES (25,'toca');
        	 INSERT INTO palabras VALUES (26,'prende');
        	 INSERT INTO palabras VALUES (27,'camina a la cocina');
        	 INSERT INTO palabras VALUES (28,'toma la olla');
        	 INSERT INTO palabras VALUES (29,'camina a la chimenea');
        	 INSERT INTO palabras VALUES (30,'calienta');

        	 INSERT INTO palabras VALUES (31,'café');
        	 INSERT INTO palabras VALUES (32,'coca');
        	 INSERT INTO palabras VALUES (33,'pera');
        	 INSERT INTO palabras VALUES (34,'calabaza');
        	 INSERT INTO palabras VALUES (35,'tortilla');
        	 INSERT INTO palabras VALUES (36,'carne');
        	 INSERT INTO palabras VALUES (37,'cacahuate');
        	 INSERT INTO palabras VALUES (38,'papas');
        	 INSERT INTO palabras VALUES (39,'chocolate');
        	 INSERT INTO palabras VALUES (40,'leche');
        	 INSERT INTO palabras VALUES (41,'carrito');
        	 INSERT INTO palabras VALUES (42,'frutas');

        	 INSERT INTO palabras VALUES (43,'sueter');
        	 INSERT INTO palabras VALUES (44,'playera');
        	 INSERT INTO palabras VALUES (45,'saco');
        	 INSERT INTO palabras VALUES (46,'pasta dental');
        	 INSERT INTO palabras VALUES (47,'cepillo');
        	 INSERT INTO palabras VALUES (48,'blusa azul');
        	 INSERT INTO palabras VALUES (49,'calcetines');
        	 INSERT INTO palabras VALUES (50,'zapatos');
        	 INSERT INTO palabras VALUES (51,'transporte');
        	 INSERT INTO palabras VALUES (52,'tren');
        	 INSERT INTO palabras VALUES (53,'barco');
        	 INSERT INTO palabras VALUES (54,'autobus');
        	 INSERT INTO palabras VALUES (55,'avion');
        	 INSERT INTO palabras VALUES (56,'destino');
        	 INSERT INTO palabras VALUES (57,'cancun');
        	 INSERT INTO palabras VALUES (58,'puebla');
        	 INSERT INTO palabras VALUES (59,'monterrey');
        	 INSERT INTO palabras VALUES (60,'chiapas');
        	 INSERT INTO palabras VALUES (61,'sinaloa');
        	 INSERT INTO palabras VALUES (62,'pago');
        	 INSERT INTO palabras VALUES (63,'cheque');
        	 INSERT INTO palabras VALUES (64,'tarjeta');
        	 INSERT INTO palabras VALUES (65,'efectivo');
        	 INSERT INTO palabras VALUES (66,'pa');
        	 INSERT INTO palabras VALUES (67,'ta');
        	 INSERT INTO palabras VALUES (68,'ka');
        	 INSERT INTO palabras VALUES (69,'sa');
        	 INSERT INTO palabras VALUES (70,'fa');
        	 INSERT INTO palabras VALUES (71,'sol');
        	 INSERT INTO palabras VALUES (72,'falda');
        	 INSERT INTO palabras VALUES (73,'saco');
        	 INSERT INTO palabras VALUES (74,'foca');
        	 INSERT INTO palabras VALUES (75,'fruta');
        	 INSERT INTO palabras VALUES (76,'sirena');
        	 INSERT INTO palabras VALUES (77,'filete');
			 INSERT INTO palabras VALUES (78,'salchicha');
        	 INSERT INTO palabras VALUES (79,'sopa');
        	 INSERT INTO palabras VALUES (80,'familia');
        	 INSERT INTO palabras VALUES (81,'ka');
        	 INSERT INTO palabras VALUES (82,'te');
        	 INSERT INTO palabras VALUES (83,'po');

        	 INSERT INTO palabras VALUES (84,'tina');
        	 INSERT INTO palabras VALUES (85,'toalla');
        	 INSERT INTO palabras VALUES (86,'jabon');
        	 INSERT INTO palabras VALUES (87,'cortina');
        	 INSERT INTO palabras VALUES (88,'shampoo');
        	 INSERT INTO palabras VALUES (89,'piso');
        	 INSERT INTO palabras VALUES (90,'espejo');
        	 INSERT INTO palabras VALUES (91,'puerta');
        	 INSERT INTO palabras VALUES (92,'popi');
        	 INSERT INTO palabras VALUES (93,'hueso');
        	 INSERT INTO palabras VALUES (94,'tapete');
        	 INSERT INTO palabras VALUES (95,'mesa');
        	 INSERT INTO palabras VALUES (96,'pelota');
        	 INSERT INTO palabras VALUES (97,'disco');
        	 INSERT INTO palabras VALUES (98,'cuerda');
        	 INSERT INTO palabras VALUES (99,'burbujas');
        	 INSERT INTO palabras VALUES (100,'galletas');
        	 INSERT INTO palabras VALUES (101,'croquetas');
        	 INSERT INTO palabras VALUES (102,'salchichas');
        	 INSERT INTO palabras VALUES (103,'carne');
        	 INSERT INTO palabras VALUES (104,'esponja');
        	 INSERT INTO palabras VALUES (105,'cuello');
        	 INSERT INTO palabras VALUES (106,'cabeza');
        	 INSERT INTO palabras VALUES (107,'cola');
        	 INSERT INTO palabras VALUES (108,'pancita');
        	 INSERT INTO palabras VALUES (109,'patas');
        	 INSERT INTO palabras VALUES (110,'manchas');
        	 
        	 INSERT INTO palabras VALUES (111,'cueva');
        	 INSERT INTO palabras VALUES (112,'rocas');
        	 INSERT INTO palabras VALUES (113,'tierra');
        	 INSERT INTO palabras VALUES (114,'polvo');
        	 INSERT INTO palabras VALUES (115,'cerro');
        	 INSERT INTO palabras VALUES (116,'tiranosaurio');
        	 INSERT INTO palabras VALUES (117,'montañas');
        	 INSERT INTO palabras VALUES (118,'arbusto');
        	 INSERT INTO palabras VALUES (119,'pasto');
        	 INSERT INTO palabras VALUES (120,'come carne');
        	 INSERT INTO palabras VALUES (121,'come sandia');
        	 INSERT INTO palabras VALUES (122,'come manzana');
        	 INSERT INTO palabras VALUES (123,'come tuna');
        	 INSERT INTO palabras VALUES (124,'plato');
        	 INSERT INTO palabras VALUES (125,'mochila');
        	 INSERT INTO palabras VALUES (126,'piedra');
        	 INSERT INTO palabras VALUES (127,'camino');
        	 INSERT INTO palabras VALUES (128,'cama');
        	 INSERT INTO palabras VALUES (129,'colcha');
        	 INSERT INTO palabras VALUES (130,'canasta');
                 INSERT INTO palabras VALUES (131,'brontosaurio');        
                 INSERT INTO palabras VALUES (132,'montaña');
                 INSERT INTO palabras VALUES (133,'roca');
                 INSERT INTO palabras VALUES (134,'manzana');
                 INSERT INTO palabras VALUES (135,'naranja');
                 INSERT INTO palabras VALUES (136,'fuego');
                 INSERT INTO palabras VALUES (137,'coco');
                 INSERT INTO palabras VALUES (138,'pera');
                 INSERT INTO palabras VALUES (139,'museo');
                 INSERT INTO palabras VALUES (140,'circo');
                 INSERT INTO palabras VALUES (141,'cine');
                 INSERT INTO palabras VALUES (142,'hospital');
                 INSERT INTO palabras VALUES (143,'cerro');
                 INSERT INTO palabras VALUES (144,'bici');
                 INSERT INTO palabras VALUES (145,'taxi');
                 INSERT INTO palabras VALUES (146,'motocicleta');
                 INSERT INTO palabras VALUES (147,'avion');
                 INSERT INTO palabras VALUES (148,'palo');
                 INSERT INTO palabras VALUES (149,'billetes');
                 INSERT INTO palabras VALUES (150,'monedas');
                 INSERT INTO palabras VALUES (151,'chimenea');
                 INSERT INTO palabras VALUES (152,'lampara');
                 INSERT INTO palabras VALUES (153,'sandia');
                 INSERT INTO palabras VALUES (154,'montaña');
                 INSERT INTO palabras VALUES (155,'montana');
                 INSERT INTO palabras VALUES (156,'foca');
                 INSERT INTO palabras VALUES (157,'sirena');
                 INSERT INTO palabras VALUES (158,'casa');
                 INSERT INTO palabras VALUES (159,'planta');
                 
                 
                 
         ]]
        return query
    end

return tabla_palabras
