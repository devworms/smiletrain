-- Tabla de Palabras, solo para la creacion de la base de datos

    local tabla_coincidencias={}

function tabla_coincidencias.query()
    local query = 
    
        [[ 

             INSERT INTO coincidencias VALUES (1,'ventanas',1);
             INSERT INTO coincidencias VALUES (2,'ventana',1);
             INSERT INTO coincidencias VALUES (3,'ventanal',1);
             INSERT INTO coincidencias VALUES (4,'venta ana',1);


             INSERT INTO coincidencias VALUES (5,'puertas',2);
             INSERT INTO coincidencias VALUES (6,'huertas',2);
             INSERT INTO coincidencias VALUES (7,'muertas',2);
             INSERT INTO coincidencias VALUES (8,'puerta',2);
             INSERT INTO coincidencias VALUES (9,'cómo estas',2);


             INSERT INTO coincidencias VALUES (10,'hey on',3);
             INSERT INTO coincidencias VALUES (11,'mejillón',3);
             INSERT INTO coincidencias VALUES (12,'sillón',3);
             INSERT INTO coincidencias VALUES (13,'zhejiang',3);
             INSERT INTO coincidencias VALUES (14,'he ion',3);


             INSERT INTO coincidencias VALUES (15,'app',4);
             INSERT INTO coincidencias VALUES (16,'para pepe',4);
             INSERT INTO coincidencias VALUES (17,'para psp',4);
             INSERT INTO coincidencias VALUES (18,'para pc',4);
             INSERT INTO coincidencias VALUES (19,'a pepe',4);
             INSERT INTO coincidencias VALUES (20,'tt',4);
             INSERT INTO coincidencias VALUES (21,'tuenti',4);
             INSERT INTO coincidencias VALUES (22,'gente',4);
             INSERT INTO coincidencias VALUES (23,'hackkett',4);
             INSERT INTO coincidencias VALUES (24,'chapete',4);
             INSERT INTO coincidencias VALUES (25,'chaquet',4);
             INSERT INTO coincidencias VALUES (26,'chapter',4);


             INSERT INTO coincidencias VALUES (27,'cochinito',5);
             INSERT INTO coincidencias VALUES (28,'chinito',5);
             INSERT INTO coincidencias VALUES (29,'chinitito',5);
             INSERT INTO coincidencias VALUES (30,'chinitos',5);
             INSERT INTO coincidencias VALUES (31,'hot chinito',5);


             INSERT INTO coincidencias VALUES (32,'flores',6);
             INSERT INTO coincidencias VALUES (33,'florence',6);
             INSERT INTO coincidencias VALUES (34,'floren',6);
             INSERT INTO coincidencias VALUES (35,'florero',6);
             INSERT INTO coincidencias VALUES (36,'flórez',6);
             INSERT INTO coincidencias VALUES (37,'florez',6);


             INSERT INTO coincidencias VALUES (38,'arbusto',7);
             INSERT INTO coincidencias VALUES (39,'arbustos',7);
             INSERT INTO coincidencias VALUES (40,'al gusto',7);
             INSERT INTO coincidencias VALUES (41,'al busto',7);


             INSERT INTO coincidencias VALUES (42,'vino',8);
             INSERT INTO coincidencias VALUES (43,'vinos',8);
             INSERT INTO coincidencias VALUES (44,'tino',8);
             INSERT INTO coincidencias VALUES (45,'chino',8);
             INSERT INTO coincidencias VALUES (46,'pino',8);


             INSERT INTO coincidencias VALUES (47,'wow',9);
             INSERT INTO coincidencias VALUES (48,'tráfico',9);
             INSERT INTO coincidencias VALUES (49,'plástico',9);
             INSERT INTO coincidencias VALUES (50,'ratico',9);
             INSERT INTO coincidencias VALUES (51,'cántico',9);
             INSERT INTO coincidencias VALUES (52,'gráfico',9);
             INSERT INTO coincidencias VALUES (53,'iko iko',9);
             INSERT INTO coincidencias VALUES (54,'5 tráfico',9);
             INSERT INTO coincidencias VALUES (55,'practico',9);
             INSERT INTO coincidencias VALUES (56,'prácticum',9);
             INSERT INTO coincidencias VALUES (57,'prágmatico',9);             
             
             INSERT INTO coincidencias VALUES (58,'kiko',9);
             INSERT INTO coincidencias VALUES (59,'francisco',9);
             INSERT INTO coincidencias VALUES (60,'disco',9);
             INSERT INTO coincidencias VALUES (61,'circo',9);
             INSERT INTO coincidencias VALUES (62,'chico',9);


             INSERT INTO coincidencias VALUES (63,'niña iniesta',10);
             INSERT INTO coincidencias VALUES (64,'violinista violinistas',10);
             INSERT INTO coincidencias VALUES (65,'violines style',10);
             INSERT INTO coincidencias VALUES (66,'violín just style',10);
             INSERT INTO coincidencias VALUES (67,'violín y orquesta',10);
             

             INSERT INTO coincidencias VALUES (68,'autista',11);
             INSERT INTO coincidencias VALUES (69,'flauta',11);
             INSERT INTO coincidencias VALUES (70,'flaws',11);
             INSERT INTO coincidencias VALUES (71,'flaws tío',11);
             INSERT INTO coincidencias VALUES (72,'flaw',11);
             INSERT INTO coincidencias VALUES (73,'flautist',11);


             INSERT INTO coincidencias VALUES (74,'roja',12);
             INSERT INTO coincidencias VALUES (75,'rosas',12);
             INSERT INTO coincidencias VALUES (76,'rosa',12);
             INSERT INTO coincidencias VALUES (77,'rozas',12);


             INSERT INTO coincidencias VALUES (78,'blanco',13);
             INSERT INTO coincidencias VALUES (79,'manco',13);
             INSERT INTO coincidencias VALUES (80,'mango',13);
             INSERT INTO coincidencias VALUES (81,'bano',13);
             INSERT INTO coincidencias VALUES (82,'baño',13);


             INSERT INTO coincidencias VALUES (83,'paja',14);
             INSERT INTO coincidencias VALUES (84,'pajas',14);
             INSERT INTO coincidencias VALUES (85,'ajá',14);
             INSERT INTO coincidencias VALUES (86,'pájaro',14);


             INSERT INTO coincidencias VALUES (87,'juan paja',15);
             INSERT INTO coincidencias VALUES (88,'juan pantoja',15);
             INSERT INTO coincidencias VALUES (89,'paja',15);
             INSERT INTO coincidencias VALUES (90,'fondo paja',15);
             INSERT INTO coincidencias VALUES (91,'juego paja',15);
             INSERT INTO coincidencias VALUES (92,'pon paja',15);
             INSERT INTO coincidencias VALUES (93,'tom paja',15);
             INSERT INTO coincidencias VALUES (94,'pompa ja',15);
             INSERT INTO coincidencias VALUES (95,'pompa',15);
             INSERT INTO coincidencias VALUES (96,'pom paja',15);
             

             INSERT INTO coincidencias VALUES (97,'palos',16);
             INSERT INTO coincidencias VALUES (98,'palo',16);
             INSERT INTO coincidencias VALUES (99,'para los',16);
             INSERT INTO coincidencias VALUES (100,'a los',16);


             INSERT INTO coincidencias VALUES (101,'lagrimeo',17);
             INSERT INTO coincidencias VALUES (102,'ladrillo',17);
             INSERT INTO coincidencias VALUES (103,'la gringa', 17);
             INSERT INTO coincidencias VALUES (104,'la gripe', 17);
             INSERT INTO coincidencias VALUES (105,'la grande',17);


                    	 
             INSERT INTO coincidencias VALUES (106,'mesa',18);
             INSERT INTO coincidencias VALUES (107,'mesas',18);
             INSERT INTO coincidencias VALUES (108,'mesa',18);
             INSERT INTO coincidencias VALUES (109,'meza',18);
             INSERT INTO coincidencias VALUES (110,'me sa',18);
             INSERT INTO coincidencias VALUES (111,'meisa',18);
             INSERT INTO coincidencias VALUES (112,'feisa',18);
             INSERT INTO coincidencias VALUES (113,'feixa',18);



             INSERT INTO coincidencias VALUES (114,'howard',19);
             INSERT INTO coincidencias VALUES (115,'guarde', 19);
             INSERT INTO coincidencias VALUES (116,'josé', 19);
             INSERT INTO coincidencias VALUES (117,'cuadros',19);
             INSERT INTO coincidencias VALUES (118,'cuadro',19);
             
             INSERT INTO coincidencias VALUES (119,'camila',20);
             INSERT INTO coincidencias VALUES (120,'caminar',20);
             INSERT INTO coincidencias VALUES (121,'camina',20);
             INSERT INTO coincidencias VALUES (122,'camilina',20);
             INSERT INTO coincidencias VALUES (123,'jamilena',20); 
             
             INSERT INTO coincidencias VALUES (124,'ya está',21);
             INSERT INTO coincidencias VALUES (125,'ya estás',21);
             INSERT INTO coincidencias VALUES (126,'ya esta',21);
             INSERT INTO coincidencias VALUES (127,'estás',21);
             INSERT INTO coincidencias VALUES (128,'salta',21);
             INSERT INTO coincidencias VALUES (129,'sala',21);
             INSERT INTO coincidencias VALUES (130,'sexta',21);
             INSERT INTO coincidencias VALUES (131,'saica',21);
             INSERT INTO coincidencias VALUES (132,'salta',21);
             INSERT INTO coincidencias VALUES (133,'taika',21);
             INSERT INTO coincidencias VALUES (134,'saltar',21);
             
             INSERT INTO coincidencias VALUES (135,'qué tal',22);
             INSERT INTO coincidencias VALUES (136,'qué tal as',22);
             INSERT INTO coincidencias VALUES (137,'que tal',22);
             INSERT INTO coincidencias VALUES (138,'qué tal ha',22);
             INSERT INTO coincidencias VALUES (139,'qué estás',22);
             INSERT INTO coincidencias VALUES (140,'kettal',22);
             INSERT INTO coincidencias VALUES (141,'que tal',22);
             INSERT INTO coincidencias VALUES (142,'que esta',22);
             INSERT INTO coincidencias VALUES (143,'qué estás',22);
             
             INSERT INTO coincidencias VALUES (144,'escoge',23);
             INSERT INTO coincidencias VALUES (145,'escoger',23);
             INSERT INTO coincidencias VALUES (146,'coge',23);
             INSERT INTO coincidencias VALUES (147,'se coge',23);
             INSERT INTO coincidencias VALUES (148,'se coje',23);
             INSERT INTO coincidencias VALUES (149,'recoge',23);
             INSERT INTO coincidencias VALUES (150,'recoger',23);
             INSERT INTO coincidencias VALUES (151,'recoges',23);
             INSERT INTO coincidencias VALUES (152,'recajo',23);
             INSERT INTO coincidencias VALUES (153,'recas',23);
             INSERT INTO coincidencias VALUES (154,'recoja',23);
             INSERT INTO coincidencias VALUES (155,'recarga',23);
             INSERT INTO coincidencias VALUES (156,'re caja',23);

             INSERT INTO coincidencias VALUES (157,'google',24);
             INSERT INTO coincidencias VALUES (158,'cover',24);
             INSERT INTO coincidencias VALUES (159,'kobe',24);
             INSERT INTO coincidencias VALUES (160,'gogle',24);
             INSERT INTO coincidencias VALUES (161,'coves',24);
             INSERT INTO coincidencias VALUES (162,'corre',24);
             INSERT INTO coincidencias VALUES (163,'corroe',24);
             INSERT INTO coincidencias VALUES (164,'torre',24);
             INSERT INTO coincidencias VALUES (165,'hotmail',24);
             INSERT INTO coincidencias VALUES (166,'jódar',24);
             INSERT INTO coincidencias VALUES (167,'joda',24);
             INSERT INTO coincidencias VALUES (168,'josé',24);
            
             INSERT INTO coincidencias VALUES (169,'caca',25);
             INSERT INTO coincidencias VALUES (170,'kaká',25);
             INSERT INTO coincidencias VALUES (171,'kk',25);
             INSERT INTO coincidencias VALUES (172,'carta',25);
             INSERT INTO coincidencias VALUES (173,'toca',25);
             INSERT INTO coincidencias VALUES (174,'foca',25);
             INSERT INTO coincidencias VALUES (175,'sopcast',25);
             INSERT INTO coincidencias VALUES (176,'tocan',25);
             
             
             INSERT INTO coincidencias VALUES (177,'chile',26);
             INSERT INTO coincidencias VALUES (178,'tele',26);
             INSERT INTO coincidencias VALUES (179,'hitler',26);
             INSERT INTO coincidencias VALUES (180,'lne',26);
             INSERT INTO coincidencias VALUES (181,'prende',26);
             INSERT INTO coincidencias VALUES (182,'pene',26);
             INSERT INTO coincidencias VALUES (183,'penes',26);
             INSERT INTO coincidencias VALUES (184,'pende',26);
             INSERT INTO coincidencias VALUES (185,'prendes',26);
             INSERT INTO coincidencias VALUES (186,'prenden',26);
             INSERT INTO coincidencias VALUES (187,'prender',26);
             INSERT INTO coincidencias VALUES (188,'por ende',26);
             
             INSERT INTO coincidencias VALUES (189,'camino a la cocina', 27);
             INSERT INTO coincidencias VALUES (190,'caminar a la cocina', 27);
             INSERT INTO coincidencias VALUES (191,'camina en la cocina', 27);
             INSERT INTO coincidencias VALUES (192,'caminan en la cocina', 27);
             INSERT INTO coincidencias VALUES (193,'caminar en la cocina', 27);
             INSERT INTO coincidencias VALUES (194,'caminar en la cocin', 27);
             INSERT INTO coincidencias VALUES (195,'camino a la ina', 27);
             INSERT INTO coincidencias VALUES (196,'camino a la', 27);
             INSERT INTO coincidencias VALUES (197,'camino a la oficina', 27);
             
             INSERT INTO coincidencias VALUES (198,'tomó la olla',28);
             INSERT INTO coincidencias VALUES (199,'toma la olla',28);
             INSERT INTO coincidencias VALUES (200,'toma la horts',28);
             INSERT INTO coincidencias VALUES (201,'toma la hora',28);
             INSERT INTO coincidencias VALUES (202,'tómo la 8',28);
             INSERT INTO coincidencias VALUES (203,'tomo la 8',28);
             INSERT INTO coincidencias VALUES (204,'tómo la otxa',28);
             
             INSERT INTO coincidencias VALUES (205,'camina a la chimenea',29);
             INSERT INTO coincidencias VALUES (206,'camina a las chimeneas',29);
             INSERT INTO coincidencias VALUES (207,'camino a la chimenea',29);
             INSERT INTO coincidencias VALUES (208,'amina a la chimenea',29);
             INSERT INTO coincidencias VALUES (209,'amino a la chimenea',29);
             INSERT INTO coincidencias VALUES (210,'a mí no a la chimenea',29);
             INSERT INTO coincidencias VALUES (211,'aminas a la chimenea',29);
             INSERT INTO coincidencias VALUES (212,'a mí ni a la chimenea.',29);
             
             INSERT INTO coincidencias VALUES (213,'caliente',30);
             INSERT INTO coincidencias VALUES (214,'calientes',30);
             INSERT INTO coincidencias VALUES (215,'calienta',30);
             INSERT INTO coincidencias VALUES (216,'alienta',30);
             INSERT INTO coincidencias VALUES (217,'calientas',30);
             INSERT INTO coincidencias VALUES (218,'alienta',30);
             INSERT INTO coincidencias VALUES (219,'aliento',30);
             INSERT INTO coincidencias VALUES (220,'saliente',30);
             INSERT INTO coincidencias VALUES (221,'aliente',30);
             INSERT INTO coincidencias VALUES (222,'aliens',30);
                         
             INSERT INTO coincidencias VALUES (223,'g',31);
             INSERT INTO coincidencias VALUES (224,'je',31);
             INSERT INTO coincidencias VALUES (225,'c',31);
             INSERT INTO coincidencias VALUES (226,'casa',31);
             INSERT INTO coincidencias VALUES (227,'café',31);
             INSERT INTO coincidencias VALUES (228,'caf',31);
             INSERT INTO coincidencias VALUES (229,'cafe',31);
                          
             INSERT INTO coincidencias VALUES (230,'chaca',32);
             INSERT INTO coincidencias VALUES (231,'casa',32);
             INSERT INTO coincidencias VALUES (232,'chao',32);
             INSERT INTO coincidencias VALUES (233,'oa',32);
             INSERT INTO coincidencias VALUES (234,'kaká',32);
             INSERT INTO coincidencias VALUES (235,'caca',32);
             INSERT INTO coincidencias VALUES (236,'coca',32);

             INSERT INTO coincidencias VALUES (237,'hella',33);
             INSERT INTO coincidencias VALUES (238,'gela',33);
             INSERT INTO coincidencias VALUES (239,'telas',33);
             INSERT INTO coincidencias VALUES (240,'tela',33);
             INSERT INTO coincidencias VALUES (241,'pera',33);
             INSERT INTO coincidencias VALUES (242,'pela',33);
             INSERT INTO coincidencias VALUES (243,'terra',33);
             INSERT INTO coincidencias VALUES (244,'pero',33);
             INSERT INTO coincidencias VALUES (245,'telar',33);

             INSERT INTO coincidencias VALUES (246,'alabaza',34);
             INSERT INTO coincidencias VALUES (247,'calabaza',34);
             INSERT INTO coincidencias VALUES (248,'cala bassa',34);
             INSERT INTO coincidencias VALUES (249,'carabasa',34);
             INSERT INTO coincidencias VALUES (250,'caracava',34);


             INSERT INTO coincidencias VALUES (251,'barquilla',35);
             INSERT INTO coincidencias VALUES (252,'cartilla',35);
             INSERT INTO coincidencias VALUES (253,'arquía',35);
             INSERT INTO coincidencias VALUES (254,'partía',35);
             INSERT INTO coincidencias VALUES (255,'quíla',35);
             INSERT INTO coincidencias VALUES (256,'cortila',35);
             INSERT INTO coincidencias VALUES (257,'cortina',35);
             INSERT INTO coincidencias VALUES (258,'totila',35);
             INSERT INTO coincidencias VALUES (259,'totilla',35);
             
             INSERT INTO coincidencias VALUES (260,'harley. gla',36);
             INSERT INTO coincidencias VALUES (261,'henley',36);
             INSERT INTO coincidencias VALUES (262,'hanley',36);
             INSERT INTO coincidencias VALUES (263,'helle',36);
             INSERT INTO coincidencias VALUES (264,'chalet. carne',36);
             INSERT INTO coincidencias VALUES (265,'carnet',36);
             INSERT INTO coincidencias VALUES (266,'chandler',36);
             INSERT INTO coincidencias VALUES (267,'carl',36);

            
            INSERT INTO coincidencias VALUES (268,'caso gárate',37);
            INSERT INTO coincidencias VALUES (269,'talaso white',37);
            INSERT INTO coincidencias VALUES (270,'caso guateque caso white',37);
            INSERT INTO coincidencias VALUES (271,'arahuetes',37);
            INSERT INTO coincidencias VALUES (272,'aragua',37);
            INSERT INTO coincidencias VALUES (273,'has o haz',37);
            INSERT INTO coincidencias VALUES (274,'aro 17',37);
            INSERT INTO coincidencias VALUES (275,'cacahuates ',37);
            INSERT INTO coincidencias VALUES (276,'cacahuete.',37);
            INSERT INTO coincidencias VALUES (277,'caso guateque',37);
            INSERT INTO coincidencias VALUES (278,'caso white',37);

            INSERT INTO coincidencias VALUES (279,'papas',38);
            INSERT INTO coincidencias VALUES (280,'zapatos',38);
            INSERT INTO coincidencias VALUES (281,'capaz',38);
            INSERT INTO coincidencias VALUES (282,'papá',38);
            INSERT INTO coincidencias VALUES (283,'papa',38);
            INSERT INTO coincidencias VALUES (284,'capa',38);
            INSERT INTO coincidencias VALUES (285,'cap',38);
            INSERT INTO coincidencias VALUES (286,'cup',38);
            INSERT INTO coincidencias VALUES (287,'casp',38);

            INSERT INTO coincidencias VALUES (288,'chocolat',39);
            INSERT INTO coincidencias VALUES (289,'chocolates',39);
            INSERT INTO coincidencias VALUES (290,'chocolatero',39);
            INSERT INTO coincidencias VALUES (291,'yo karate',39);
            INSERT INTO coincidencias VALUES (292,'karate',39);

            INSERT INTO coincidencias VALUES (293,'ages',40);
            INSERT INTO coincidencias VALUES (294,'eje',40);
            INSERT INTO coincidencias VALUES (295,'eche',40);
            INSERT INTO coincidencias VALUES (296,'leche',40);
            INSERT INTO coincidencias VALUES (297,'leches',40);
            INSERT INTO coincidencias VALUES (298,'lecce',40);
            INSERT INTO coincidencias VALUES (299,'le eches',40);
            INSERT INTO coincidencias VALUES (300,'le eche.',40);
            
            
            INSERT INTO coincidencias VALUES (301,'calvito',41);
            INSERT INTO coincidencias VALUES (302,'caldito',41);
            INSERT INTO coincidencias VALUES (303,'caballito',41);
            INSERT INTO coincidencias VALUES (304,'cariñito',41);
            INSERT INTO coincidencias VALUES (305,'caballito',41);
            INSERT INTO coincidencias VALUES (306,'carrito',41);
            INSERT INTO coincidencias VALUES (307,'carito',41);
            INSERT INTO coincidencias VALUES (308,'calito',41);
            INSERT INTO coincidencias VALUES (309,'carritos',41);
            INSERT INTO coincidencias VALUES (310,'calitos',41);
            INSERT INTO coincidencias VALUES (311,'caritos',41);

            INSERT INTO coincidencias VALUES (312,'futas',42);
            INSERT INTO coincidencias VALUES (313,'fruta',42);
            INSERT INTO coincidencias VALUES (314,'frutales',42);
            INSERT INTO coincidencias VALUES (315,'frutal',42);
            INSERT INTO coincidencias VALUES (316,'fut',42);
            INSERT INTO coincidencias VALUES (317,'food',42);
            INSERT INTO coincidencias VALUES (318,'foot',42);
            INSERT INTO coincidencias VALUES (319,'futa',42);
            
            
            INSERT INTO coincidencias VALUES (320,'swatch',43);
            INSERT INTO coincidencias VALUES (321,'soacha',43);
            INSERT INTO coincidencias VALUES (322,'sweps',43);
            INSERT INTO coincidencias VALUES (323,'twiter',43);
            INSERT INTO coincidencias VALUES (324,'shutter',43);
            INSERT INTO coincidencias VALUES (325,'sueter',43);
            INSERT INTO coincidencias VALUES (326,'setter',43);
            
            INSERT INTO coincidencias VALUES (327,'para ella',44);
            INSERT INTO coincidencias VALUES (328,'playa ya',44);
            INSERT INTO coincidencias VALUES (329,'para ellas',44);
            INSERT INTO coincidencias VALUES (330,'era ella',44);
            INSERT INTO coincidencias VALUES (331,'playeras',44);
            INSERT INTO coincidencias VALUES (332,'ayer',44);
            INSERT INTO coincidencias VALUES (333,'jayena',44);


            INSERT INTO coincidencias VALUES (334,'saco',45);
            INSERT INTO coincidencias VALUES (335,'saca',45);
            INSERT INTO coincidencias VALUES (336,'chaco',45);
            INSERT INTO coincidencias VALUES (337,'sacos',45);
            INSERT INTO coincidencias VALUES (338,'sacó',45);
            INSERT INTO coincidencias VALUES (339,'sac',45);
            INSERT INTO coincidencias VALUES (340,'zac',45);
            INSERT INTO coincidencias VALUES (341,'suck',45);
            INSERT INTO coincidencias VALUES (342,'zack',45);
            INSERT INTO coincidencias VALUES (343,'zach',45);
            
            INSERT INTO coincidencias VALUES (344,'pasa que tal',46);
            INSERT INTO coincidencias VALUES (345,'qué tal',46);
            INSERT INTO coincidencias VALUES (346,'pasa qué tal',46);
            INSERT INTO coincidencias VALUES (347,'payasa qué tal',46);

            INSERT INTO coincidencias VALUES (348,'sepelio',47);
            INSERT INTO coincidencias VALUES (349,'cepillo',47);
            INSERT INTO coincidencias VALUES (350,'sepe y yo',47);
            INSERT INTO coincidencias VALUES (351,'cp y yo',47);
            INSERT INTO coincidencias VALUES (352,'sepei y yo',47);
            INSERT INTO coincidencias VALUES (353,'sátiro',47);
            INSERT INTO coincidencias VALUES (354,'tío',47);
            INSERT INTO coincidencias VALUES (355,'video',47);
            INSERT INTO coincidencias VALUES (356,'sabio',47);
            INSERT INTO coincidencias VALUES (357,'saltillo',47);
            INSERT INTO coincidencias VALUES (358,'qué tío',47);
            INSERT INTO coincidencias VALUES (359,'qué tía',47);

            INSERT INTO coincidencias VALUES (360,'lusa azul',48);
            INSERT INTO coincidencias VALUES (361,'blusa azul',48);
            INSERT INTO coincidencias VALUES (362,'usa azul',48);
            INSERT INTO coincidencias VALUES (363,'blusa a azul',48);
            INSERT INTO coincidencias VALUES (364,'tuzsa azul',48);
            INSERT INTO coincidencias VALUES (365,'blusa al sur',48);
            INSERT INTO coincidencias VALUES (366,'blusas al sur',48);
            INSERT INTO coincidencias VALUES (367,'blues al sur',48);


            INSERT INTO coincidencias VALUES (368,'calcetines',49);
            INSERT INTO coincidencias VALUES (369,'calcetin',49);
            INSERT INTO coincidencias VALUES (370,'casa',49);
            INSERT INTO coincidencias VALUES (371,'cassette',49);
            INSERT INTO coincidencias VALUES (372,'caso',49);

            INSERT INTO coincidencias VALUES (373,'zapatos',50);
            INSERT INTO coincidencias VALUES (374,'sap',50);
            INSERT INTO coincidencias VALUES (375,'sap para dos',50);
            INSERT INTO coincidencias VALUES (376,'zap tos',50);
            INSERT INTO coincidencias VALUES (377,'patos',50);

            INSERT INTO coincidencias VALUES (378,'transporte',51);
            INSERT INTO coincidencias VALUES (379,'last post',51);
            INSERT INTO coincidencias VALUES (380,'las potter',51);
            INSERT INTO coincidencias VALUES (381,'raz potter',51);
            INSERT INTO coincidencias VALUES (382,'rush poster',51);
            INSERT INTO coincidencias VALUES (383,'transporter',51);
            INSERT INTO coincidencias VALUES (384,'transport',51);
            INSERT INTO coincidencias VALUES (385,'transportes',51);
            
            INSERT INTO coincidencias VALUES (386,'chen',52);
            INSERT INTO coincidencias VALUES (387,'en',52);
            INSERT INTO coincidencias VALUES (388,'zen',52);
            INSERT INTO coincidencias VALUES (389,'ten',52);
            INSERT INTO coincidencias VALUES (390,'tren',52);
            INSERT INTO coincidencias VALUES (391,'change',52);
            INSERT INTO coincidencias VALUES (392,'saint',52);
            
            INSERT INTO coincidencias VALUES (393,'barc',53);
            INSERT INTO coincidencias VALUES (394,'barco',53);
            INSERT INTO coincidencias VALUES (395,'barcos',53);
            INSERT INTO coincidencias VALUES (396,'pasco',53);
            INSERT INTO coincidencias VALUES (397,'vasco',53);
            INSERT INTO coincidencias VALUES (398,'asco',53);
            INSERT INTO coincidencias VALUES (399,'park',53);
            INSERT INTO coincidencias VALUES (400,'arch',53);
            INSERT INTO coincidencias VALUES (401,'bar',53);
            INSERT INTO coincidencias VALUES (402,'bart',53);
            INSERT INTO coincidencias VALUES (403,'marca',53);
            INSERT INTO coincidencias VALUES (404,'var',53);

            INSERT INTO coincidencias VALUES (405,'autobuses',54);
            INSERT INTO coincidencias VALUES (406,'autobús',54);
            INSERT INTO coincidencias VALUES (407,'audio bus',54);
            INSERT INTO coincidencias VALUES (408,'uvus',54);
            INSERT INTO coincidencias VALUES (409,'obus',54);
            
            INSERT INTO coincidencias VALUES (410,'ion',55);
            INSERT INTO coincidencias VALUES (411,'ileon',55);
            INSERT INTO coincidencias VALUES (412,'iron',55);
            INSERT INTO coincidencias VALUES (413,'aion',55);
            INSERT INTO coincidencias VALUES (414,'león',55);
            INSERT INTO coincidencias VALUES (415,'avión',55);
            INSERT INTO coincidencias VALUES (416,'acción',55);

            INSERT INTO coincidencias VALUES (417,'destinos',56);
            INSERT INTO coincidencias VALUES (418,'destino',56);
            INSERT INTO coincidencias VALUES (419,'testino',56);
            INSERT INTO coincidencias VALUES (420,'tetina',56);
            INSERT INTO coincidencias VALUES (421,'esti no',56);

            INSERT INTO coincidencias VALUES (422,'um',57);
            INSERT INTO coincidencias VALUES (423,'aún',57);
            INSERT INTO coincidencias VALUES (424,'un',57);
            INSERT INTO coincidencias VALUES (425,'a un',57);
            INSERT INTO coincidencias VALUES (426,'cam',57);
            INSERT INTO coincidencias VALUES (427,'cancún',57);
            INSERT INTO coincidencias VALUES (428,'canción',57);
            INSERT INTO coincidencias VALUES (429,'canco',57);

            INSERT INTO coincidencias VALUES (430,'pueblo',58);
            INSERT INTO coincidencias VALUES (431,'puebla',58);
            INSERT INTO coincidencias VALUES (432,'tebla',58);
            INSERT INTO coincidencias VALUES (433,'pela',58);
            INSERT INTO coincidencias VALUES (434,'puela',58);

            INSERT INTO coincidencias VALUES (435,'monterrey',59);
            INSERT INTO coincidencias VALUES (436,'montaje del rey',59);
            INSERT INTO coincidencias VALUES (437,'moon r',59);
            INSERT INTO coincidencias VALUES (438,'obd',59);
            INSERT INTO coincidencias VALUES (439,'he',59);


            INSERT INTO coincidencias VALUES (440,'chapas',60);
            INSERT INTO coincidencias VALUES (441,'chat',60);
            INSERT INTO coincidencias VALUES (442,'xataka',60);
            INSERT INTO coincidencias VALUES (443,'chapa',60);
            INSERT INTO coincidencias VALUES (444,'chaval',60);
            INSERT INTO coincidencias VALUES (445,'app',60);
            INSERT INTO coincidencias VALUES (446,'up',60);
            INSERT INTO coincidencias VALUES (447,'y',60);
            INSERT INTO coincidencias VALUES (448,'y app',60);

            INSERT INTO coincidencias VALUES (449,'y ah oo',61);
            INSERT INTO coincidencias VALUES (450,'ia o',61);
            INSERT INTO coincidencias VALUES (451,'y ahoo',61);
            INSERT INTO coincidencias VALUES (452,'ia',61);
            INSERT INTO coincidencias VALUES (453,'y',61);
            INSERT INTO coincidencias VALUES (454,'sin olor',61);
            INSERT INTO coincidencias VALUES (455,'sinaloa',61);

            INSERT INTO coincidencias VALUES (456,'habbo',62);
            INSERT INTO coincidencias VALUES (457,'pago',62);
            INSERT INTO coincidencias VALUES (458,'jabón',62);
            INSERT INTO coincidencias VALUES (459,'pagos',62);
            INSERT INTO coincidencias VALUES (460,'pagó',62);
            INSERT INTO coincidencias VALUES (461,'java',62);
            INSERT INTO coincidencias VALUES (462,'javo',62);
            INSERT INTO coincidencias VALUES (463,'pavo',62);
            
            
            INSERT INTO coincidencias VALUES (464,'check',63);
            INSERT INTO coincidencias VALUES (465,'chat',63);
            INSERT INTO coincidencias VALUES (466,'chep',63);
            INSERT INTO coincidencias VALUES (467,'chucky',63);
            INSERT INTO coincidencias VALUES (468,'chuck',63);
            INSERT INTO coincidencias VALUES (469,'ets',63);
            INSERT INTO coincidencias VALUES (470,'x',63);
            INSERT INTO coincidencias VALUES (471,'ex',63);
            INSERT INTO coincidencias VALUES (472,'cheche',63);
            INSERT INTO coincidencias VALUES (473,'cheste',63);
            INSERT INTO coincidencias VALUES (474,'chester',63);
            INSERT INTO coincidencias VALUES (475,'che che che.',63);

            INSERT INTO coincidencias VALUES (476,'tarjeta',64);
            INSERT INTO coincidencias VALUES (477,'tarjetas',64);
            INSERT INTO coincidencias VALUES (478,'aeat',64);
            INSERT INTO coincidencias VALUES (479,'aemet',64);
            INSERT INTO coincidencias VALUES (480,'aea',64);
            INSERT INTO coincidencias VALUES (481,'aeam',64);
            INSERT INTO coincidencias VALUES (482,'asset',64);
            INSERT INTO coincidencias VALUES (483,'algete',64);
            INSERT INTO coincidencias VALUES (484,'azeta',64);
            INSERT INTO coincidencias VALUES (485,'asetra',64);
            INSERT INTO coincidencias VALUES (486,'acepta',64);

            INSERT INTO coincidencias VALUES (487,'es festivo',65);
            INSERT INTO coincidencias VALUES (488,'efectivo',65);
            INSERT INTO coincidencias VALUES (489,'s tío',65);
            INSERT INTO coincidencias VALUES (490,'ese tío',65);
            INSERT INTO coincidencias VALUES (491,'es el hijo',65);

            INSERT INTO coincidencias VALUES (492,'papá',66);
            INSERT INTO coincidencias VALUES (493,'caca',66);
            INSERT INTO coincidencias VALUES (494,'cap',66);
            INSERT INTO coincidencias VALUES (495,'pasta',66);
            INSERT INTO coincidencias VALUES (496,'pack',66);
            
            INSERT INTO coincidencias VALUES (497,'sol',71);
            INSERT INTO coincidencias VALUES (498,'scholl',71);
            INSERT INTO coincidencias VALUES (499,'son',71);
            INSERT INTO coincidencias VALUES (500,'hall',71);
            INSERT INTO coincidencias VALUES (501,'song',71);
            INSERT INTO coincidencias VALUES (502,'o',71);
            INSERT INTO coincidencias VALUES (503,'ol',71);
            INSERT INTO coincidencias VALUES (504,'hola',71);
            INSERT INTO coincidencias VALUES (505,'old',71);

            INSERT INTO coincidencias VALUES (506,'faldas',72);
            INSERT INTO coincidencias VALUES (507,'adidas',72);
            INSERT INTO coincidencias VALUES (508,'alda',72);
            INSERT INTO coincidencias VALUES (509,'adela',72);
            INSERT INTO coincidencias VALUES (510,'alzira',72);
            INSERT INTO coincidencias VALUES (511,'anda',72);
            INSERT INTO coincidencias VALUES (512,'ata',72);
            INSERT INTO coincidencias VALUES (513,'at',72);
            INSERT INTO coincidencias VALUES (514,'aeat',72);
            INSERT INTO coincidencias VALUES (515,'app',72);
            INSERT INTO coincidencias VALUES (516,'add',72);
            INSERT INTO coincidencias VALUES (517,'alta',72);
            INSERT INTO coincidencias VALUES (518,'alda',72);
            INSERT INTO coincidencias VALUES (519,'alba',72);
            
            INSERT INTO coincidencias VALUES (520,'soc',73);
            INSERT INTO coincidencias VALUES (521,'sac',73);
            INSERT INTO coincidencias VALUES (522,'suck',73);
            INSERT INTO coincidencias VALUES (523,'zach',73);

            INSERT INTO coincidencias VALUES (524,'hot',74);
            INSERT INTO coincidencias VALUES (525,'hope',74);
            INSERT INTO coincidencias VALUES (526,'hop',74);
            INSERT INTO coincidencias VALUES (527,'ok',74);
            INSERT INTO coincidencias VALUES (528,'yoc',74);
            INSERT INTO coincidencias VALUES (529,'fox',74);
            INSERT INTO coincidencias VALUES (530,'soc',74);
            INSERT INTO coincidencias VALUES (531,'foca',74);
            INSERT INTO coincidencias VALUES (532,'focs',74);
            INSERT INTO coincidencias VALUES (533,'fac',74);
            INSERT INTO coincidencias VALUES (534,'fosca',74);
            INSERT INTO coincidencias VALUES (535,'ford ka',74);

            INSERT INTO coincidencias VALUES (536,'frutas',75);
            INSERT INTO coincidencias VALUES (537,'fruit',75);
            INSERT INTO coincidencias VALUES (538,'fruta',75);
            INSERT INTO coincidencias VALUES (539,'fut',75);
            INSERT INTO coincidencias VALUES (540,'food',75);
            INSERT INTO coincidencias VALUES (541,'foot',75);
            INSERT INTO coincidencias VALUES (542,'suit',75);
            INSERT INTO coincidencias VALUES (543,'foot',75);
            
            INSERT INTO coincidencias VALUES (544,'sirenas',76);
            INSERT INTO coincidencias VALUES (545,'sirena',76);
            INSERT INTO coincidencias VALUES (546,'serena',76);
            INSERT INTO coincidencias VALUES (547,'elena',76);
            INSERT INTO coincidencias VALUES (548,'gerena',76);
            INSERT INTO coincidencias VALUES (549,'heorina',76);
            INSERT INTO coincidencias VALUES (550,'helena',76);
            INSERT INTO coincidencias VALUES (551,'ere na',76);
            INSERT INTO coincidencias VALUES (552,'sirens',76);

            INSERT INTO coincidencias VALUES (553,'filet',77);
            INSERT INTO coincidencias VALUES (554,'filete',77);
            INSERT INTO coincidencias VALUES (555,'filetes',77);
            INSERT INTO coincidencias VALUES (556,'gilet',77);
            INSERT INTO coincidencias VALUES (557,'gillette',77);
            INSERT INTO coincidencias VALUES (558,'ie',77);
            INSERT INTO coincidencias VALUES (559,'ieee',77);


            INSERT INTO coincidencias VALUES (560,'salchichas',78);
            INSERT INTO coincidencias VALUES (561,'salchichón',78);
            INSERT INTO coincidencias VALUES (562,'salchicha',78);
            INSERT INTO coincidencias VALUES (563,'hay chicha',78);
            INSERT INTO coincidencias VALUES (564,'al chicho',78);
            INSERT INTO coincidencias VALUES (565,'al chico',78);
            INSERT INTO coincidencias VALUES (566,'al chicharo',78);


            INSERT INTO coincidencias VALUES (567,'sopa',79);
            INSERT INTO coincidencias VALUES (568,'opa',79);
            INSERT INTO coincidencias VALUES (569,'op',79);
            INSERT INTO coincidencias VALUES (570,'pop',79);
            INSERT INTO coincidencias VALUES (571,'popa',79);
            INSERT INTO coincidencias VALUES (572,'hop',79);
            INSERT INTO coincidencias VALUES (573,'opw',79);
            INSERT INTO coincidencias VALUES (574,'chop',79);
            INSERT INTO coincidencias VALUES (575,'ropa',79);
            INSERT INTO coincidencias VALUES (576,'sop',79);
            INSERT INTO coincidencias VALUES (577,'shop',79);
            
            INSERT INTO coincidencias VALUES (578,'familia',80);
            INSERT INTO coincidencias VALUES (579,'amilia',80);
            INSERT INTO coincidencias VALUES (580,'amelia',80);
            INSERT INTO coincidencias VALUES (581,'a million',80);
            INSERT INTO coincidencias VALUES (582,'jamilena',80);
            INSERT INTO coincidencias VALUES (583,'chamilia',80);
            
            INSERT INTO coincidencias VALUES (585,'china',84);
            INSERT INTO coincidencias VALUES (586,'tina',84);
            INSERT INTO coincidencias VALUES (587,'cine',84);
            INSERT INTO coincidencias VALUES (588,'trina',84);
            INSERT INTO coincidencias VALUES (589,'fina',84);
            INSERT INTO coincidencias VALUES (590,'inns',84);
            INSERT INTO coincidencias VALUES (591,'inem',84);
            INSERT INTO coincidencias VALUES (592,'ina',84);
            INSERT INTO coincidencias VALUES (593,'ine',84);
            INSERT INTO coincidencias VALUES (594,'inap',84);
            
            INSERT INTO coincidencias VALUES (595,'talla',85);
            INSERT INTO coincidencias VALUES (596,'kayak',85);
            INSERT INTO coincidencias VALUES (597,'para allá',85);
            INSERT INTO coincidencias VALUES (598,'toalla',85);
            INSERT INTO coincidencias VALUES (599,'calla',85);
            INSERT INTO coincidencias VALUES (600,'falla',85);
            INSERT INTO coincidencias VALUES (601,'gaia',85);
            INSERT INTO coincidencias VALUES (602,'palla',85);
            INSERT INTO coincidencias VALUES (603,'jaiak',85);
            INSERT INTO coincidencias VALUES (604,'hyatt',85);
            INSERT INTO coincidencias VALUES (605,'allá',85);
            INSERT INTO coincidencias VALUES (606,'hi ha',85);


            INSERT INTO coincidencias VALUES (607,'jabón',86);
            INSERT INTO coincidencias VALUES (608,'avón',86);
            INSERT INTO coincidencias VALUES (609,'japón',86);
            INSERT INTO coincidencias VALUES (610,'upon',86);
            INSERT INTO coincidencias VALUES (611,'up on',86);
            INSERT INTO coincidencias VALUES (612,'app on',86);

            INSERT INTO coincidencias VALUES (613,'cortina',87);
            INSERT INTO coincidencias VALUES (614,'cortinas',87);
            INSERT INTO coincidencias VALUES (615,'hot inna',87);
            INSERT INTO coincidencias VALUES (616,'inna',87);
            INSERT INTO coincidencias VALUES (617,'inem',87);
            INSERT INTO coincidencias VALUES (618,'irina',87);
            INSERT INTO coincidencias VALUES (619,'in a',87);
            INSERT INTO coincidencias VALUES (620,'opina',87);
            INSERT INTO coincidencias VALUES (621,'okina',87);
            INSERT INTO coincidencias VALUES (622,'orina',87);
            INSERT INTO coincidencias VALUES (623,'tina',87);
            INSERT INTO coincidencias VALUES (624,'chinas',87);

            INSERT INTO coincidencias VALUES (625,'champu',88);
            INSERT INTO coincidencias VALUES (626,'tampu',88);
            INSERT INTO coincidencias VALUES (627,'ampu',88);
            INSERT INTO coincidencias VALUES (628,'ampans',88);
            INSERT INTO coincidencias VALUES (629,'ampo',88);
            INSERT INTO coincidencias VALUES (630,'campu',88);
            INSERT INTO coincidencias VALUES (631,'campus',88);
            
            INSERT INTO coincidencias VALUES (632,'piece',89);
            INSERT INTO coincidencias VALUES (633,'piso',89);
            INSERT INTO coincidencias VALUES (634,'pics',89);
            INSERT INTO coincidencias VALUES (635,'pis',89);
            INSERT INTO coincidencias VALUES (636,'pisos',89);
            INSERT INTO coincidencias VALUES (637,'ipod',89);
            INSERT INTO coincidencias VALUES (638,'ikea',89);
            INSERT INTO coincidencias VALUES (639,'icloud',89);
            INSERT INTO coincidencias VALUES (640,'icot',89);
            INSERT INTO coincidencias VALUES (641,'ipod',89);
            
            INSERT INTO coincidencias VALUES (642,'espejos',90);
            INSERT INTO coincidencias VALUES (643,'espejo',90);
            INSERT INTO coincidencias VALUES (644,'gipe',90);
            INSERT INTO coincidencias VALUES (645,'ep',90);
            INSERT INTO coincidencias VALUES (646,'pepe',90);
                        
            INSERT INTO coincidencias VALUES (647,'festa',91);
            INSERT INTO coincidencias VALUES (648,'ya está',91);
            INSERT INTO coincidencias VALUES (649,'cuesta',91);
            INSERT INTO coincidencias VALUES (650,'perta',91);
            INSERT INTO coincidencias VALUES (651,'puerta',91);
            INSERT INTO coincidencias VALUES (652,'puesta',91);

            INSERT INTO coincidencias VALUES (653,'happy',92);
            INSERT INTO coincidencias VALUES (654,'papi',92);
            INSERT INTO coincidencias VALUES (655,'popi',92);
            INSERT INTO coincidencias VALUES (656,'poni',92);
            INSERT INTO coincidencias VALUES (657,'pop',92);
            INSERT INTO coincidencias VALUES (658,'opi',92);
            INSERT INTO coincidencias VALUES (659,'api',92);
            INSERT INTO coincidencias VALUES (660,'off',92);
            INSERT INTO coincidencias VALUES (661,'oye',92);
            INSERT INTO coincidencias VALUES (662,'y',92);
            INSERT INTO coincidencias VALUES (663,'ir',92);

            INSERT INTO coincidencias VALUES (664,'bolso',93);
            INSERT INTO coincidencias VALUES (665,'sexo',93);
            INSERT INTO coincidencias VALUES (666,'salsa',93);
            INSERT INTO coincidencias VALUES (667,'bolsa',93);
            INSERT INTO coincidencias VALUES (668,'8',93);
            INSERT INTO coincidencias VALUES (669,'whatsapp',93);
            INSERT INTO coincidencias VALUES (670,'hueso',93);
            INSERT INTO coincidencias VALUES (671,'webcam',93);
            INSERT INTO coincidencias VALUES (672,'watson',93);
            INSERT INTO coincidencias VALUES (673,'west',93);
            INSERT INTO coincidencias VALUES (674,'quest',93);
            INSERT INTO coincidencias VALUES (675,'pues sí',93);
            INSERT INTO coincidencias VALUES (676,'pues',93);
            
            INSERT INTO coincidencias VALUES (677,'tapete',94);
            INSERT INTO coincidencias VALUES (678,'app',94);
            INSERT INTO coincidencias VALUES (679,'chapete',94);
            INSERT INTO coincidencias VALUES (680,'te apeteces',94);
            INSERT INTO coincidencias VALUES (681,'tapetes',94);
            INSERT INTO coincidencias VALUES (682,'tapestry',94);
            INSERT INTO coincidencias VALUES (683,'te apetece',94);
            INSERT INTO coincidencias VALUES (684,'a pepe',94);
            INSERT INTO coincidencias VALUES (685,'app app',94);

            INSERT INTO coincidencias VALUES (686,'mesa',95);
            INSERT INTO coincidencias VALUES (687,'me sa',95);
            INSERT INTO coincidencias VALUES (688,'mesas',95);
            INSERT INTO coincidencias VALUES (689,'mena',95);
            INSERT INTO coincidencias VALUES (690,'mensa',95);
            INSERT INTO coincidencias VALUES (691,'mensaje',95);
            
            INSERT INTO coincidencias VALUES (692,'pelotas',96);
            INSERT INTO coincidencias VALUES (693,'pelota',96);
            INSERT INTO coincidencias VALUES (694,'clot',96);
            INSERT INTO coincidencias VALUES (695,'claude',96);
            INSERT INTO coincidencias VALUES (696,'slot',96);
            INSERT INTO coincidencias VALUES (697,'lot',96);
            INSERT INTO coincidencias VALUES (698,'close',96);
            INSERT INTO coincidencias VALUES (699,'pelot',96);
            INSERT INTO coincidencias VALUES (700,'pelo',96);
            INSERT INTO coincidencias VALUES (701,'otan',96);
            INSERT INTO coincidencias VALUES (702,'hota',96);
            INSERT INTO coincidencias VALUES (703,'hotel',96);


            INSERT INTO coincidencias VALUES (704,'disco',97);
            INSERT INTO coincidencias VALUES (705,'beeg',97);
            INSERT INTO coincidencias VALUES (706,'disc',97);
            INSERT INTO coincidencias VALUES (707,'disk',97);
            INSERT INTO coincidencias VALUES (708,'dicop',97);
            INSERT INTO coincidencias VALUES (709,'big cock',97);
            INSERT INTO coincidencias VALUES (710,'big bobos',97);
            
            INSERT INTO coincidencias VALUES (711,'fuerza',98);
            INSERT INTO coincidencias VALUES (712,'puertas',98);
            INSERT INTO coincidencias VALUES (713,'puerta',98);
            INSERT INTO coincidencias VALUES (714,'fuerzas',98);
            INSERT INTO coincidencias VALUES (715,'juerga',98);
            INSERT INTO coincidencias VALUES (716,'cuerdas',98);
            INSERT INTO coincidencias VALUES (717,'corda',98);
            INSERT INTO coincidencias VALUES (718,'acuerda',98);
            INSERT INTO coincidencias VALUES (719,'huelga',98);
                        
            INSERT INTO coincidencias VALUES (720,'burbuja',99);
            INSERT INTO coincidencias VALUES (721,'upap',99);
            INSERT INTO coincidencias VALUES (722,'huc whatsapp',99);
            INSERT INTO coincidencias VALUES (723,'uefa',99);
            INSERT INTO coincidencias VALUES (724,'usal',99);
            INSERT INTO coincidencias VALUES (725,'usa',99);
            INSERT INTO coincidencias VALUES (726,'uja',99);
            INSERT INTO coincidencias VALUES (727,'husa',99);
            INSERT INTO coincidencias VALUES (728,'usar',99);
            INSERT INTO coincidencias VALUES (729,'boo',99);
            INSERT INTO coincidencias VALUES (730,'bu',99);
            INSERT INTO coincidencias VALUES (731,'wu',99);

            INSERT INTO coincidencias VALUES (732,'aieta',100);
            INSERT INTO coincidencias VALUES (733,'bayeta',100);
            INSERT INTO coincidencias VALUES (734,'galleta',100);
            INSERT INTO coincidencias VALUES (735,'galletas',100);
            INSERT INTO coincidencias VALUES (736,'alleta',100);
            INSERT INTO coincidencias VALUES (737,'gallet',100);
            INSERT INTO coincidencias VALUES (738,'gaieta',100);
            INSERT INTO coincidencias VALUES (739,'calle está',100);
            INSERT INTO coincidencias VALUES (740,'calle ta.',100);
            
            INSERT INTO coincidencias VALUES (741,'coqueta',101);
            INSERT INTO coincidencias VALUES (742,'koketas',101);
            INSERT INTO coincidencias VALUES (743,'coquetas',101);
            INSERT INTO coincidencias VALUES (744,'coquette',101);
            INSERT INTO coincidencias VALUES (745,'coquetear',101);
            INSERT INTO coincidencias VALUES (746,'coquette',101);
            INSERT INTO coincidencias VALUES (747,'croquetas',101);
            INSERT INTO coincidencias VALUES (748,'socket',101);
            INSERT INTO coincidencias VALUES (749,'soqueta',101);
            INSERT INTO coincidencias VALUES (750,'chaqueta.',101);

            INSERT INTO coincidencias VALUES (751,'salchicha',102);
            INSERT INTO coincidencias VALUES (752,'salchichas',102);
            INSERT INTO coincidencias VALUES (753,'salchichón',102);
            
            
            INSERT INTO coincidencias VALUES (754,'carmen',103);
            INSERT INTO coincidencias VALUES (755,'carne',103);
            INSERT INTO coincidencias VALUES (756,'carnet',103);
            INSERT INTO coincidencias VALUES (757,'carme',103);
            INSERT INTO coincidencias VALUES (758,'carbone',103);

            INSERT INTO coincidencias VALUES (759,'sport',104);
            INSERT INTO coincidencias VALUES (760,'esponja',104);
            INSERT INTO coincidencias VALUES (761,'spot',104);
            INSERT INTO coincidencias VALUES (762,'esponjas',104);
            INSERT INTO coincidencias VALUES (763,'sponja',104);
            INSERT INTO coincidencias VALUES (764,'spa',104);
            
            INSERT INTO coincidencias VALUES (765,'coelho',105);
            INSERT INTO coincidencias VALUES (766,'cobeño',105);
            INSERT INTO coincidencias VALUES (767,'cobeña',105);
            INSERT INTO coincidencias VALUES (768,'con ello',105);
            INSERT INTO coincidencias VALUES (769,'ello',105);
            INSERT INTO coincidencias VALUES (770,'ellos',105);
            INSERT INTO coincidencias VALUES (771,'ella',105);
            INSERT INTO coincidencias VALUES (772,'a ella',105);
            INSERT INTO coincidencias VALUES (773,'con ellos',105);
            INSERT INTO coincidencias VALUES (774,'jue yo',105);
            INSERT INTO coincidencias VALUES (775,'cubelles',105);
            INSERT INTO coincidencias VALUES (776,'codillo',105);
            INSERT INTO coincidencias VALUES (777,'colegio',105);
            INSERT INTO coincidencias VALUES (778,'cuellos',105);
            INSERT INTO coincidencias VALUES (779,'uellos',105);
            INSERT INTO coincidencias VALUES (780,'uello.',105);
            

            INSERT INTO coincidencias VALUES (781,'abc',106);
            INSERT INTO coincidencias VALUES (782,'cabeza',106);
            INSERT INTO coincidencias VALUES (783,'chávez',106);
            INSERT INTO coincidencias VALUES (784,'sabes',106);
            INSERT INTO coincidencias VALUES (785,'claves',106);
            INSERT INTO coincidencias VALUES (786,'cabezón',106);
            INSERT INTO coincidencias VALUES (787,'cabezal',106);
            INSERT INTO coincidencias VALUES (788,'cada vez',106);
            INSERT INTO coincidencias VALUES (789,'cabe',106);
            INSERT INTO coincidencias VALUES (790,'cabezal',106);
            INSERT INTO coincidencias VALUES (791,'aves',106);
            INSERT INTO coincidencias VALUES (792,'aveces',106);
            
            
            INSERT INTO coincidencias VALUES (793,'hola',107);
            INSERT INTO coincidencias VALUES (794,'cola',107);
            INSERT INTO coincidencias VALUES (795,'collar',107);
            INSERT INTO coincidencias VALUES (796,'colega',107);
            INSERT INTO coincidencias VALUES (797,'collage',107);
            INSERT INTO coincidencias VALUES (798,'hola hola',107);


            INSERT INTO coincidencias VALUES (799,'panchita',108);
            INSERT INTO coincidencias VALUES (800,'pancita',108);
            INSERT INTO coincidencias VALUES (801,'panchito',108);
            INSERT INTO coincidencias VALUES (802,'transit',108);
            INSERT INTO coincidencias VALUES (803,'panceta',108);
            INSERT INTO coincidencias VALUES (804,'pan cita',108);
            INSERT INTO coincidencias VALUES (805,'can cita',108);
            INSERT INTO coincidencias VALUES (806,'ikea',108);
            INSERT INTO coincidencias VALUES (807,'y',108);
            INSERT INTO coincidencias VALUES (808,'aemet',108);
            INSERT INTO coincidencias VALUES (809,'icc',108);

            INSERT INTO coincidencias VALUES (810,'pap',109);
            INSERT INTO coincidencias VALUES (811,'papa',109);
            INSERT INTO coincidencias VALUES (812,'cap',109);
            INSERT INTO coincidencias VALUES (813,'pata',109);
            INSERT INTO coincidencias VALUES (814,'pasta',109);
            INSERT INTO coincidencias VALUES (815,'patas',109);
            INSERT INTO coincidencias VALUES (816,'patata',109);
            INSERT INTO coincidencias VALUES (817,'cata',109);
            INSERT INTO coincidencias VALUES (818,'tata',109);
            
            INSERT INTO coincidencias VALUES (819,'ancha',110);
            INSERT INTO coincidencias VALUES (820,'mancha',110);
            INSERT INTO coincidencias VALUES (821,'manchas',110);
            INSERT INTO coincidencias VALUES (822,'manchar',110);
            INSERT INTO coincidencias VALUES (823,'pancha',110);
            INSERT INTO coincidencias VALUES (824,'panchas',110);

            INSERT INTO coincidencias VALUES (825,'bbva',111);
            INSERT INTO coincidencias VALUES (826,'cueva',111);
            INSERT INTO coincidencias VALUES (827,'nueva',111);
            INSERT INTO coincidencias VALUES (828,'juega',111);
            INSERT INTO coincidencias VALUES (829,'eva',111);
            INSERT INTO coincidencias VALUES (830,'prueba',111);
            
            INSERT INTO coincidencias VALUES (831,'top',112);
            INSERT INTO coincidencias VALUES (832,'tops',112);
            INSERT INTO coincidencias VALUES (833,'toc',112);
            INSERT INTO coincidencias VALUES (834,'loca',112);
            INSERT INTO coincidencias VALUES (835,'loc',112);
            INSERT INTO coincidencias VALUES (836,'local',112);
            INSERT INTO coincidencias VALUES (837,'locas',112);
            INSERT INTO coincidencias VALUES (838,'los k',112);
            INSERT INTO coincidencias VALUES (839,'rock',112);
            INSERT INTO coincidencias VALUES (840,'ropa',112);
            INSERT INTO coincidencias VALUES (841,'procaz',112);
            INSERT INTO coincidencias VALUES (842,'topas',112);

            INSERT INTO coincidencias VALUES (843,'chera',113);
            INSERT INTO coincidencias VALUES (844,'terra',113);
            INSERT INTO coincidencias VALUES (845,'tera',113);
            INSERT INTO coincidencias VALUES (846,'cher terrasa',113);
            INSERT INTO coincidencias VALUES (847,'tierra',113);
            INSERT INTO coincidencias VALUES (848,'yedra',113);
            INSERT INTO coincidencias VALUES (849,'piedra',113);
            INSERT INTO coincidencias VALUES (850,'hiedra',113);
            INSERT INTO coincidencias VALUES (851,'pietra',113);

            INSERT INTO coincidencias VALUES (852,'volvo',114);
            INSERT INTO coincidencias VALUES (853,'polvo',114);
            INSERT INTO coincidencias VALUES (854,'polvos',114);
            INSERT INTO coincidencias VALUES (855,'pólipo',114);
            INSERT INTO coincidencias VALUES (856,'olivo',114);
            INSERT INTO coincidencias VALUES (857,'ovo',114);
            INSERT INTO coincidencias VALUES (858,'obo',114);
            INSERT INTO coincidencias VALUES (859,'oboe',114);
            INSERT INTO coincidencias VALUES (860,'obos',114);
            INSERT INTO coincidencias VALUES (861,'bo bo',114);

            INSERT INTO coincidencias VALUES (862,'0',115);
            INSERT INTO coincidencias VALUES (863,'cerro',115);
            INSERT INTO coincidencias VALUES (864,'cero',115);
            INSERT INTO coincidencias VALUES (865,'cerró',115);
            INSERT INTO coincidencias VALUES (866,'jero',115);
            INSERT INTO coincidencias VALUES (867,'texto',115);
            INSERT INTO coincidencias VALUES (868,'testo',115);
            INSERT INTO coincidencias VALUES (869,'te quiero',115);
            INSERT INTO coincidencias VALUES (870,'esto',115);

            INSERT INTO coincidencias VALUES (871,'it a audio',116);
            INSERT INTO coincidencias VALUES (872,'its a audio',116);
            INSERT INTO coincidencias VALUES (873,'dinosauiro',116);
            INSERT INTO coincidencias VALUES (874,'titanosaurio',116);
            INSERT INTO coincidencias VALUES (875,'silanos audio',116);
            INSERT INTO coincidencias VALUES (876,'síganos audio',116);
            INSERT INTO coincidencias VALUES (877,'siganos audio',116);
            INSERT INTO coincidencias VALUES (878,'titanos audio',116);

            
            INSERT INTO coincidencias VALUES (879,'montaña',117);
            INSERT INTO coincidencias VALUES (880,'montaño',117);
            INSERT INTO coincidencias VALUES (881,'montania',117);
            INSERT INTO coincidencias VALUES (882,'montagna',117);
            INSERT INTO coincidencias VALUES (883,'años',117);
            INSERT INTO coincidencias VALUES (884,'año',117);
            INSERT INTO coincidencias VALUES (885,'añas',117);
            INSERT INTO coincidencias VALUES (886,'azaña',117);
            INSERT INTO coincidencias VALUES (887,'aña',117);

            INSERT INTO coincidencias VALUES (888,'al gusto',118);
            INSERT INTO coincidencias VALUES (889,'al busto',118);
            INSERT INTO coincidencias VALUES (890,'al bulto',118);
            INSERT INTO coincidencias VALUES (891,'arbusto',118);
            INSERT INTO coincidencias VALUES (892,'arbustos',118);

            INSERT INTO coincidencias VALUES (893,'pasta',119);
            INSERT INTO coincidencias VALUES (894,'past',119);
            INSERT INTO coincidencias VALUES (895,'as',119);
            INSERT INTO coincidencias VALUES (896,'cast',119);
            INSERT INTO coincidencias VALUES (897,'tast',119);
            INSERT INTO coincidencias VALUES (898,'pasto',119);
            INSERT INTO coincidencias VALUES (899,'estás',119);

            INSERT INTO coincidencias VALUES (900,'come caca',120);
            INSERT INTO coincidencias VALUES (901,'come carne',120);
            INSERT INTO coincidencias VALUES (902,'tome cano',120);
            INSERT INTO coincidencias VALUES (903,'como spam',120);
            INSERT INTO coincidencias VALUES (904,'comer carne',120);
            INSERT INTO coincidencias VALUES (905,'comic sans',120);
            INSERT INTO coincidencias VALUES (906,'comic año',120);
            INSERT INTO coincidencias VALUES (907,'comic años',120);
            INSERT INTO coincidencias VALUES (908,'comic antes. andy',120);
            INSERT INTO coincidencias VALUES (909,'andy',120);
            INSERT INTO coincidencias VALUES (910,'amén amén',120);
            INSERT INTO coincidencias VALUES (911,'en milán',120);
            INSERT INTO coincidencias VALUES (912,'dónde anda',120);

            INSERT INTO coincidencias VALUES (913,'com sandia',121);
            INSERT INTO coincidencias VALUES (914,'come sandia',121);
            INSERT INTO coincidencias VALUES (915,'camí sandia',121);
            INSERT INTO coincidencias VALUES (916,'carmen sandia',121);
            INSERT INTO coincidencias VALUES (917,'camisa en día',121);
            INSERT INTO coincidencias VALUES (918,'omnia',121);
            INSERT INTO coincidencias VALUES (919,'omenaldia',121);
            INSERT INTO coincidencias VALUES (920,'omnia',121);
            INSERT INTO coincidencias VALUES (921,'omnis',121);
            INSERT INTO coincidencias VALUES (922,'omni a',121);
            INSERT INTO coincidencias VALUES (923,'home al día',121);

            INSERT INTO coincidencias VALUES (924,'come manzanas',122);
            INSERT INTO coincidencias VALUES (925,'hotmail manzana',122);
            INSERT INTO coincidencias VALUES (926,'home manzana',122);
            INSERT INTO coincidencias VALUES (927,'om manzana',122);
            INSERT INTO coincidencias VALUES (928,'home mantuano',122);
            INSERT INTO coincidencias VALUES (929,'manzana',122);
            INSERT INTO coincidencias VALUES (930,'ome manzana',122);

            INSERT INTO coincidencias VALUES (931,'home tuna',123);
            INSERT INTO coincidencias VALUES (932,'home tonight',123);
            INSERT INTO coincidencias VALUES (933,'hotm tuna',123);
            INSERT INTO coincidencias VALUES (934,'come tona',123);
            INSERT INTO coincidencias VALUES (935,'hamilton',123);
            INSERT INTO coincidencias VALUES (936,'carmen tona',123);
            INSERT INTO coincidencias VALUES (937,'jaime tona',123);
            INSERT INTO coincidencias VALUES (938,'comete una',123);
            INSERT INTO coincidencias VALUES (939,'carnet una',123);
            INSERT INTO coincidencias VALUES (940,'carme tuna',123);
            INSERT INTO coincidencias VALUES (941,'carne tuna',123);

            INSERT INTO coincidencias VALUES (942,'chat',124);
            INSERT INTO coincidencias VALUES (943,'rata',124);
            INSERT INTO coincidencias VALUES (944,'tata',124);
            INSERT INTO coincidencias VALUES (945,'chata',124);
            INSERT INTO coincidencias VALUES (946,'ya está',124);
            INSERT INTO coincidencias VALUES (947,'rap',124);
            INSERT INTO coincidencias VALUES (948,'seat',124);
            INSERT INTO coincidencias VALUES (949,'plato',124);
            INSERT INTO coincidencias VALUES (950,'sap',124);
            INSERT INTO coincidencias VALUES (951,'pat',124);
            INSERT INTO coincidencias VALUES (952,'path',124);
            INSERT INTO coincidencias VALUES (953,'pad',124);
            INSERT INTO coincidencias VALUES (954,'pato',124);
            INSERT INTO coincidencias VALUES (955,'cat',124);


            INSERT INTO coincidencias VALUES (956,'mochila',125);
            INSERT INTO coincidencias VALUES (957,'mochilas',125);
            INSERT INTO coincidencias VALUES (958,'mozilla',125);
            INSERT INTO coincidencias VALUES (959,'hotmail',125);
            INSERT INTO coincidencias VALUES (960,'motxilla',125);
            INSERT INTO coincidencias VALUES (961,'isla',125);
            INSERT INTO coincidencias VALUES (962,'llia',125);
            INSERT INTO coincidencias VALUES (963,'il ya',125);
            
            
            INSERT INTO coincidencias VALUES (964,'pietra',126);
            INSERT INTO coincidencias VALUES (965,'piedra',126);
            INSERT INTO coincidencias VALUES (966,'piedras',126);
            INSERT INTO coincidencias VALUES (967,'yedra',126);
            INSERT INTO coincidencias VALUES (968,'pedro',126);
            INSERT INTO coincidencias VALUES (969,'hiedra',126);
            INSERT INTO coincidencias VALUES (970,'sidra',126);
            
            INSERT INTO coincidencias VALUES (971,'amina',127);
            INSERT INTO coincidencias VALUES (972,'camino',127);
            INSERT INTO coincidencias VALUES (973,'caminos',127);
            INSERT INTO coincidencias VALUES (974,'camina',127);
            INSERT INTO coincidencias VALUES (975,'a mí no',127);
            INSERT INTO coincidencias VALUES (976,'ikea',127);
            INSERT INTO coincidencias VALUES (977,'himno',127);
            INSERT INTO coincidencias VALUES (978,'icon',127);
            INSERT INTO coincidencias VALUES (979,'icono',127);
            INSERT INTO coincidencias VALUES (980,'y no',127);

            INSERT INTO coincidencias VALUES (981,'cajamar',128);
            INSERT INTO coincidencias VALUES (982,'cama',128);
            INSERT INTO coincidencias VALUES (983,'camas',128);
            INSERT INTO coincidencias VALUES (984,'hama',128);
            INSERT INTO coincidencias VALUES (985,'ketama',128);
            INSERT INTO coincidencias VALUES (986,'hama',128);
            INSERT INTO coincidencias VALUES (987,'jama',128);
            INSERT INTO coincidencias VALUES (988,'jarama',128);
            INSERT INTO coincidencias VALUES (989,'ama',128);

            INSERT INTO coincidencias VALUES (990,'colcha',129);
            INSERT INTO coincidencias VALUES (991,'colt',129);
            INSERT INTO coincidencias VALUES (992,'cold',129);
            INSERT INTO coincidencias VALUES (993,'colchas',129);
            INSERT INTO coincidencias VALUES (994,'colchón',129);
            INSERT INTO coincidencias VALUES (995,'hot',129);
            INSERT INTO coincidencias VALUES (996,'ott',129);
            INSERT INTO coincidencias VALUES (997,'8',129);
            INSERT INTO coincidencias VALUES (998,'hotel',129);

            INSERT INTO coincidencias VALUES (999,'canasta',130);
            INSERT INTO coincidencias VALUES (1000,'canal',130);
            INSERT INTO coincidencias VALUES (1001,'kanata',130);
            INSERT INTO coincidencias VALUES (1002,'canal fiesta',130);
            INSERT INTO coincidencias VALUES (1003,'canal está',130);
            INSERT INTO coincidencias VALUES (1004,'lanata',130);
            INSERT INTO coincidencias VALUES (1005,'para nata',130);
            INSERT INTO coincidencias VALUES (1006,'anata',130);
            
            INSERT INTO coincidencias VALUES (1007,'iranosaurio',116);
            INSERT INTO coincidencias VALUES (1008,'ianoaurio',116);
            INSERT INTO coincidencias VALUES (1009,'aurio',116);
            
            INSERT INTO coincidencias VALUES (1010,'bronoaurio',131);
            INSERT INTO coincidencias VALUES (1011,'grontoaurio',131);
            INSERT INTO coincidencias VALUES (1012,'ontoaurio',131);
            INSERT INTO coincidencias VALUES (1013,'ontosaurio',131);
            INSERT INTO coincidencias VALUES (1014,'grontosaurio',131);
            
            INSERT INTO coincidencias VALUES (1015,'utas',42);
            
            INSERT INTO coincidencias VALUES (1016,'grutas',42);
            INSERT INTO coincidencias VALUES (1017,'gruta',42);
            INSERT INTO coincidencias VALUES (1018,'dutas',42);
            
            INSERT INTO coincidencias VALUES (1019,'arne',36);
            INSERT INTO coincidencias VALUES (1020,'garne',36);
            INSERT INTO coincidencias VALUES (1021,'darne',36);
            INSERT INTO coincidencias VALUES (1022,'ne',36);
            
            INSERT INTO coincidencias VALUES (1023,'ontana',132);
            INSERT INTO coincidencias VALUES (1024,'ontama',132);
            INSERT INTO coincidencias VALUES (1025,'gontana',132);
            INSERT INTO coincidencias VALUES (1026,'ontaña',132);
            
            INSERT INTO coincidencias VALUES (1027,'ocas',133);
            INSERT INTO coincidencias VALUES (1028,'oca',133);
            INSERT INTO coincidencias VALUES (1029,'roca',133);
            INSERT INTO coincidencias VALUES (1030,'goca',133);
            INSERT INTO coincidencias VALUES (1031,'gocas',133);
            
            INSERT INTO coincidencias VALUES (1032,'anzana',134);
            INSERT INTO coincidencias VALUES (1033,'angana',134);
            INSERT INTO coincidencias VALUES (1034,'andana',134);
            INSERT INTO coincidencias VALUES (1035,'mandana',134);
            INSERT INTO coincidencias VALUES (1036,'mangana',134);
            
            INSERT INTO coincidencias VALUES (1037,'aranja',135);
            INSERT INTO coincidencias VALUES (1038,'aanja',135);
            INSERT INTO coincidencias VALUES (1039,'garanga',135);
            
            INSERT INTO coincidencias VALUES (1040,'uego',136);
            INSERT INTO coincidencias VALUES (1041,'juego',136);
            INSERT INTO coincidencias VALUES (1042,'guego',136);
            INSERT INTO coincidencias VALUES (1043,'ego',136);
            
            INSERT INTO coincidencias VALUES (1044,'ogo',137);
            INSERT INTO coincidencias VALUES (1045,'oco',137);
            INSERT INTO coincidencias VALUES (1046,'gogo',137);
            INSERT INTO coincidencias VALUES (1047,'jojo',137);
            
            INSERT INTO coincidencias VALUES (1048,'hella',138);
            INSERT INTO coincidencias VALUES (1049,'gela',138);
            INSERT INTO coincidencias VALUES (1050,'telas',138);
            INSERT INTO coincidencias VALUES (1051,'tela',138);
            INSERT INTO coincidencias VALUES (1052,'pera',138);
            INSERT INTO coincidencias VALUES (1053,'pela',138);
            INSERT INTO coincidencias VALUES (1054,'terra',138);
            INSERT INTO coincidencias VALUES (1055,'pero',138);
            
            INSERT INTO coincidencias VALUES (1056,'museos',139);
            INSERT INTO coincidencias VALUES (1057,'museo',139);
            INSERT INTO coincidencias VALUES (1058,'mustio',139);
            INSERT INTO coincidencias VALUES (1059,'buseo',139);
            INSERT INTO coincidencias VALUES (1060,'cuseo',139);
            INSERT INTO coincidencias VALUES (1061,'sucio',139);
            INSERT INTO coincidencias VALUES (1062,'osea',139);
            INSERT INTO coincidencias VALUES (1063,'no sea',139);
            INSERT INTO coincidencias VALUES (1064,'eo',139);
            INSERT INTO coincidencias VALUES (1065,'eol',139);
            INSERT INTO coincidencias VALUES (1066,'useo',139);
            INSERT INTO coincidencias VALUES (1067,'mujeo',139);
            INSERT INTO coincidencias VALUES (1068,'mufeo',139);
            
            INSERT INTO coincidencias VALUES (1069,'circo',140);
            INSERT INTO coincidencias VALUES (1070,'circ',140);
            INSERT INTO coincidencias VALUES (1071,'citop',140);
            INSERT INTO coincidencias VALUES (1072,'cicop',140);
            INSERT INTO coincidencias VALUES (1073,'oct',140);
            INSERT INTO coincidencias VALUES (1074,'ok',140);
            INSERT INTO coincidencias VALUES (1075,'pipo',140);
            INSERT INTO coincidencias VALUES (1076,'si o no',140);
            INSERT INTO coincidencias VALUES (1077,'si',140);
            INSERT INTO coincidencias VALUES (1078,'ico',140);
            INSERT INTO coincidencias VALUES (1079,'isco',140);
            INSERT INTO coincidencias VALUES (1080,'kiko',140);
            INSERT INTO coincidencias VALUES (1081,'icod',140);
            INSERT INTO coincidencias VALUES (1082,'jico',140);
            INSERT INTO coincidencias VALUES (1083,'jirco',140);
            INSERT INTO coincidencias VALUES (1084,'dirjo',140);
            INSERT INTO coincidencias VALUES (1085,'jirjo',140);
            
            INSERT INTO coincidencias VALUES (1086,'sime',141);
            INSERT INTO coincidencias VALUES (1087,'cines',141);
            INSERT INTO coincidencias VALUES (1088,'sim',141);
            INSERT INTO coincidencias VALUES (1089,'see me',141);
            INSERT INTO coincidencias VALUES (1090,'inem',141);
            INSERT INTO coincidencias VALUES (1091,'ines',141);
            INSERT INTO coincidencias VALUES (1092,'in',141);
            INSERT INTO coincidencias VALUES (1093,'cine',141);
            INSERT INTO coincidencias VALUES (1094,'jine',141);
            INSERT INTO coincidencias VALUES (1095,'ine',141);
            
            INSERT INTO coincidencias VALUES (1096,'ian',142);
            INSERT INTO coincidencias VALUES (1097,'y',142);
            INSERT INTO coincidencias VALUES (1098,'hospital',142);
            INSERT INTO coincidencias VALUES (1099,'opital',142);
            INSERT INTO coincidencias VALUES (1100,'oital',142);
            INSERT INTO coincidencias VALUES (1101,'ojdital',142);
            
            INSERT INTO coincidencias VALUES (1102,'EOI',143);
            INSERT INTO coincidencias VALUES (1103,'hero',143);
            INSERT INTO coincidencias VALUES (1104,'perro',143);
            INSERT INTO coincidencias VALUES (1105,'eva',143);
            INSERT INTO coincidencias VALUES (1106,'eso',143);
            INSERT INTO coincidencias VALUES (1107,'ceo',143);
            INSERT INTO coincidencias VALUES (1108,'berro',143);
            INSERT INTO coincidencias VALUES (1109,'erro',143);
            INSERT INTO coincidencias VALUES (1110,'eo',143);
            INSERT INTO coincidencias VALUES (1111,'jerro',143);
            INSERT INTO coincidencias VALUES (1112,'jeo',143);
            
            INSERT INTO coincidencias VALUES (1113,'vichy',144);
            INSERT INTO coincidencias VALUES (1114,'visit',144);
            INSERT INTO coincidencias VALUES (1115,'vice',144);
            INSERT INTO coincidencias VALUES (1116,'visite',144);
            INSERT INTO coincidencias VALUES (1117,'bici',144);
            INSERT INTO coincidencias VALUES (1118,'bii',144);
            INSERT INTO coincidencias VALUES (1119,'viri',144);
            INSERT INTO coincidencias VALUES (1120,'pili',144);
            INSERT INTO coincidencias VALUES (1121,'pici',144);
            INSERT INTO coincidencias VALUES (1122,'biji',144);
            INSERT INTO coincidencias VALUES (1123,'iji',144);
            
            INSERT INTO coincidencias VALUES (1124,'taxis',145);
            INSERT INTO coincidencias VALUES (1125,'perfil',145);
            INSERT INTO coincidencias VALUES (1126,'papi',145);
            INSERT INTO coincidencias VALUES (1127,'taxi',145);
            INSERT INTO coincidencias VALUES (1128,'maxi',145);
            INSERT INTO coincidencias VALUES (1129,'ahi',145);
            INSERT INTO coincidencias VALUES (1130,'ai',145);
            INSERT INTO coincidencias VALUES (1131,'tai',145);
            INSERT INTO coincidencias VALUES (1132,'daji',145);
            INSERT INTO coincidencias VALUES (1133,'aji',145);
            INSERT INTO coincidencias VALUES (1134,'taji',145);
            
            INSERT INTO coincidencias VALUES (1135,'moto y le está',146);
            INSERT INTO coincidencias VALUES (1136,'moto y letra',146);
            INSERT INTO coincidencias VALUES (1137,'motor y letra',146);
            INSERT INTO coincidencias VALUES (1138,'moto y letal',146);
            INSERT INTO coincidencias VALUES (1139,'moto y le ta',146);
            INSERT INTO coincidencias VALUES (1140,'motocicletas',146);
            INSERT INTO coincidencias VALUES (1141,'motocicleta',146);
            INSERT INTO coincidencias VALUES (1142,'INE',146);            
            INSERT INTO coincidencias VALUES (1143,'INEM',146);
            INSERT INTO coincidencias VALUES (1144,'ENM',146);
            INSERT INTO coincidencias VALUES (1145,'image',146);
            INSERT INTO coincidencias VALUES (1146,'motojicleta',146);
            
            INSERT INTO coincidencias VALUES (1147,'aion',147);
            INSERT INTO coincidencias VALUES (1148,'ion',147);
            INSERT INTO coincidencias VALUES (1149,'aeon',147);
            INSERT INTO coincidencias VALUES (1150,'y león',147);
            INSERT INTO coincidencias VALUES (1151,'avion',147);
            INSERT INTO coincidencias VALUES (1152,'aviones',147);
            INSERT INTO coincidencias VALUES (1153,'avión y',147);
            INSERT INTO coincidencias VALUES (1154,'avions',147);
            INSERT INTO coincidencias VALUES (1155,'acción',147);
            INSERT INTO coincidencias VALUES (1156,'action',147);
            INSERT INTO coincidencias VALUES (1157,'jhon',147);
            INSERT INTO coincidencias VALUES (1158,'yon',147);
            
            INSERT INTO coincidencias VALUES (1159,'pon',104);
            INSERT INTO coincidencias VALUES (1160,'esponja',104);
            INSERT INTO coincidencias VALUES (1161,'pons',104);
            INSERT INTO coincidencias VALUES (1162,'pont',104);
            INSERT INTO coincidencias VALUES (1163,'com',104);
            INSERT INTO coincidencias VALUES (1164,'ESPO',104);
            INSERT INTO coincidencias VALUES (1165,'esponjas',104);
            INSERT INTO coincidencias VALUES (1166,'Sponja',104);
            INSERT INTO coincidencias VALUES (1167,'Sport',104);
            INSERT INTO coincidencias VALUES (1168,'esposa',104);
            INSERT INTO coincidencias VALUES (1169,'esposo',104);
            INSERT INTO coincidencias VALUES (1170,'Epo',104);
            INSERT INTO coincidencias VALUES (1171,'epos',104);
            INSERT INTO coincidencias VALUES (1172,'pepo',104);
            INSERT INTO coincidencias VALUES (1173,'epoxi',104);
            INSERT INTO coincidencias VALUES (1174,'EPOC',104);
            INSERT INTO coincidencias VALUES (1175,'el pozo',104);
            INSERT INTO coincidencias VALUES (1176,'he Esponja',104);
            
            INSERT INTO coincidencias VALUES (1177,'alo',148);
            INSERT INTO coincidencias VALUES (1178,'dalo',148);
            INSERT INTO coincidencias VALUES (1179,'balo',148);
            INSERT INTO coincidencias VALUES (1180,'galo',148);
            INSERT INTO coincidencias VALUES (1181,'gao',148);
            INSERT INTO coincidencias VALUES (1182,'ao',148);
            INSERT INTO coincidencias VALUES (1183,'dao',148);
            INSERT INTO coincidencias VALUES (1184,'bao',148);
            
            INSERT INTO coincidencias VALUES (1185,'apatos',50);		 	
            INSERT INTO coincidencias VALUES (1186,'apados',50);			
            INSERT INTO coincidencias VALUES (1187,'dapados',50);			
            INSERT INTO coincidencias VALUES (1188,'dapado',50);			
            INSERT INTO coincidencias VALUES (1189,'apado',50);			
            INSERT INTO coincidencias VALUES (1190,'apaos',50);			
        
            INSERT INTO coincidencias VALUES (1191,'piano audio',116);				
            INSERT INTO coincidencias VALUES (1192,'piano a orio',116);				
            INSERT INTO coincidencias VALUES (1193,'piano ahora',116);				
            INSERT INTO coincidencias VALUES (1194,'piano AOL',116);				
            INSERT INTO coincidencias VALUES (1195,'ya no ahora',116);				
            INSERT INTO coincidencias VALUES (1196,'iano ua',116);				
            INSERT INTO coincidencias VALUES (1197,'ian',116);				
            INSERT INTO coincidencias VALUES (1198,'ian AOLL',116);				
            INSERT INTO coincidencias VALUES (1199,'Ian Audi',116);				
            INSERT INTO coincidencias VALUES (1200,'Ian Ahora',116);				
            INSERT INTO coincidencias VALUES (1201,'y ano',116);				
            INSERT INTO coincidencias VALUES (1202,'y en audio',116);				
            INSERT INTO coincidencias VALUES (1203,'y no abrió',116);				
            INSERT INTO coincidencias VALUES (1204,'Ian abrió',116);				
            INSERT INTO coincidencias VALUES (1205,'Ian Audio',116);				
            INSERT INTO coincidencias VALUES (1206,'Ian Auryn',116);				
            INSERT INTO coincidencias VALUES (1207,'ya no saurio',116);				
            INSERT INTO coincidencias VALUES (1208,'Ian o saurio',116);				
            INSERT INTO coincidencias VALUES (1209,'yanos audio',116);				
            INSERT INTO coincidencias VALUES (1210,'y ah no saurio',116);				
            INSERT INTO coincidencias VALUES (1211,'ya nos Auryn',116);				

            INSERT INTO coincidencias VALUES (1212,'adta endal',46);				
            INSERT INTO coincidencias VALUES (1213,'asta',46);				
            INSERT INTO coincidencias VALUES (1214,'adta',46);				
            INSERT INTO coincidencias VALUES (1215,'al',46);				
            INSERT INTO coincidencias VALUES (1216,'ental',46);				
            INSERT INTO coincidencias VALUES (1217,'ednal',46);				
            INSERT INTO coincidencias VALUES (1218,'asta endal',46);				
            INSERT INTO coincidencias VALUES (1219,'ata enal',46);				
            INSERT INTO coincidencias VALUES (1220,'renal',46);				
            INSERT INTO coincidencias VALUES (1221,'serial',46);				


            INSERT INTO coincidencias VALUES (1222,'etes',149);			
            INSERT INTO coincidencias VALUES (1223,'dientes',149);			
            INSERT INTO coincidencias VALUES (1224,'dieta',149);			
            INSERT INTO coincidencias VALUES (1225,'dietes',149);			
            INSERT INTO coincidencias VALUES (1226,'diente',149);			
            INSERT INTO coincidencias VALUES (1227,'yate',149);			
            INSERT INTO coincidencias VALUES (1228,'yates',149);			
            INSERT INTO coincidencias VALUES (1229,'yete',149);			
            INSERT INTO coincidencias VALUES (1230,'ietes',149);			

            
            
            INSERT INTO coincidencias VALUES (1231,'oneda',150);				
            INSERT INTO coincidencias VALUES (1232,'onena',150);				
            INSERT INTO coincidencias VALUES (1233,'onenas',150);				
            INSERT INTO coincidencias VALUES (1234,'nena',150);				
            INSERT INTO coincidencias VALUES (1235,'bodega',150);				
            INSERT INTO coincidencias VALUES (1236,'bodegas',150);				
            INSERT INTO coincidencias VALUES (1237,'oenas',150);				
            INSERT INTO coincidencias VALUES (1238,'onemas',150);				
            INSERT INTO coincidencias VALUES (1239,'boneda',150);				
            INSERT INTO coincidencias VALUES (1240,'bonebas',150);				


            INSERT INTO coincidencias VALUES (1241,'idea',151);				
            INSERT INTO coincidencias VALUES (1242,'jimenea',151);				
            INSERT INTO coincidencias VALUES (1243,'jienea',151);				
            INSERT INTO coincidencias VALUES (1244,'jinea',151);				
            INSERT INTO coincidencias VALUES (1245,'enea',151);				
            INSERT INTO coincidencias VALUES (1246,'ea',151);				
            INSERT INTO coincidencias VALUES (1247,'fimea',151);				
            INSERT INTO coincidencias VALUES (1248,'fimenea',151);				

            INSERT INTO coincidencias VALUES (1249,'ana',152);				
            INSERT INTO coincidencias VALUES (1250,'paja',152);				
            INSERT INTO coincidencias VALUES (1251,'amana',152);				
            INSERT INTO coincidencias VALUES (1252,'gamana',152);				
            INSERT INTO coincidencias VALUES (1253,'gamaga',152);				
            INSERT INTO coincidencias VALUES (1254,'ara',152);				
            INSERT INTO coincidencias VALUES (1255,'aga',152);				
            INSERT INTO coincidencias VALUES (1256,'aja',152);				
            INSERT INTO coincidencias VALUES (1257,'amala',152);				
            INSERT INTO coincidencias VALUES (1258,'lamala',152);				
			
            INSERT INTO coincidencias VALUES (1259,'dia',153);				
            INSERT INTO coincidencias VALUES (1260,'ia',153);				
            INSERT INTO coincidencias VALUES (1261,'ja dia',153);				
            INSERT INTO coincidencias VALUES (1262,'dandia',153);				
            INSERT INTO coincidencias VALUES (1263,'jandia',153);				
            INSERT INTO coincidencias VALUES (1264,'fandia',153);				
            
            INSERT INTO coincidencias VALUES (1265,'Com sandia',121);				
            INSERT INTO coincidencias VALUES (1266,'come sandia',121);				
            INSERT INTO coincidencias VALUES (1267,'camí sandia',121);				
            INSERT INTO coincidencias VALUES (1268,'Carmen sandia',121);				
            INSERT INTO coincidencias VALUES (1269,'camisa en día',121);				
            INSERT INTO coincidencias VALUES (1270,'Omnia',121);				
            INSERT INTO coincidencias VALUES (1271,'omenaldia',121);				
            INSERT INTO coincidencias VALUES (1272,'omnia',121);				
            INSERT INTO coincidencias VALUES (1273,'omnis',121);				
            INSERT INTO coincidencias VALUES (1274,'omni a',121);				
            INSERT INTO coincidencias VALUES (1275,'home al día',121);				

            INSERT INTO coincidencias VALUES (1276,'BBVA',111);				
            INSERT INTO coincidencias VALUES (1277,'cueva',111);				
            INSERT INTO coincidencias VALUES (1278,'cuevas',111);				
            INSERT INTO coincidencias VALUES (1279,'Eva',111);				
            INSERT INTO coincidencias VALUES (1280,'eBay',111);				
            INSERT INTO coincidencias VALUES (1281,'EBA',111);				
            INSERT INTO coincidencias VALUES (1282,'qué va',111);				
            INSERT INTO coincidencias VALUES (1283,'beba',111);				
            INSERT INTO coincidencias VALUES (1284,'queva',111);				
            INSERT INTO coincidencias VALUES (1285,'prueba',111);				
            INSERT INTO coincidencias VALUES (1286,'nueva',111);				

            INSERT INTO coincidencias VALUES (1287,'Hero',143);			
            INSERT INTO coincidencias VALUES (1288,'eros',143);			
            INSERT INTO coincidencias VALUES (1289,'air',143);			
            INSERT INTO coincidencias VALUES (1290,'Ebro',143);			
            INSERT INTO coincidencias VALUES (1291,'perro',143);			
            INSERT INTO coincidencias VALUES (1292,'ero',143);			
            INSERT INTO coincidencias VALUES (1293,'enero',143);			
            INSERT INTO coincidencias VALUES (1294,'seur',143);			
            INSERT INTO coincidencias VALUES (1295,'ok',143);			
            INSERT INTO coincidencias VALUES (1296,'se o',143);			
            INSERT INTO coincidencias VALUES (1297,'seur',143);			
            INSERT INTO coincidencias VALUES (1298,'o',143);			
            INSERT INTO coincidencias VALUES (1299,'segó',143);			
            INSERT INTO coincidencias VALUES (1300,'sergo',143);			
            INSERT INTO coincidencias VALUES (1301,'ser',143);			
            INSERT INTO coincidencias VALUES (1302,'juego',143);			
            INSERT INTO coincidencias VALUES (1303,'cejó',143);			
            INSERT INTO coincidencias VALUES (1304,'seco',143);			
            INSERT INTO coincidencias VALUES (1305,'sergio',143);			
            INSERT INTO coincidencias VALUES (1306,'Gecko',143);			
            INSERT INTO coincidencias VALUES (1307,'Gesco',143);			
            INSERT INTO coincidencias VALUES (1308,'geco',143);
            
            INSERT INTO coincidencias VALUES (1309,'canal',130);			
            INSERT INTO coincidencias VALUES (1310,'canals',130);			
            INSERT INTO coincidencias VALUES (1311,'canasta',130);			
            INSERT INTO coincidencias VALUES (1312,'canada',130);			
            INSERT INTO coincidencias VALUES (1313,'Ana',130);			
            INSERT INTO coincidencias VALUES (1314,'ANAC',130);			
            INSERT INTO coincidencias VALUES (1315,'anal',130);			
            INSERT INTO coincidencias VALUES (1316,'Anas',130);			
            INSERT INTO coincidencias VALUES (1317,'Anna',130);			
            INSERT INTO coincidencias VALUES (1318,'anata',130);			
            INSERT INTO coincidencias VALUES (1319,'Ana está',130);			
            INSERT INTO coincidencias VALUES (1320,'carne',36);				
            INSERT INTO coincidencias VALUES (1321,'carnet',36);				
            INSERT INTO coincidencias VALUES (1322,'carnes',36);				
            INSERT INTO coincidencias VALUES (1323,'Carmen',36);				
            INSERT INTO coincidencias VALUES (1324,'Anne',36);				
            INSERT INTO coincidencias VALUES (1325,'ane',36);				
            INSERT INTO coincidencias VALUES (1326,'Annie',36);				
            INSERT INTO coincidencias VALUES (1327,'Any',36);				
            INSERT INTO coincidencias VALUES (1328,'Andy',36);				
            INSERT INTO coincidencias VALUES (1329,'came',36);				
            INSERT INTO coincidencias VALUES (1330,'tándem',36);				
            INSERT INTO coincidencias VALUES (1331,'candem',36);				
            INSERT INTO coincidencias VALUES (1332,'cande',36);				
            INSERT INTO coincidencias VALUES (1333,'cannes',36);	
                    
            INSERT INTO coincidencias VALUES (1334,'Utah',42);				
            INSERT INTO coincidencias VALUES (1335,'utad',42);				
            INSERT INTO coincidencias VALUES (1336,'UTA',42);				
            INSERT INTO coincidencias VALUES (1337,'putas',42);				
            INSERT INTO coincidencias VALUES (1338,'futa',42);				
            INSERT INTO coincidencias VALUES (1339,'futas',42);				
            INSERT INTO coincidencias VALUES (1340,'puta',42);				
            INSERT INTO coincidencias VALUES (1341,'uta',42);				
            INSERT INTO coincidencias VALUES (1342,'su ta',42);				
            INSERT INTO coincidencias VALUES (1343,'ruta',42);				
            INSERT INTO coincidencias VALUES (1344,'muta',42);				
            INSERT INTO coincidencias VALUES (1345,'mutas',42);				
            INSERT INTO coincidencias VALUES (1346,'mudas',42);	
            
            INSERT INTO coincidencias VALUES (1347,'gandía',153);			
            INSERT INTO coincidencias VALUES (1348,'Jandía',153);			
            INSERT INTO coincidencias VALUES (1349,'andía',153);			
            INSERT INTO coincidencias VALUES (1350,'Andy',153);			
            INSERT INTO coincidencias VALUES (1351,'al día',153);			
            INSERT INTO coincidencias VALUES (1352,'and día',153);			
            INSERT INTO coincidencias VALUES (1353,'mandía',153);			
            INSERT INTO coincidencias VALUES (1354,'mania',153);			
            INSERT INTO coincidencias VALUES (1355,'aria',153);			
            INSERT INTO coincidencias VALUES (1356,'adidas',153);			
            INSERT INTO coincidencias VALUES (1357,'haría',153);			
            INSERT INTO coincidencias VALUES (1358,'a día',153);			
            INSERT INTO coincidencias VALUES (1359,'adía',153);			
            
            INSERT INTO coincidencias VALUES (1360,'lámpara',152);
            INSERT INTO coincidencias VALUES (1361,'lámparas',152);
            INSERT INTO coincidencias VALUES (1362,'la lámpara',152);
            INSERT INTO coincidencias VALUES (1363,'plan para',152);
            INSERT INTO coincidencias VALUES (1364,'lam para',152);
            
            INSERT INTO coincidencias VALUES (1365,'práctico',9);
            
            INSERT INTO coincidencias VALUES (1366,'rutas',42);
            
            

            INSERT INTO coincidencias VALUES (1367,'sandía',153);
            INSERT INTO coincidencias VALUES (1368,'familia',153);
            INSERT INTO coincidencias VALUES (1369,'gran día',153);
            INSERT INTO coincidencias VALUES (1370,'gandia',153);
            INSERT INTO coincidencias VALUES (1371,'la mía',153);
            
            INSERT INTO coincidencias VALUES (1372,'montaña',154);
            INSERT INTO coincidencias VALUES (1373,'montañas',154);
            INSERT INTO coincidencias VALUES (1374,'mon taña',154);
            INSERT INTO coincidencias VALUES (1375,'montaña y',154);
            INSERT INTO coincidencias VALUES (1376,'montañas y',154);
            
            INSERT INTO coincidencias VALUES (1377,'come sandía',121);
            INSERT INTO coincidencias VALUES (1378,'comer sandía',121);
            INSERT INTO coincidencias VALUES (1379,'cómo es un día',121);
            INSERT INTO coincidencias VALUES (1380,'comezón día',121);
            INSERT INTO coincidencias VALUES (1381,'como es el día',121);
            
            INSERT INTO coincidencias VALUES (1382,'montaña',155);
            INSERT INTO coincidencias VALUES (1383,'montañas',155);
            INSERT INTO coincidencias VALUES (1384,'mon taña',155);
            INSERT INTO coincidencias VALUES (1385,'montaña y',155);
            INSERT INTO coincidencias VALUES (1386,'montañas y',155);
                        
            INSERT INTO coincidencias VALUES (1387,'oca',156);			
            INSERT INTO coincidencias VALUES (1388,'gota',156);			
            INSERT INTO coincidencias VALUES (1389,'hoja',156);			
            INSERT INTO coincidencias VALUES (1390,'jota',156);			
            INSERT INTO coincidencias VALUES (1391,'goca',156);			
            INSERT INTO coincidencias VALUES (1392,'joga',156);		
            
            INSERT INTO coincidencias VALUES (1393,'ena',157);			
            INSERT INTO coincidencias VALUES (1394,'irene',157);			
            INSERT INTO coincidencias VALUES (1395,'jirena',157);			
            INSERT INTO coincidencias VALUES (1396,'iena',157);			
            INSERT INTO coincidencias VALUES (1397,'llena',157);			
            INSERT INTO coincidencias VALUES (1398,'igena',157);			

            INSERT INTO coincidencias VALUES (1399,'casa',158);             
            INSERT INTO coincidencias VALUES (1400,'caza',158);
            INSERT INTO coincidencias VALUES (1401,'casas',158);               
            INSERT INTO coincidencias VALUES (1402,'la casa',158);               
            INSERT INTO coincidencias VALUES (1403,'a casa',158);               
            
            INSERT INTO coincidencias VALUES (1404,'planta',159);
            INSERT INTO coincidencias VALUES (1405,'plantas',159);
            INSERT INTO coincidencias VALUES (1406,'atlanta',159);
            INSERT INTO coincidencias VALUES (1407,'planta de',159);               
            INSERT INTO coincidencias VALUES (1408,'lámparas',159);               
            INSERT INTO coincidencias VALUES (1409,'bandas',159);               
            INSERT INTO coincidencias VALUES (1410,'banda',159);               

            

    ]]
    return query
    end

return tabla_coincidencias

