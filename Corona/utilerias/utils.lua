local base = require("utilerias.base" )
local composer = require( "composer" )
local gameState = require("utilerias.gameState")
local physics = require "physics"

local utils={}

local currentScene
local currentButton
local varError = 0
local imgClick = ""
local markX,markY=0,0
local isBegan =false

---------------------------------------------------------------------------------------
---------*********************SONIDOS INSTRUCCIONES******************-----------
-- Manda el audio de la imagen/seleccion
local function sonarPalabra(event)
	
        if ( event.completed ) then
            
            local sonido = audio.loadSound( "sounds/sounds/Objetos/"..currentButton..".mp3")
            local audioChannel = audio.play( sonido, { channel=audioChannel, onComplete= currentScene.successful }  )
            if audioChannel > 0 then 
                 audio.setVolume( 1, { channel=audioChannel } )
            end     
        end
end

function utils.primerSonido(nameScene,button)
   --poner codigo 
   
   composer.showOverlay( "bloqueo" ,{ isModal = true } )
   
   currentScene = require( nameScene )
   currentButton = button
   
   local sonido = audio.loadSound( "sounds/sounds/principales/pronuncia.mp3")
   local audioChannel = audio.play( sonido, { channel=audioChannel , onComplete= sonarPalabra} )
   if audioChannel > 0 then 
        audio.setVolume( 1, { channel=audioChannel } )
   end
end

---------*********************FIN SONIDOS INSTRUCCIONES******************-----------

--Este metodo retorna la validacion de speech recognicer para saber si se pronuncio correctamente
-- Regresa un true o un false
function utils.validarVoz(nombreImagen)
    
    print("nombre imagen: "..tostring(nombreImagen))
    
   --[[local coincidencia = SpeechRecognized.LanzarSpeech()
    
    coincidencia = string.lower(coincidencia)
    nombreImagen = string.lower(nombreImagen)
    print("coincidencia: "..tostring(coincidencia)) 
    
    local boolPalabra = base.consultarExistencia(nombreImagen,coincidencia)]]--
    
     local boolPalabra = true      
    
    if boolPalabra or (imgClick~=nombreImagen) then
        varError = 0;
    elseif not(boolPalabra) then
        varError = varError+1;
        
        if varError > 1 then
            varError = 0
            boolPalabra = true
        end
    end
    
    imgClick = nombreImagen
    
    return boolPalabra
end
 
--Metodo para poner ids a las imagenes
function utils.renombrarImagenes(tipoJuego,object)
    
    if tipoJuego == 1 then
        if object.myName==1 then --la compra
             object.id= "cacahuates"
        
        elseif object.myName ==2 then
             object.id= "calabaza"
               
        elseif object.myName ==3 then 
             object.id= "carne"
               
        elseif object.myName ==4 then
             object.id= "coca"
               
        elseif object.myName ==5 then
             object.id= "chocolates"
               
         elseif object.myName ==6 then
             object.id= "papas"
        end
    elseif tipoJuego == 2 then--el viaje
        if object.myName==1 then 
             object.id= "blusa azul"
    
        elseif object.myName ==2 then
             object.id= "calcetines"
        elseif object.myName ==3 then
             object.id= "cepillo"
        elseif object.myName ==4 then
             object.id= "zapatos"
        elseif object.myName ==5 then
             object.id= "saco"
        elseif object.myName ==6 then
             object.id= "tren"
        end
        elseif tipoJuego == 3 then--cochinito
        if object.myName==1 then 
             object.id= "arbusto"
    
        elseif object.myName ==2 then
             object.id= "cochinito"
        elseif object.myName ==3 then
             object.id= "chimenea"
        elseif object.myName ==4 then
             object.id= "pino"
        elseif object.myName ==5 then
             object.id= "puerta"
        elseif object.myName ==6 then
             object.id= "ventana"
        end
     elseif tipoJuego == 4 then-- sonidos
         if object.myName==1 then --la compra
             object.id= "falda"
        elseif object.myName ==2 then
             object.id= "filete"
        elseif object.myName ==3 then
             object.id= "sopa"
        elseif object.myName ==4 then
             object.id= "sol"
        elseif object.myName ==5 then
             object.id= "saco"
        elseif object.myName ==6 then
             object.id= "fruta"
        end
    elseif tipoJuego == 5 then--popi
         if object.myName==1 then 
             --object.id= "cacahuates"
            object.id= "burbujas"
        elseif object.myName ==2 then
             --object.id= "calabaza"
            object.id= "shampoo"
        elseif object.myName ==3 then 
             --object.id= "carne"
            object.id= "pelota" 
        elseif object.myName ==4 then
             --object.id= "coca"
             object.id= "plato"   
        elseif object.myName ==5 then
            -- object.id= "chocolates"
            object.id= "popi"  
         elseif object.myName ==6 then
            -- object.id= "cacahuate"  
             object.id= "tina"
        end
    elseif tipoJuego == 6 then--tiranosaurio
         if object.myName==1 then 
       
         object.id= "brontosaurio"
        elseif object.myName ==2 then
            object.id= "carne"    
        elseif object.myName ==3 then 
            object.id= "frutas"     
        elseif object.myName ==4 then
           object.id= "montana"      
        elseif object.myName ==5 then
            object.id= "rocas"     
         elseif object.myName ==6 then
            object.id= "tiranosaurio"
        end
    elseif tipoJuego == 7 then  --  diferencias el viaje 
        if object.myName==1 then 
             object.id= "blusa azul"    
        elseif object.myName ==2 then
             object.id= "cepillo"
        elseif object.myName ==3 then
             object.id= "pasta dental"
        end
    elseif tipoJuego == 8 then  --  diferencias cochinitos
        if object.myName==1 then 
             object.id= "chimenea"    
        elseif object.myName ==2 then
             object.id= "cuadro"
        elseif object.myName ==3 then
             object.id= "lampara"
        end
    elseif tipoJuego == 9 then  --  diferencias la compra
        if object.myName==1 then 
             object.id= "chocolate"    
        elseif object.myName ==2 then
             object.id= "coca"
        elseif object.myName ==3 then
             object.id= "papas"
        end
    elseif tipoJuego == 10 then  -- diferencias sonidos
        if object.myName==1 then 
             object.id= "cepillo"    
        elseif object.myName ==2 then
             object.id= "foca"
        elseif object.myName ==3 then
             object.id= "sirena"
        end
     elseif tipoJuego == 11 then  -- popi patio
        if object.myName==1 then 
             object.id= "pelota"    
        elseif object.myName ==2 then
            -- object.id= "cuerda"  
             object.id= "cuerda"
        elseif object.myName ==3 then
            -- object.id= "disco"  
             object.id= "palo"
         elseif object.myName ==4 then
            -- object.id= "hueso"  
             object.id= "hueso"
        end
     elseif tipoJuego == 12 then  -- popi baño
        if object.myName==1 then 
             object.id= "shampoo"    
        elseif object.myName ==2 then  
             object.id= "cepillo"
        elseif object.myName ==3 then  
             object.id= "esponja"
         elseif object.myName ==4 then
             object.id= "toalla"
        end
     elseif tipoJuego == 13 then  -- popi cocina
        if object.myName==1 then 
             object.id= "croquetas"    
        elseif object.myName ==2 then
             object.id= "salchichas"
        elseif object.myName ==3 then
             object.id= "plato"
         elseif object.myName ==4 then
             object.id= "galletas"
        end
    
    elseif tipoJuego == 14 then-- la compra la compra
        if object.myName==1 then 
             object.id= "cacahuates"    
        elseif object.myName ==2 then
             object.id= "calabaza"
        elseif object.myName ==3 then
             object.id= "carne"
         elseif object.myName ==4 then
             object.id= "chocolates"
             elseif object.myName ==5 then
             object.id= "coca"
             elseif object.myName ==6 then
             object.id= "papas"
             elseif object.myName ==7 then
             object.id= "pera"
             elseif object.myName ==8 then
             object.id= "tortillas"
             
        end
     elseif tipoJuego == 15 then  -- la maleta
        if object.myName==1 then 
             object.id= "blusa azul"    
        elseif object.myName ==2 then
             object.id= "calcetines"
        elseif object.myName ==3 then
             object.id= "cepillo"
         elseif object.myName ==4 then
             object.id= "zapatos"             
         elseif object.myName ==5 then
             object.id= "saco"
         elseif object.myName ==6 then
             object.id= "pasta dental"
        end
     elseif tipoJuego == 16 then  -- sarita viaje lugar
        if object.myName==1 then 
             object.id= "cerro"    
        elseif object.myName ==2 then
             object.id= "cine"
        elseif object.myName ==3 then
             object.id= "circo"
         elseif object.myName ==4 then
             object.id= "hospital"             
         elseif object.myName ==5 then
             object.id= "museo"
        end
    elseif tipoJuego == 17 then  -- sarita viaje modo
        if object.myName==1 then 
             object.id= "motocicleta"    
        elseif object.myName ==2 then
             object.id= "bici"
        elseif object.myName ==3 then
             object.id= "autobus"
         elseif object.myName ==4 then
             object.id= "taxi"             
        end
    elseif tipoJuego == 18 then  -- sarita viaje formaPago
        if object.myName==1 then 
             object.id= "billetes"    
        elseif object.myName ==2 then
             object.id= "cheque"
        elseif object.myName ==3 then
             object.id= "monedas"
         elseif object.myName ==4 then
             object.id= "tarjeta"             
        end
     elseif tipoJuego == 19 then  -- arrastra
        if object.myName==1 then 
             object.id= "falda"    
        elseif object.myName ==2 then
             object.id= "coca"
        elseif object.myName ==3 then
             object.id= "pino"
         elseif object.myName ==4 then
             object.id= "sol"             
         elseif object.myName ==5 then
             object.id= "tortillas"
        end
     elseif tipoJuego == 20 then  -- secuencias
        if object.myName==1 then 
             object.id= "pelota"    
        elseif object.myName ==2 then
             object.id= "tina"
        elseif object.myName ==3 then
             object.id= "esponja"
         elseif object.myName ==4 then
             object.id= "toalla"             
         elseif object.myName ==5 then
             object.id= "croquetas"             
         elseif object.myName ==6 then
             object.id= "cama"
        end
    end 
end

--"sounds/intentalo de nuevo recuerda pronunciar correctamente.mp3")

-- pone los sonidos y los elimina de momoria
-- el primer parametro sera el sonido que reproducira
-- el segundo parametro es para saber si quieres que haya bloqueo
--                            si es 0 se pone bloqueo y se elimina
--                            si es 1 solo se elimina el bloqueo (es el caso de que venga de validar voz)
-- el tercer parametro es una funcion (en la clase que lo invoca se pone que 
--                                      hará cuando tome valor esta funcion)
function utils.reproducirSonido( sonido, bloqueo, fnCallback )
    
    if bloqueo == 0 then
        composer.showOverlay( "bloqueo" ,{ isModal = true } )
    end
        
        local sound = audio.loadSound( "sounds/"..sonido..".mp3" )

        local audioChannel = audio.play( sound, { channel = audioChannel,
                            onComplete= function()
                                                audioChannel = nil
                                                audio.dispose( audioChannel )
                                                if bloqueo == 0 or bloqueo == 1 then
                                                    composer.removeScene( "bloqueo" )
                                                end
                                                if fnCallback~=nil then
                                                    fnCallback()
                                                end
                                        end  } )
	if audioChannel > 0 then 
		--audio.setVolume( 0.07, { channel=laserChannel } )
		--print( "channel new " .. laserChannel )
	end
	
end

--Método para generar el drag & drop
function utils.dragging( myImage, toImage, nameScene, fnCallback )
    
    function myImage:touch( event )
        if event.phase == "began" then
            
            -- begin focus  ---- +++ para que no mueva otras piezas mientras arrastras +++ ----
            display.getCurrentStage():setFocus( self, event.id )
            self.isFocus = true
            
            myImage:toFront()
        
            markX = self.x    -- store x location of object
            markY = self.y    -- store y location of object
            isBegan = true    -- Esta variable sirve para saber si se ha comenzado a tocar la imagen con un touch
            --print ("Began dragging "..markX.." myImage.id"..myImage.id)
        elseif event.phase == "moved" then
            --print (tostring(isBegan))
            if isBegan== false then --Hay casos que no entra a began porque la imagen comienza "en movimiento"
            -- cuando pasa esto la lo que se requiere es la posicion original de la imagen que se quiere mover
                markX = self.x    -- store x location of object
                markY = self.y    -- store y location of object
                isBegan=true --Se marca la bandera para indicarq que la imagen ha comenzado a moverse ya
            end
            --Procedimiento normal
            local x = (event.x - event.xStart) + markX
            local y = (event.y - event.yStart) + markY
            
            self.x, self.y = x, y    -- move object based on calculations above
            
            
        
        --Para apagar la bandera cuando se libera
        elseif event.phase == "ended" or event.phase == "cancelled" then
            isBegan=false --apagar la bandera para evitar conflictos con el bloqueo temporal del sonido y draggins futuros    
            currentScene = require( nameScene )
            currentScene.imagenSeleccionada = myImage
            currentScene.imagenColisionada = toImage
            currentScene.posIniX = markX
            currentScene.posIniY = markY
            
            --end focus
            display.getCurrentStage():setFocus( self, nil )
            self.isFocus = false
                
            if utils.colision(myImage,toImage) then                 
                --print ("El nombre de la imagen sleccionada: "..currentScene.imagenSeleccionada.id)
                fnCallback(true)-- devuelve si colisiono
            else
                fnCallback(false)
            end
            
           -- print(" final "..tostring(isBegan))
        end
        
        return true
    end


     
    -- make 'myObject' listen for touch events
    myImage:addEventListener( "touch", myImage )

end


function utils.obtenerPreferencesJuego(nombreJuego)
    row = base.consultarJuego(nombreJuego)
    if row ==nil then
        print("el juego: "..nombreJuego.. " no existe, verificar el nombre en la tabla minijuegos ")
    end
    return row
end

function utils.actualizarPreferenciaJuego(campo,valorNuevo,nombreJuego)
    base.actualizarJuegoSuper(campo, valorNuevo, nombreJuego)
end

function utils.colision(obj1, obj2) 
   
   --Sirve para colicionar la el touch del usuario con su dedo y una imagen enespecifico
   if obj1.w==nil then
   		obj1.w=0
   end
   if obj1.h==nil then
   		obj1.h=0
   end

   --Objeto 1
   posicionX1delObj1=(obj1.x-obj1.w/2)
   posicionX2delObj1=(obj1.x+obj1.w/2)
   posicionY1delObj1=(obj1.y-obj1.h/2)
   posicionY2delObj1=(obj1.y+obj1.h/2)
   --Objeto 2
   posicionX1delObj2=(obj2.x-obj2.w/2)
   posicionX2delObj2=(obj2.x+obj2.w/2)
   posicionY1delObj2=(obj2.y-obj2.h/2)
   posicionY2delObj2=(obj2.y+obj2.h/2)
   
   if (posicionX2delObj1>=posicionX1delObj2 and posicionX1delObj1<=posicionX2delObj2)
   and(posicionY2delObj1>=posicionY1delObj2 and posicionY1delObj1<=posicionY2delObj2)

   	then
   	return true
   else
   	return false
   end
   
end

function utils.tipoImagen()
    if  gameState.tipodejuego==1 then
        path="images/lacompra/"
    elseif  gameState.tipodejuego==2 then
        path="images/elviaje/"
    elseif  gameState.tipodejuego==3 then
        path="images/cochinitos/"
    elseif  gameState.tipodejuego==4 then
        path="images/lossonidos/"
    elseif  gameState.tipodejuego==5 then
        path="images/popi/"
    elseif  gameState.tipodejuego==6 then
        path="images/tiranosaurio/"
    end     
end

function utils.resetearMiniJuego(nombreMiniJuego)
    base.resetearMiniJuego(nombreMiniJuego)    
end

--Indica si un elemento existe dentro de una tabla
function utils.contains(table, element)
  for _, value in pairs(table) do
    if value == element then
      return true
    end
  end
  return false
end

function utils.ponerGlobos()
    
    local group = display.newGroup()
    
    utils.reproducirSonido("sounds/principales/globos", 0, 
                            function() 
                            physics.stop()
                            group:removeSelf()
                            composer.gotoScene ( "menuJuego", { effect = "fade"} )
                            end)
        
        physics.start()

        local globo = display.newImage( group,"images/lacompra/final/1.png" )
        globo:translate( centerX*1.4, centerY*1.3 )
        local globo2 = display.newImage( group,"images/lacompra/final/2.png" )
        globo2:translate( centerX/2, centerY*1.2 )
        local globo3 = display.newImage( group,"images/lacompra/final/3.png" )
        globo3:translate( centerX, centerY*1.4 )
        
        physics.setGravity( 0, -2 )

        physics.addBody( globo )
        physics.addBody( globo2 )
        physics.addBody( globo3 )
        
end


return utils
