
require "sqlite3"
local tabl_palabras= require( "utilerias.tabla_palabras")
local tbl_coincidencias= require ("utilerias.tabla_coincidencias")
local tbl_minijuegos= require ("utilerias.tabla_minijuegos")

local base={}

local db

--Abrir la base de datos, si no existe la crea
function base.abrir()
	local path = system.pathForFile("juegosv54.db", system.DocumentsDirectory)

	

	--Crea la tabla si no existe
	local tablecoincidencias = [[CREATE TABLE IF NOT EXISTS coincidencias (id INTEGER PRIMARY KEY,coincidencia , id_palabra);]]
	local tablepalabras = [[CREATE TABLE IF NOT EXISTS palabras (id_palabra INTEGER PRIMARY KEY,palabra);]]
        local tablesuper = [[CREATE TABLE IF NOT EXISTS miniJuego (id_minijuego TEXT PRIMARY KEY,btn1,btn2,btn3,btn4);]]
        
        db = sqlite3.open( path )   
	db:exec( tablecoincidencias )
	db:exec( tablepalabras )
        db:exec( tablesuper )
        base.resetearMiniJuego("elsuper")
        base.resetearMiniJuego("popi")
        row=base.selectCustomFrom("select count( * ) numR from coincidencias;")
	print( "numero de filas:".. row.numR)
	if(row.numR==0)then
	
		local tablePalabrasFill = tabl_palabras.query()
		local tableCoincidenciasFill = tbl_coincidencias.query()
                local tableminiJuegosFill = tbl_minijuegos.query()
		print( "Base de datos creada Query:tabla palabras: "..tablePalabrasFill )
		print( "Base de datos creada Query:tabla coincidencias: "..tableCoincidenciasFill )
                print( "Base de datos creada Query:tabla miniJuegos: "..tableminiJuegosFill )
		
                
                db:exec("BEGIN TRANSACTION;")
                db:exec( tablePalabrasFill )
                db:exec( tableCoincidenciasFill )
                db:exec( tableminiJuegosFill )    
                db:exec("END TRANSACTION;")
                

	end
	
end

-- cerrar base de datos, meterlo en un onSystemEvent
function base.cerrar()
        if db and db:isopen() then
			db:close()
		end
end
--Consulta personalizada
function base.selectCustomFrom(query)
	print( tostring(db) )
	for row in db:nrows(query) do
	  print( "base.selectCustomFrom:"..query );
	 return row
	end
	
end
--Consulta general
function base.selectAllFrom()
	
	for row in db:nrows("SELECT a.*,b.* FROM palabras a join coincidencias b on a.id_palabra==b.id_palabra") do
	  --print( "idJugador:"..row.id );
	  return row
	end
	
end

-- la palabra que es buscada y las coincidencias generadas por el speech recognicer (No siempre lo que se dice por el microfono es correcto)
function base.consultarExistencia(palabra, coincidencias)
    local row = base.consultarPalabra(palabra,coincidencias)
    if row then
        return true
    else 
        print("No existe!!!")
        print("La palabra '"..palabra.."' del speech no existe en la BD ni en el juego")
        return false
    end
end

-- args representa a un arreglo con ciertos palabras dentro de la base de datos
function base.consultarPalabra(palabra, args)
	local query = "SELECT * FROM palabras a left join coincidencias b on a.id_palabra==b.id_palabra where b.coincidencia in ("..tostring(args)..") or a.palabra in ("..tostring(args)..")"
        for row in db:nrows(query) do
            
        local palabraFinal= palabra
        local palabraFinal2= palabra
        if string.sub( palabraFinal, string.len( palabraFinal ), string.len( palabraFinal )) == "s" then
            palabraFinal= string.sub( palabraFinal, 0, string.len( palabraFinal )-1)
            palabraFinal2 = string.sub( palabraFinal2, 0, string.len( palabraFinal2 )-2)
        end
        if string.lower(row.palabra) == string.lower(palabra) or string.lower(row.palabra) == string.lower(palabraFinal) or string.lower(row.palabra) ==string.lower( palabraFinal2) then
            print("Correcto!!!")
            print ("Palabras correctas: "..palabra)
            return row
        else
            print("Se encontro pero no coincide!!!")
            print("La palabra: '"..tostring(row.palabra).."' existe en la BD pero la palabra buscada es: '"..palabra.."'")
        end
            
	end
    
    return nil
end

function base.consultarJuego(nombreJuego)
	local query = "SELECT * FROM miniJuego where id_minijuego= '"..nombreJuego.."'"
	for row in db:nrows(query) do
            return row
	end
    
    return nil
end

function base.actualizarJuegoSuper(campo,valor, juego)
	local query = "UPDATE miniJuego set "..campo.." = '"..valor.."' where id_miniJuego='"..juego.."'"
	base.update(query)
    
    return nil
end

function  base.update(query)--ejemplo: [[UPDATE test SET name='Big Bird' WHERE id=3;]]
	db:exec( query )
end

function base.resetearMiniJuego(claveMiniJuego)
    db:exec( "update miniJuego set btn1 = 'true', btn2='true', btn3='true', btn4='true' where id_minijuego = '"..claveMiniJuego.."'")
end

return base
-- --setup the system listener to catch applicationExit
-- Runtime:addEventListener( "system", onSystemEvent )